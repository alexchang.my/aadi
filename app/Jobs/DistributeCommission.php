<?php

namespace App\Jobs;

use App\Models\TimeTable;
use App\Models\User;
use App\Models\UserInvestmentTransaction;
use App\Models\UserTransaction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DistributeCommission implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds after which the job's unique lock will be released.
     *
     * @var int
     */
    public int $uniqueFor = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(public TimeTable $time)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            \DB::beginTransaction();

            $time = TimeTable::lockForUpdate()
                ->find($this->time->id);

            $this->logMessage('Distributing commission for . '.$time->getTimeFrom());

            if ($time->assured_roi_day != 1 && $time->structured_roi_day != 1) {
                throw new \Exception('This is not Assured/Structured ROI Day!!!!!!!!!');
            }

//            if ($time->assured_roi_commission_paid != 0 && $time->structured_roi_paid != 0) {
//                throw new \Exception('Assured/Structured ROI Commission PAID!!!!!!!!!!!');
//            }

            $records = UserInvestmentTransaction::with(['user', 'userInvestment'])
//                ->whereHas('userInvestment', function ($q) {
//                    $q->where('plan_type', '=', 'assured');
//                })
                ->where('need_pay_commission', '=', 1)
                ->where('commission_paid', '=', 0)
                ->orderBy('id', 'ASC')->get();

            foreach ($records as $uit) {
                $direct = $uit->user->introducer;

                $bmi_bv = 0;

                $params = [
                    'user_id' => $uit->user->id,
                    'username' => $uit->user->username,
                    'member_id' => $uit->user->member_id,
                    'contract_id' => $uit->userInvestment->contract_id,
                ];

                if ($direct) {
                    //DIRECT SPONSOR IS ACTIVE, CAN TAKE COMM
                    if ($direct->isActive()) {
                        $dsb_percentage = $direct->getDsbPercentage();
                        if ($dsb_percentage > 0) {
                            $dsb = bcmul(bcdiv($uit->amount, 100, 5), $dsb_percentage, 5);

                            if ($dsb > 0) {
                                $ut = new UserTransaction();
                                $ut->user_id = $direct->id;
                                $ut->credit_type = 2;
                                $ut->transaction_type = 401;
                                $ut->amount = $dsb;
                                $ut->is_bonus_type = $uit->userInvestment->plan_type;
                                $ut->related_key = $uit->related_key;
                                $ut->params = $params;
                                $ut->bonus_date_at = $time->getTimeFrom()->copy();
                                $ut->save();

                                User::where('id', '=', $ut->user_id)
                                    ->increment('credit_'.$ut->credit_type, $ut->amount);

                                if ($direct->ranking > 0) {
                                    $bmi_bv = bcadd($bmi_bv, $dsb, 5);
                                }
                            }
                        }
                    }

                    /**
                     * Bmb
                     * The nearest bm take
                     * Direct sponsor possible take bmb and direct tgt
                     */
                    $bm_receiver = null;
                    $current = $direct;
                    while (! $bm_receiver) {
                        if ($current->ranking > 0) {
                            $bm_receiver = $current;
                            break;
                        } else {
                            $current = $current->introducer;
                        }

                        if (! $current) {
                            break;
                        }
                    }

                    if ($bm_receiver) {
                        $bmb = bcmul(bcdiv($uit->amount, 100, 5), $bm_receiver->getBmbPercentage(), 5);
                        if ($bmb > 0) {
                            $ut2 = new UserTransaction();
                            $ut2->user_id = $bm_receiver->id;
                            $ut2->credit_type = 2;
                            $ut2->transaction_type = 501;
                            $ut2->amount = $bmb;
                            $ut2->is_bonus_type = $uit->userInvestment->plan_type;
                            $ut2->related_key = $uit->related_key;
                            $ut2->params = $params;
                            $ut2->bonus_date_at = $time->getTimeFrom()->copy();
                            $ut2->save();

                            User::where('id', '=', $ut2->user_id)
                                ->increment('credit_'.$ut2->credit_type, $ut2->amount);

                            $bmi_bv = bcadd($bmi_bv, $bmb, 5);
                        }

                        /**
                         * Bmi
                         * Per Roi find 10 nearest bm to pay, start from BMB / direct sponsor receiver
                         * Direct sponsor bonus(if direct sponsor is BM1,2,3) + Bmb paid just now is BV
                         * 10/15/20% of BV to the BM, max 10pax
                         */
                        if ($bmi_bv > 0) {
                            //START CLIMB FROM BM RECEIVER
                            $climbing = $bm_receiver->introducer;
                            $taken_pax = 0;
                            while ($climbing) {
                                if ($climbing->ranking > 0 && $climbing->getBmiPercentage() > 0) {
                                    $ut3 = new UserTransaction();
                                    $ut3->user_id = $climbing->id;
                                    $ut3->credit_type = 2;
                                    $ut3->transaction_type = 601;
                                    $ut3->amount = bcmul(bcdiv($bmi_bv, 100, 5), $climbing->getBmiPercentage(), 5);
                                    $ut3->is_bonus_type = $uit->userInvestment->plan_type;
                                    $ut3->related_key = $uit->related_key;
                                    $ut3->params = $params;
                                    $ut3->bonus_date_at = $time->getTimeFrom()->copy();
                                    $ut3->save();

                                    User::where('id', '=', $ut3->user_id)
                                        ->increment('credit_'.$ut3->credit_type, $ut3->amount);

                                    $taken_pax++;
                                }

                                if ($taken_pax >= 10) {
                                    break;
                                }

                                $climbing = $climbing->introducer;

                                if (! $climbing) {
                                    break;
                                }
                            }
                        }
                    }
                }

                $uit->commission_paid = 1;
                $uit->save();
            }

            if ($time->assured_roi_day == 1 && $time->assured_roi_commission_paid == 0) {
                $time->assured_roi_commission_paid = 1;
            }

            if ($time->structured_roi_day == 1 && $time->structured_roi_commission_paid == 0) {
                $time->structured_roi_commission_paid = 1;
            }

            $time->save();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            $this->logMessage('========== ASSURED ROI DISTRIBUTE COMMISSION THROW ERROR BELOW ==========');
            $this->logMessage($e->getFile());
            $this->logMessage($e->getLine());
            $this->logMessage($e->getMessage());
        }
    }

    protected function logMessage($message)
    {
        \Log::error($message);
    }

    /**
     * The unique ID of the job.
     *
     * @return string
     */
    public function uniqueId()
    {
        return 'distribute-commission-'.$this->time->id;
    }
}
