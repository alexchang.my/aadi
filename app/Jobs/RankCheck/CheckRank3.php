<?php

namespace App\Jobs\RankCheck;

use App\Models\User;
use App\Models\UserInvestment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckRank3 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The podcast instance.
     *
     * @var \App\Models\User
     */
    public $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->user->id);
        #RESET
        $user->downline_ranking_1 = 0;
        $user->downline_ranking_2 = 0;
        $user->downline_ranking_3 = 0;
        $user->bmb_count = 0;
        $user->group_sales = 0;

        $downlines = User::where('introducer_user_id', '=', $user->id)
            ->orderBy('id', 'ASC');

        //Calculate BMB and up rank
        if ($downlines->count()) {
            $line_achieved = 0;
            foreach ($downlines->get() as $g1) {
                $ids = [$g1->id];
                $line_ticked = [];
                if ($g1->last_ranking_check_set_rank == 0) {
                    $user->bmb_count++;
                }

                if ($g1->last_ranking_check_set_rank == 1) {
                    $user->downline_ranking_1++;
                } else if ($g1->last_ranking_check_set_rank == 2) {
                    $user->downline_ranking_2++;
                    if (!isset($line_ticked[$g1->id]) || !$line_ticked[$g1->id]) {
                        $line_ticked[$g1->id] = true;
                    }
                } else if ($g1->last_ranking_check_set_rank == 3) {
                    $user->downline_ranking_3++;
                    if (!isset($line_ticked[$g1->id]) || !$line_ticked[$g1->id]) {
                        $line_ticked[$g1->id] = true;
                    }
                }

                while ($ids) {
                    foreach (User::with(['introducer'])->whereIn('introducer_user_id', $ids)->orderBy('id', 'ASC')->get() as $u) {
                        if ($u->last_ranking_check_set_rank == 0 && $u->introducer && $u->introducer->last_ranking_check_set_rank == 0) {
                            $user->bmb_count++;
                        }

                        if ($u->last_ranking_check_set_rank == 1) {
                            $user->downline_ranking_1++;
                        } else if ($u->last_ranking_check_set_rank == 2) {
                            $user->downline_ranking_2++;
                            if (!isset($line_ticked[$g1->id]) || !$line_ticked[$g1->id]) {
                                $line_ticked[$g1->id] = true;
                            }
                        } else if ($u->last_ranking_check_set_rank == 3) {
                            $user->downline_ranking_3++;
                            if (!isset($line_ticked[$g1->id]) || !$line_ticked[$g1->id]) {
                                $line_ticked[$g1->id] = true;
                            }
                        }
                    }

                    $user->group_sales += UserInvestment::whereIn('user_id', $ids)
                        ->sum('total_invest');

                    $ids = User::whereIn('introducer_user_id', $ids)
                        ->orderBy('id', 'ASC')->pluck('id')->toArray();
                }

                if (isset($line_ticked[$g1->id]) && $line_ticked[$g1->id] == true) {
                    $line_achieved++;
                }
            }

            if ($line_achieved >= 5 && $user->last_ranking_check_set_rank == 2) {
                $user->last_ranking_check_set_rank = 3;
            }
        }

        $user->ranking = $user->last_ranking_check_set_rank;
        $user->last_complete_ranking_check = now();
        $user->save();
    }
}
