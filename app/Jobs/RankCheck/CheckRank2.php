<?php

namespace App\Jobs\RankCheck;

use App\Models\User;
use App\Models\UserInvestment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckRank2 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The podcast instance.
     *
     * @var \App\Models\User
     */
    public $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->user->id);
        if ($user->last_ranking_check_set_rank == 1) {
            $downlines = User::where('introducer_user_id', '=', $user->id)
                ->orderBy('id', 'ASC')
                ->get();
            $line_achieved = 0;
            foreach ($downlines as $g1) {
                $ids = [$g1->id];
                if ($g1->last_ranking_check_set_rank >= 1) {
                    $line_achieved++;
                    continue;
                }

                while ($ids) {
                    $gen_users = User::whereIn('introducer_user_id', $ids)
                        ->orderBy('id', 'ASC');

                    if ($gen_users->where('last_ranking_check_set_rank', '>=', 1)->count()) $line_achieved++;

                    $ids = $gen_users->pluck('id')->toArray();
                }

                if ($line_achieved >= 5) break;
            }

            if ($line_achieved >= 5) {
                $user->last_ranking_check_set_rank = 2;
            }
        }

        $user->last_complete_ranking_check = now();
        $user->save();
    }
}
