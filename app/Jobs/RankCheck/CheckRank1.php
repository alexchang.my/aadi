<?php

namespace App\Jobs\RankCheck;

use App\Models\User;
use App\Models\UserInvestment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckRank1 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The podcast instance.
     *
     * @var \App\Models\User
     */
    public $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->user->id);
        $user->last_ranking_check_set_rank = 0;

        if ($user->force_ranking > 0) {
            $user->last_ranking_check_set_rank = $user->force_ranking;
        }

        if ($user->last_ranking_check_set_rank <= 0) {
            $own_active_amount = UserInvestment::where('user_id', '=', $user->id)->Active()->sum('current_balance');
            $achieve_line = 0;
            $achieved = false;
            if ($own_active_amount >= 20000) {
                $achieved = true;
            }

            if (!$achieved) {
                foreach (User::where('introducer_user_id', '=', $user->id)->orderBy('id', 'ASC')->get() as $gen1) {
                    $line_total_invest = UserInvestment::where('user_id', '=', $gen1->id)->Active()->sum('current_balance');
                    $ids = [$gen1->id];
                    while (count($ids)) {
                        $uss = User::whereIn('introducer_user_id', $ids)
                            ->pluck('id');
                        $ids = $uss->toArray();

                        if (count($ids)) {
                            $line_total_invest += UserInvestment::whereIn('user_id', $ids)->Active()->sum('current_balance');
                        }

                        if ($line_total_invest >= 4000) {
                            break;
                        }
                    }

                    if ($line_total_invest >= 4000) {
                        $achieve_line += 1;
                    }
                }

                if ($achieve_line >= 5) {
                    $achieved = true;
                }
            }

            if ($achieved) {
                $user->last_ranking_check_set_rank = 1;
            }
        }

        $user->last_complete_ranking_check = now();
        $user->save();
    }
}
