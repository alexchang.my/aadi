<?php

namespace App\Jobs;

use App\Models\User;
use App\Models\UserInvestment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UserRankingCheck implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The podcast instance.
     *
     * @var \App\Models\User
     */
    public $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        $user = User::find($this->user->id);
//
//        $own_active_amount = UserInvestment::where('user_id', '=', $user->id)->Active()->sum('current_balance');
//        $achieve_line = 0;
//        $achieved = false;
//        if ($own_active_amount >= 20000 || $user->force_ranking > 0) {
//            $achieved = true;
//        }
//
//        if (!$achieved) {
//            foreach (User::where('introducer_user_id', '=', $user->id)->orderBy('id', 'ASC')->get() as $gen1) {
//                $line_total_invest = UserInvestment::where('user_id', '=', $gen1)->Active()->sum('current_balance')
//                    ->sum('current_balance');
//                $ids = [$gen1->id];
//                while (count($ids)) {
//                    $uss = User::whereIn('introducer_user_id', $ids)
//                        ->pluck('id');
//                    $ids = $uss->toArray();
//
//                    if (count($ids)) {
//                        $line_total_invest += UserInvestment::whereIn('user_id', $ids)->Active()->sum('current_balance');
//                    }
//
//                    if ($line_total_invest >= 4000) {
//                        break;
//                    }
//                }
//
//                if ($line_total_invest >= 4000) {
//                    $achieved = true;
//                }
//            }
//        }
//
//        $deep = new UserDeepRankingCheck();
//        $deep->check_date = now();
//        $deep->user_id = $user->id;
//        $deep->new_ranking = $achieved ? 1 : 0;
//        $deep->ranking_step = 1;
//        $deep->dispatch_at = now();
//        $deep->save();
//
//        dispatch(new UserRankingDeepCheck($deep));

        $user = User::find($this->user->id);

        $own_active_amount = UserInvestment::where('user_id', '=', $user->id)->Active()->sum('current_balance');

        //RESET FIRST
        $user->ranking = 0;
        $user->downline_ranking_1 = 0;
        $user->downline_ranking_2 = 0;
        $user->downline_ranking_3 = 0;
        $user->bmb_count = 0;
        $user->bmi_count = 0;

        /**
         * CALCULATE RANKING/BMI ----- START
         */
        $gen1_invest_rows = UserInvestment::whereHas('user', function ($query) use ($user) {
            $query->where('introducer_user_id', '=', $user->id);
        })->Active()->groupBy('user_id')->get();

        $gen1_invest_amount = UserInvestment::whereHas('user', function ($query) use ($user) {
            $query->where('introducer_user_id', '=', $user->id);
        })->Active()->sum('current_balance');

        if ($own_active_amount > 0 && $gen1_invest_rows->count() >= 5 && $gen1_invest_amount >= 20000) {
            $user->ranking = 1;
        }

        $downlines = User::where('introducer_user_id', '=', $user->id)->orderBy('id', 'ASC')->get();

        $b1 = 0;
        $b2 = 0;
        $b3 = 0;

        if ($downlines && $downlines->count()) {
            $line_ticks = [];
            foreach ($downlines as $downline) {
                $line_ticks[$downline->id] = [
                    'ranking_1' => false,
                    'ranking_2' => false,
                    'ranking_3' => false,
                ];

                if ($downline->ranking == 1) {
                    $line_ticks[$downline->id]['ranking_1'] = true;
                } elseif ($downline->ranking == 2) {
                    $line_ticks[$downline->id]['ranking_2'] = true;
                } elseif ($downline->ranking == 3) {
                    $line_ticks[$downline->id]['ranking_3'] = true;
                }
            }

            //TO BE SAFE, LOOP 2 ROUNDS
            //THIS IS THE FIRST ROUND, FIND OUT BMI and ranking up
            foreach ($downlines as $downline) {
                $ids = User::where('introducer_user_id', '=', $downline->id)
                    ->orderBy('id', 'ASC')->pluck('id');

                while (count($ids)) {
                    $b1_count = User::whereIn('id', $ids)
                        ->where('ranking', '=', 1)
                        ->count();

                    $b2_count = User::whereIn('id', $ids)
                        ->where('ranking', '=', 2)
                        ->count();

                    $b3_count = User::whereIn('id', $ids)
                        ->where('ranking', '=', 3)
                        ->count();

                    $b1 += $b1_count;
                    $b2 += $b2_count;
                    $b3 += $b3_count;

                    if ($b1_count > 0 && ! $line_ticks[$downline->id]['ranking_1']) {
                        $line_ticks[$downline->id]['ranking_1'] = true;
                    }

                    if ($b2_count > 0 && ! $line_ticks[$downline->id]['ranking_2']) {
                        $line_ticks[$downline->id]['ranking_2'] = true;
                    }

                    if ($b3_count > 0 && ! $line_ticks[$downline->id]['ranking_3']) {
                        $line_ticks[$downline->id]['ranking_3'] = true;
                    }

                    $new_ids = User::whereIn('introducer_user_id', $ids)
                        ->orderBy('id', 'ASC')->pluck('id');

                    if (count($new_ids)) {
                        $ids = $new_ids;
                    } else {
                        $ids = [];
                    }
                }
            }

            if ($user->ranking == 1) {
                $ranking_2_tick = 0;
                foreach ($line_ticks as $tick) {
                    if ($tick['ranking_1'] || $tick['ranking_2'] || $tick['ranking_3']) {
                        $ranking_2_tick += 1;
                    }
                }

                if ($ranking_2_tick >= 5) {
                    $user->ranking = 2;
                }
            }

            if ($user->ranking == 2) {
                $ranking_3_tick = 0;
                foreach ($line_ticks as $tick) {
                    if ($tick['ranking_2'] || $tick['ranking_3']) {
                        $ranking_3_tick += 1;
                    }
                }

                if ($ranking_3_tick >= 5) {
                    $user->ranking = 3;
                }
            }
        }

        $user->downline_ranking_1 = $b1;
        $user->downline_ranking_2 = $b2;
        $user->downline_ranking_3 = $b3;
        $user->bmi_count = $b1 + $b2 + $b3;
        /**
         * CALCULATE RANKING/BMI ----- END
         */

        /**
         * CALCULATE BMB SIZE ----- START
         */
        if ($downlines && $downlines->count()) {
            //TO BE SAFE, LOOP 2 ROUNDS
            //THIS IS THE SECOND ROUND, FIND OUT BMB only
            foreach ($downlines as $downline) {
                if ($downline->ranking > 0) {
                    $user->bmb_count += 1;
                    continue;
                }

                $ids = User::where('introducer_user_id', '=', $downline->id)
                    ->orderBy('id', 'ASC')->pluck('id');

                while (count($ids)) {
                    $user->bmb_count += User::whereIn('introducer_user_id', $ids)
                        ->where('ranking', '>', 0)
                        ->orderBy('id', 'ASC')
                        ->count();

                    $new_ids = User::whereIn('introducer_user_id', $ids)
                        ->where('ranking', '=', 0)
                        ->orderBy('id', 'ASC')
                        ->pluck('id');

                    if (count($new_ids)) {
                        $ids = $new_ids;
                    } else {
                        $ids = [];
                    }
                }
            }
        }
        /**
         * CALCULATE BMB SIZE ----- END
         */
        if ($user->force_ranking > $user->ranking) {
            $user->ranking = $user->force_ranking;
        }

        $user->last_complete_ranking_check = now();
        $user->save();

    }
}
