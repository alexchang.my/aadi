<?php

namespace App\Jobs;

use App\Models\TimeTable;
use App\Models\UserInvestment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ApproveFund implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds after which the job's unique lock will be released.
     *
     * @var int
     */
    public int $uniqueFor = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(public TimeTable $time)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            \DB::beginTransaction();

            $time = TimeTable::lockForUpdate()
                ->find($this->time->id);

            $this->logMessage('Approving fund for . '.$time->getTimeFrom());

            if ($time->assured_roi_day != 1 && $time->structured_roi_day != 1) {
                throw new \Exception('This is not Assured ROI Day!!!!!!!!!');
            }

            if ($time->assured_roi_fund_approved != 0 && $time->structured_roi_fund_approved != 0) {
                throw new \Exception('Assured ROI fund all approved!!!!!!!!!!!');
            }

            if ($time->assured_roi_day == 1 && $time->assured_roi_fund_approved == 0) {
                $records = UserInvestment::with(['investmentPlan'])
                    ->Inactive()
                    ->where('plan_type', '=', 'assured')
                    ->orderBy('id', 'ASC')->get();

                if ($records->count()) {
                    foreach ($records as $ui) {
                        $ui->approveFund();
                        $ui->save();
                    }
                }

                $time->assured_roi_fund_approved = 1;
            }

            if ($time->structured_roi_day == 1 && $time->structured_roi_fund_approved == 0) {
                $records = UserInvestment::with(['investmentPlan'])
                    ->Inactive()
                    ->where('plan_type', '=', 'structured')
                    ->orderBy('id', 'ASC')->get();

                if ($records->count()) {
                    foreach ($records as $ui) {
                        $ui->approveFund();
                        $ui->save();
                    }
                }

                $time->structured_roi_fund_approved = 1;
            }

            $time->save();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            $this->logMessage('========== ASSURED ROI APPROVE FUND THROW ERROR BELOW ==========');
            $this->logMessage($e->getFile());
            $this->logMessage($e->getLine());
            $this->logMessage($e->getMessage());
        }
    }

    protected function logMessage($message)
    {
        \Log::error($message);
    }

    /**
     * The unique ID of the job.
     *
     * @return string
     */
    public function uniqueId()
    {
        return 'approve-fund-'.$this->time->id;
    }
}
