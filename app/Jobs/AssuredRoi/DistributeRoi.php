<?php

namespace App\Jobs\AssuredRoi;

use App\Models\DistributionBatch;
use App\Models\DistributionSetting;
use App\Models\TimeTable;
use App\Models\UserInvestment;
use App\Models\UserInvestmentTransaction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DistributeRoi implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds after which the job's unique lock will be released.
     *
     * @var int
     */
    public int $uniqueFor = 3600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(public TimeTable $time)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            \DB::beginTransaction();

            $time = TimeTable::lockForUpdate()
                ->find($this->time->id);

            $this->logMessage('Distributing ROI for . '.$time->getTimeFrom());

            if ($time->assured_roi_day != 1) {
                throw new \Exception('This is not Assured ROI Day!!!!!!!!!');
            }

            if ($time->assured_roi_paid != 0) {
                throw new \Exception('Assured ROI PAID!!!!!!!!!!!');
            }

            $plan_type = 'assured';

            $this->logMessage('current utc time hour is '.$time->getTimeFrom()->hour.' ('.$time->getTimeFrom()->copy()->format('Y-m-d H:i:s').')');

            $setting = DistributionSetting::where('plan_type', '=', $plan_type)->where('approved', '=', 1)->Pending()->Latest()->first();

            $records = UserInvestment::with(['investmentPlan'])
                ->Active()
                ->where('plan_type', '=', $plan_type)
                ->where(function ($q) use ($time) {
                    $q->orWhereNull('last_distribute_roi_at')
                        ->orWhereDate('last_distribute_roi_at', '<', $time->getTimeFrom()->copy()->format('Y-m-d'));
                })
                ->where(function ($q) use ($time) {
                    $q->whereNotNull('fund_approve_at')
                        ->whereDate('fund_approve_at', '<', $time->getTimeFrom()->copy()->format('Y-m-d'));
                })
                ->orderBy('id', 'ASC');

            if ($records->count()) {
                $batch = DistributionBatch::where('plan_type', '=', $plan_type)->whereDate('distribution_date', '=', $time->getTimeFrom()->copy()->format('Y-m-d'))->orderBy('id', 'DESC');

                if ($setting) {
                    $batch = $batch->where('distribution_setting_id', '=', $setting->id);
                }

                $batch = $batch->first();

                if (! $batch) {
                    $batch = new DistributionBatch();
                    $batch->plan_type = $plan_type;
                    $batch->distribution_date = $time->getTimeFrom()->copy();
                    $batch->related_key = \Str::uuid();

                    if ($setting) {
                        $batch->distribution_setting_id = $setting->id;
                    }

                    $batch->params = $setting;
                    $batch->save();
                }

                foreach ($records->get() as $ui) {
                    if ($setting && isset($setting->adjusted_percentage[$ui->code])) {
                        $roi_percentage = $setting->adjusted_percentage[$ui->code];
                    } else {
                        $roi_percentage = $ui->investmentPlan->setting['min_return_percentage_per_cycle'];
                    }

                    if ($roi_percentage > 0) {
                        //POSITIVE
                        $roi = bcmul(bcdiv($ui->current_balance, 100, 5), $roi_percentage, 5);

                        $uit = new UserInvestmentTransaction();
                        $uit->user_id = $ui->user_id;
                        $uit->user_investment_id = $ui->id;
                        $uit->distribution_batch_id = $batch->id;
                        $uit->transaction_type = 101;
                        $uit->amount = $roi;
                        $uit->active = 1;
                        $uit->is_roi = 1;
                        $uit->need_pay_commission = 1;
                        $uit->commission_paid = 0;
                        $uit->related_key = $batch->related_key;
                        $uit->params = [
                            'roi_percentage' => $roi_percentage,
                            'batch_id' => $batch->id,
                            'date' => $time->getTimeFrom()->copy()->format('Y-m-d'),
                            'setting_id' => $setting->id ?? null,
                        ];
                        $uit->bonus_date_at = $time->getTimeFrom()->copy();
                        $uit->save();

                        $ui->total_roi = bcadd($ui->total_roi, $roi, 5);
                        $ui->roi_balance = bcadd($ui->roi_balance, $roi, 5);

                        $batch->total_roi = bcadd($batch->total_roi, $roi, 5);
                    }

                    $ui->last_distribute_roi_at = $time->getTimeFrom()->copy();
                    $ui->save();

                    $batch->total_capital = bcadd($batch->total_capital, $ui->current_balance, 5);
                }

                $batch->save();

                if ($setting) {
                    $setting->status = 1;
                    $setting->used_at = $time->getTimeFrom()->copy();
                    $setting->save();
                }
            }

            $time->assured_roi_paid = 1;
            $time->save();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            $this->logMessage('========== ASSURED ROI PAID ERROR BELOW ==========');
            $this->logMessage($e->getFile());
            $this->logMessage($e->getLine());
            $this->logMessage($e->getMessage());
        }
    }

    protected function logMessage($message)
    {
        \Log::info($message);
    }

    /**
     * The unique ID of the job.
     *
     * @return string
     */
    public function uniqueId()
    {
        return 'assured-roi-'.$this->time->id;
    }
}
