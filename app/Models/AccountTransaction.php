<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class AccountTransaction extends BaseModel
{
    use HasFactory;

    protected $casts = [
        'account_id' => 'integer',
        'created_by_admin_id' => 'integer',
        'verified_by_admin_id' => 'integer',
        'transaction_type' => 'integer',
        'amount' => 'float',
        'remark' => 'string',
        'related_key' => 'string',
        'params' => 'array',
        'verified' => 'integer',
        'verified_at' => 'datetime',
    ];

    protected $dates = [
        'verified_at',
    ];

    protected static function boot()
    {
        parent::boot();
        static::created(function ($instance) {
            if ($instance->verified) {
                $instance->triggerVerified();
            }
        });
    }

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }

    public function createdByAdmin()
    {
        return $this->belongsTo(Admin::class, 'created_by_admin_id', 'id');
    }

    public function verifiedByAdmin()
    {
        return $this->belongsTo(Admin::class, 'verified_by_admin_id', 'id');
    }

    public static function getTransactionTypeLists($params = [])
    {
        return [
            1 => trans('account_transaction.transfer_to_other_account_out', $params),
            2 => trans('account_transaction.transfer_to_other_account_in', $params),

            3 => trans('account_transaction.transfer_to_other_account_reject_out', $params),
            4 => trans('account_transaction.transfer_to_other_account_reject_in', $params),

            101 => trans('account_transaction.credit', $params),

            151 => trans('account_transaction.debit', $params),

            201 => trans('account_transaction.user_topup', $params),
            202 => trans('account_transaction.user_withdraw', $params),

            252 => trans('user_transaction.early_roi_withdraw_fees', $params),
            253 => trans('investment_transaction.early_capital_withdraw_roi_burn', $params),

            271 => trans('user_transaction.capital_withdraw', $params),
            272 => trans('user_transaction.early_capital_withdraw', $params),
            273 => trans('user_transaction.early_capital_withdraw_fees', $params),

            301 => trans('account_transaction.assured_earning_profit'),
            302 => trans('account_transaction.assured_earning_reserved'),
            311 => trans('account_transaction.structured_earning_profit'),
            312 => trans('account_transaction.structured_earning_reserved'),
        ];
    }

    public function explainTransactionType()
    {
        $langs = static::getTransactionTypeLists($this->params);

        return $langs[$this->transaction_type] ?? 'UNKNOWN';
    }

    public static function getVerifiedLists(): array
    {
        return getYesNoForSelect();
    }

    public function canVerify(): bool
    {
        if (! $this->verified && in_array($this->transaction_type, [2])) {
            return true;
        }

        return false;
    }

    public function triggerVerified(): void
    {
        if ($this->amount > 0) {
            Account::where('id', '=', $this->account_id)->increment('account_balance', abs($this->amount));
        } elseif ($this->amount < 0) {
            Account::where('id', '=', $this->account_id)->decrement('account_balance', abs($this->amount));
        }
    }

    public function toArray(): array
    {
        $arr = parent::toArray();
        $arr['can_verify'] = $this->canVerify();

        return $arr;
    }
}
