<?php

namespace App\Models;

use App\Traits\HasAllCache;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends BaseModel
{
    use HasFactory;
    use SoftDeletes;
    use HasAllCache;

    protected $casts = [
        'setting' => 'string',
        'setting_name' => 'string',
        'setting_value' => 'string',
        'setting_type' => 'string',
        'params' => 'array',
    ];

    public static function getSettingTypeLists()
    {
        return [
            'select' => trans('common.select'),
            'text' => trans('common.text'),
            'textarea' => trans('common.textarea'),
            'editor' => trans('common.editor'),
            'number' => trans('common.number'),
            'fund' => trans('common.fund'),
        ];
    }
}
