<?php

namespace App\Models;

use App\Traits\UploadFile;

class UserTopup extends BaseModel
{
    use UploadFile;

    const ORDER_NUMBER_PREFIX = 'TP';

    const ORDER_NUMBER_STARTING = 1000000;

    const ORDER_NUMBER_SUFFIX = '';

    protected $casts = [
        'order_number' => 'string',
        'admin_id' => 'integer',
        'user_id' => 'integer',
        'country_id' => 'integer',
        'credit_type' => 'integer',
        'topup_amount' => 'float',
        'conversion_rate' => 'float',
        'from_crypto_symbol' => 'string',
        'to_crypto_address' => 'string',
        'received_crypto_amount' => 'float',
        'status' => 'integer',
        'payment_method' => 'integer',
        'transaction_id' => 'string',
        'receipt' => 'string',
    ];

    public function fileColumns()
    {
        $arr = [];
        $arr['receipt'] = [
            'path' => '/user_topup_receipt/',
            'rename' => 'id',
        ];

        return $arr;
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($instance) {
            if (! $instance->status) {
                $instance->status = 0;
            }
            if (! $instance->related_key) {
                $instance->related_key = \Str::uuid();
            }

            if ($instance->from_crypto_symbol && $instance->conversion_rate) {
                $instance->received_crypto_amount = bcmul($instance->topup_amount, $instance->conversion_rate, 18);
            }

            $count = static::count() + 1;
            $instance->order_number = static::ORDER_NUMBER_PREFIX.(static::ORDER_NUMBER_STARTING + $count).static::ORDER_NUMBER_SUFFIX;
        });
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id', 'id')->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public static function getStatusLists()
    {
        return [
            0 => trans('common.pending'),
            1 => trans('common.completed'),
            2 => trans('common.canceled'),
        ];
    }

    public static function getCreditTypeLists()
    {
        return [
            1 => trans('common.credit_1'),
        ];
    }

    public function setRemarkAttribute($value)
    {
        $this->attributes['remark'] = trim(strip_tags($value));
    }

    public function setAdminRemarkAttribute($value)
    {
        $this->attributes['admin_remark'] = trim(strip_tags($value));
    }

    public function isDone()
    {
        return ! in_array($this->status, [0]);
    }

    public static function getPaymentMethodLists()
    {
        return [
            999 => trans('common.usdt'),
        ];
    }

    public function distributeCredit()
    {
        $user = $this->user;

        $ut = new UserTransaction();
        $ut->user_id = $user->id;
        $ut->credit_type = $this->credit_type;
        $ut->transaction_type = 101;
        $ut->amount = $this->topup_amount;
        $ut->related_key = $this->related_key;
        $ut->params = [
            'user_topup_id' => $this->id,
        ];
        $ut->save();

        User::where('id', '=', $user->id)
            ->increment('credit_'.$ut->credit_type, $ut->amount);
    }

    public function triggerTopupSuccess()
    {
        $this->distributeCredit();

        $deposit = Account::where('account_code', '=', Account::DEPOSIT_ACCOUNT_CODE)->orderBy('id', 'ASC')->first();

        $rk = $this->related_key;
        $params = [
            'user_topup_id' => $this->id,
            'username' => $this->user->username,
            'user_id' => $this->user->id,
        ];

        $deposit->creditToAccount(
            amount: $this->topup_amount,
            transaction_type: 201,
            created_by_admin: $this->admin,
            verified_by_admin: $this->admin,
            verified: true,
            related_key: $rk,
            params: $params
        );
    }

    public function toArray()
    {
        $arr = parent::toArray();

        $arr['status_explained'] = $this->explainStatus();
        $arr['receipt'] = $this->__get('receipt');

        return $arr;
    }

    public function setTransactionIdAttribute($value)
    {
        $this->attributes['transaction_id'] = trim($value);
    }
}
