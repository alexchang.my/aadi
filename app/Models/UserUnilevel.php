<?php

namespace App\Models;

class UserUnilevel extends BaseModel
{
    public static function boot()
    {
        parent::boot();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function introducer()
    {
        return $this->belongsTo(User::class, 'introducer_user_id', 'id');
    }
}
