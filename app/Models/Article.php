<?php

namespace App\Models;

use App\Traits\HasAllCache;
use App\Traits\UploadFile;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends BaseModel
{
    use HasFactory;
    use SoftDeletes;
    use UploadFile;
    use HasAllCache;

    protected $casts = [
        'category' => 'string',
        'subject' => 'string',
        'content' => 'array',
        'description' => 'string',
        'cover' => 'string',
    ];

    public function fileColumns()
    {
        return [
            'cover' => [
                'path' => '/article/cover/',
                'rename' => 'id',
            ],
        ];
    }

    public static function getCategoryLists()
    {
        return [
            'news' => trans('common.news'),
            'help' => trans('common.help'),
            'learn' => trans('common.learn'),
        ];
    }
}
