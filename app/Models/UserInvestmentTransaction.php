<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserInvestmentTransaction extends BaseModel
{
    use HasFactory;

    protected $casts = [
        'user_id' => 'integer',
        'user_investment_id' => 'integer',
        'investment_plan_id' => 'integer',
        'distribution_batch_id' => 'integer',
        'transaction_type' => 'integer',
        'amount' => 'float',
        'active' => 'integer',
        'is_roi' => 'integer',
        'need_pay_commission' => 'integer',
        'commission_paid' => 'integer',
        'bonus_date_at' => 'datetime',
    ];

    protected $dates = [
        'bonus_date_at',
    ];

    public function getTransactionTypeLists($params = [])
    {
        return [
            1 => trans('user_investment_transaction.initial_buy_in', $params),
            2 => trans('user_investment_transaction.topup_buy_in', $params),

            101 => trans('user_investment_transaction.roi', $params),
            102 => trans('user_investment_transaction.roi_loss', $params),

            251 => trans('user_transaction.roi_withdraw', $params),
            253 => trans('user_investment_transaction.early_capital_withdraw_roi_burn', $params),

            271 => trans('user_transaction.capital_withdraw', $params),
            272 => trans('user_transaction.early_capital_withdraw', $params),
        ];
    }

    public function explainTransactionType()
    {
        $lists = $this->getTransactionTypeLists($this->params);
        if (isset($lists[$this->transaction_type])) {
            return $lists[$this->transaction_type];
        } else {
            return trans('common.unknown');
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function userInvestment()
    {
        return $this->belongsTo(UserInvestment::class, 'user_investment_id', 'id');
    }

    public function investmentPlan()
    {
        return $this->belongsTo(InvestmentPlan::class, 'investment_plan_id', 'id');
    }

    public function distributionBatch()
    {
        return $this->belongsTo(DistributionBatch::class, 'distribution_batch_id', 'id');
    }
}
