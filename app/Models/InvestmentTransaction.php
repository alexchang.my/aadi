<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class InvestmentTransaction extends BaseModel
{
    use HasFactory;

    protected $casts = [
        'investment_plan_id' => 'integer',
        'user_investment_id' => 'integer',
        'user_id' => 'integer',
        'transaction_type' => 'integer',
        'amount' => 'float',
        'status' => 'integer',
    ];

    public function getTransactionTypeLists($params = [])
    {
        return [
            1 => trans('investment_transaction.fund_approved', $params),
            2 => trans('investment_transaction.capital_early_withdrawal', $params),
            3 => trans('investment_transaction.fund_matured', $params),
        ];
    }

    public function explainTransactionType()
    {
        $lists = $this->getTransactionTypeLists($this->params);
        if (isset($lists[$this->transaction_type])) {
            return $lists[$this->transaction_type];
        } else {
            return trans('common.unknown');
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function userInvestment()
    {
        return $this->belongsTo(UserInvestment::class, 'user_investment_id', 'id');
    }

    public function investmentPlan()
    {
        return $this->belongsTo(InvestmentPlan::class, 'investment_plan_id', 'id');
    }

    public function scopePending($query)
    {
        return $query->where('status', '=', 0);
    }

    public function scopeAccepted($query)
    {
        return $query->where('status', '=', 1);
    }

    public static function getStatusLists()
    {
        return [
            0 => trans('common.pending'),
            1 => trans('common.accepted'),
        ];
    }
}
