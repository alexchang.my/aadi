<?php

namespace App\Models;

use App\Managers\Calculator;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserInvestment extends BaseModel
{
    use SoftDeletes;

    const CONTRACT_ID_START = 10000;

    const STATUS_PENDING = 0;

    const STATUS_COMPLETED = 1;

    const STATUS_PROCESSING = 2;

    const STATUS_WAITING = 3;
    const STATUS_EARLY_WITHDRAWAL = 4;

    protected $casts = [
        'contract_id' => 'string',
        'user_id' => 'integer',
        'investment_plan_id' => 'integer',
        'plan_type' => 'string',
        'code' => 'string',
        'plan_name' => 'string',
        'total_invest' => 'float',
        'current_balance' => 'float',
        'active_balance' => 'float',
        'total_roi' => 'float',
        'roi_balance' => 'float',
        'opt_in_min_amount' => 'float',
        'auto_opt_out_amount' => 'float',
        'already_auto_opt_out' => 'integer',
        'status' => 'integer',
        'opt_in' => 'integer',
        'lock_capital_from' => 'datetime',
        'lock_capital_until' => 'datetime',
        'setting' => 'array',
        'fund_approve_at' => 'datetime',
        'opt_in_at' => 'datetime',
        'last_distribute_roi_at' => 'datetime',
    ];

    protected $dates = [
        'lock_capital_from',
        'lock_capital_until',
        'fund_approve_at',
        'last_distribute_roi_at',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($instance) {
            if (! $instance->plan_type) {
                $instance->plan_type = $instance->setting['plan_type'];
            }

            if ($instance->plan_type == 'structured') {
                $instance->calculateOptInMinAmount();
                $instance->calculateAutoOptOutAmount();
            }

            $instance->already_auto_opt_out = 0;
        });
        static::created(function ($instance) {
            $instance->contract_id = $instance->code.'-'.(self::CONTRACT_ID_START + $instance->id);
            $instance->save();

            $t = new InvestmentTransaction();
            $t->investment_plan_id = $instance->investment_plan_id;
            $t->user_investment_id = $instance->id;
            $t->user_id = $instance->user_id;
            $t->transaction_type = 1;
            $t->status = 0;
            $t->amount = $instance->total_invest;
            $t->related_key = $instance->related_key;
            $t->params = [
                'contract_id' => $instance->contract_id,
                'username' => $instance->user->username,
                'member_id' => $instance->user->member_id,
            ];
            $t->save();
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function investmentPlan()
    {
        return $this->belongsTo(InvestmentPlan::class, 'investment_plan_id', 'id');
    }

    public function transactions()
    {
        return $this->hasMany(UserInvestmentTransaction::class, 'user_investment_id', 'id');
    }

    public function withdrawnTransactions()
    {
        return $this->hasMany(UserInvestmentTransaction::class, 'user_investment_id', 'id')->whereIn('transaction_type', [251]);
    }

    public static function getStatusLists()
    {
        return [
            static::STATUS_PENDING => trans('common.pending'),
            static::STATUS_COMPLETED => trans('common.completed'),
            static::STATUS_PROCESSING => trans('common.accepted'),
            static::STATUS_WAITING => trans('common.waiting'),
            static::STATUS_EARLY_WITHDRAWAL => trans('common.early_withdrawal'),
        ];
    }

    public static function getOptInLists()
    {
        return getYesNoForSelect();
    }

    public function setSettingAttribute($value)
    {
        $this->attributes['setting'] = json_encode($value);
    }

    public function getSettingAttribute($value)
    {
        return json_decode($value, true);
    }

    public function fillInvestmentPlan(InvestmentPlan $plan)
    {
        $this->investment_plan_id = $plan->id;
        $this->code = $plan->code;
        $this->plan_name = $plan->plan_name;
        $this->setting = $plan->setting;
        $this->plan_type = $plan->setting['plan_type'];
    }

    public function scopeInactive($query)
    {
        return $query->where('status', '=', 0);
    }

    public function scopeActive($query)
    {
        return $query->where(function ($q) {
            $q->orWhere(function ($iq) {
                $iq->where('plan_type', '=', 'assured')
                    ->whereIn('status', [
                        static::STATUS_PROCESSING,
                        static::STATUS_WAITING,
                    ]);
            })->orWhere(function ($iq) {
                $iq->where('plan_type', '=', 'structured')
                    ->whereIn('status', [
                        static::STATUS_PROCESSING,
                        static::STATUS_WAITING,
                    ])
                    ->where('opt_in', '=', 1);
            });
        });
    }

    public function scopeActiveIgnoreOptInStatus($query)
    {
        return $query->whereIn('status', [
            static::STATUS_PROCESSING,
            static::STATUS_WAITING,
        ]);
    }

    public function scopeAssuredFund($query)
    {
        return $query->where('plan_type', '=', 'assured');
    }

    public function scopeStructuredFund($query)
    {
        return $query->where('plan_type', '=', 'structured');
    }

    public static function getPlanTypeLists()
    {
        return [
            'assured' => trans('common.assured'),
            'structured' => trans('common.structured'),
        ];
    }

    public static function getAlreadyAutoOptOutLists() {
        return getYesNoForSelect();
    }

    public function approveFund(): void
    {
        if ($this->status == 0) {
            $this->fund_approve_at = now();
            $this->status = 2;

            $t = InvestmentTransaction::where('user_id', '=', $this->user_id)
                ->where('user_investment_id', '=', $this->id)
                ->where('investment_plan_id', '=', $this->investment_plan_id)
                ->where('transaction_type', '=', 1);

            if (! $t->count()) {
                $t = new InvestmentTransaction();
                $t->investment_plan_id = $this->investment_plan_id;
                $t->user_investment_id = $this->id;
                $t->user_id = $this->user_id;
                $t->transaction_type = 1;
                $t->status = 1;
                $t->amount = $this->total_invest;
                $t->related_key = $this->related_key;
                $t->params = [
                    'contract_id' => $this->contract_id,
                    'username' => $this->user->username,
                    'member_id' => $this->user->member_id,
                ];
                $t->save();
            } else {
                $t->update(['status' => 1]);
            }

            if ($this->plan_type == 'structured') {
                $this->opt_in = 1;
                $this->opt_in_at = now();
            }

            $this->lock_capital_from = now();
            $this->lock_capital_until = now()->addDays($this->investmentPlan->setting['capital_lock_days'])->endOfDay();
        }
    }

    public function processOptOut()
    {
        $this->opt_in = 0;
    }

    public function processOptin()
    {
        $this->opt_in = 1;
        $this->opt_in_at = now();
        $this->calculateOptInMinAmount();
    }

    public function calculateOptInMinAmount()
    {
        $this->opt_in_min_amount = (new Calculator($this->current_balance))
            ->divide(100)->times(90)->getNumber();
    }

    public function calculateAutoOptOutAmount()
    {
        $this->auto_opt_out_amount = (new Calculator($this->current_balance))
            ->divide(100)->times(90)->getNumber();
    }

    //THIS WONT VERIFY AMOUNT SUFFICIENT, TAKE NOTE!!!!!!!!
    public function withdrawCapital()
    {
        $rk = \Str::uuid();
        $params = [
            'user_investment_id' => $this->id,
            'contract_id' => $this->contract_id,
            'username' => $this->user->username,
            'member_id' => $this->user->member_id,
        ];

        //EARLY CAPITAL WITHDRAWAL
        if ($this->lock_capital_until->isFuture()) {
            $tid = 272;
            $early = true;
        } else {
            $tid = 271;
            $early = false;
        }

        $uit = new UserInvestmentTransaction();
        $uit->user_id = $this->user->id;
        $uit->user_investment_id = $this->id;
        $uit->investment_plan_id = $this->investment_plan_id;
        $uit->transaction_type = $tid;
        $uit->amount = 0 - $this->current_balance;
        $uit->active = 1;
        $uit->is_roi = 0;
        $uit->related_key = $rk;
        $uit->params = $params;
        $uit->save();

        $ut = new UserTransaction();
        $ut->user_id = $this->user->id;
        $ut->credit_type = 2;
        $ut->transaction_type = $tid;
        $ut->amount = $this->current_balance;
        $ut->related_key = $rk;
        $ut->params = $params;
        $ut->save();

        User::where('id', '=', $this->user->id)
            ->increment('credit_'.$ut->credit_type, $ut->amount);

        //EARLY WITHDRAW CAPITAL FEES
        if ($early) {
            $fees = (new Calculator($this->current_balance))->percentage(20)->getNumber();
            if ($fees > 0) {
                //DEBIT USER C2
                $ut2 = new UserTransaction();
                $ut2->user_id = $this->user->id;
                $ut2->credit_type = 2;
                $ut2->transaction_type = 273;
                $ut2->amount = 0 - $fees;
                $ut2->related_key = $rk;
                $ut2->params = $params;
                $ut2->save();

                User::where('id', '=', $this->user->id)
                    ->decrement('credit_'.$ut2->credit_type, abs($fees));

                $fees_acc = Account::where('account_code', '=', Account::FEES_CHARGES_ACCOUNT_CODE)
                    ->orderBy('id', 'ASC')->first();

                $fees_acc->creditToAccount(
                    amount: $fees,
                    transaction_type: 273,
                    verified: true,
                    related_key: $rk,
                    params: $params
                );
            }
        }

        if ($this->plan_type == 'assured' && $early && $this->roi_balance > 0) {
            //ASSURED EARLY CAPITAL WITHDRAW WILL BURN ROI

            $uit = new UserInvestmentTransaction();
            $uit->user_id = $this->user->id;
            $uit->user_investment_id = $this->id;
            $uit->investment_plan_id = $this->investment_plan_id;
            $uit->transaction_type = 253;
            $uit->amount = 0 - $this->roi_balance;
            $uit->active = 1;
            $uit->is_roi = 1;
            $uit->related_key = $rk;
            $uit->params = $params;
            $uit->save();

            $acc = Account::where('account_code', '=', Account::FEES_CHARGES_ACCOUNT_CODE)
                ->orderBy('id', 'ASC')->first();

            $acc->creditToAccount(
                amount: $this->roi_balance,
                transaction_type: 253,
                verified: true,
                related_key: $rk,
                params: $params
            );
        } elseif ($this->roi_balance > 0) {
            $this->withdrawRoi($this->roi_balance);
        } elseif ($this->roi_balance < 0) {
            #NEGATIVE ROI
            $negative_roi = abs($this->roi_balance);

            if ($negative_roi > 0) {
                $roi_transaction = new UserTransaction();
                $roi_transaction->user_id = $this->user->id;
                $roi_transaction->credit_type = 2;
                $roi_transaction->transaction_type = 253;
                $roi_transaction->amount = 0 - $negative_roi;
                $roi_transaction->related_key = $rk;
                $roi_transaction->params = $params;
                $roi_transaction->save();

                User::where('id', '=', $this->user->id)
                    ->decrement('credit_' . $roi_transaction->credit_type, $negative_roi);

                #TODO: which account should the negative ROI credit to??

//                $acc = Account::where('account_code', '=', Account::FEES_CHARGES_ACCOUNT_CODE)
//                    ->orderBy('id', 'ASC')->first();
//
//                $acc->creditToAccount(
//                    amount: $fees,
//                    transaction_type: 252,
//                    verified: true,
//                    related_key: $rk,
//                    params: $params
//                );
            }
        }

        //FUND OUT TO ACCOUNT TOO
        if ($this->plan_type == 'assured') {
            $fund_acc = Account::where('account_code', '=', Account::ASSURED_ACCOUNT_CODE)
                ->orderBy('id', 'ASC')->first();
        } elseif ($this->plan_type == 'structured') {
            $fund_acc = Account::where('account_code', '=', Account::STRUCTURED_ACCOUNT_CODE)
                ->orderBy('id', 'ASC')->first();
        } else {
            throw new \Exception('Invalid plan type');
        }

        $fund_acc->debitFromAccount(
            amount: $this->current_balance,
            transaction_type: $tid,
            verified: true,
            related_key: $rk,
            params: $params
        );

        $it = new InvestmentTransaction();
        $it->investment_plan_id = $this->investment_plan_id;
        $it->user_investment_id = $this->id;
        $it->user_id = $this->user_id;
        $it->transaction_type = $early ? 2 : 3;
        $it->amount = 0 - $this->current_balance;
        $it->status = 1;
        $it->params = $params;
        $it->save();

        $this->opt_in = 0;
        $this->status = $early ? 4 : 1;
        $this->current_balance = 0; //CLEAR BALANCE BECAUSE IT IS CAPITAL WITHDRAWAL
        $this->roi_balance = 0; //CLEAR ROI SINCE IT WILL WITHDRAW ALL
    }

    //THIS WONT VERIFY AMOUNT SUFFICIENT, TAKE NOTE!!!!!!!!
    public function withdrawRoi($amount)
    {
        $rk = \Str::uuid();
        $params = [
            'user_investment_id' => $this->id,
            'contract_id' => $this->contract_id,
            'username' => $this->user->username,
            'member_id' => $this->user->member_id,
        ];

        $tid = 251;

        $uit = new UserInvestmentTransaction();
        $uit->user_id = $this->user->id;
        $uit->user_investment_id = $this->id;
        $uit->investment_plan_id = $this->investment_plan_id;
        $uit->transaction_type = $tid;
        $uit->amount = 0 - $amount;
        $uit->active = 1;
        $uit->is_roi = 1;
        $uit->related_key = $rk;
        $uit->params = $params;
        $uit->save();

        $ut = new UserTransaction();
        $ut->user_id = $this->user->id;
        $ut->credit_type = 2;
        $ut->transaction_type = $tid;
        $ut->amount = $amount;
        $ut->related_key = $rk;
        $ut->params = $params;
        $ut->save();

        User::where('id', '=', $this->user->id)
            ->increment('credit_'.$ut->credit_type, $ut->amount);

        if ($this->plan_type == 'assured' && ($this->lock_capital_until != null && $this->lock_capital_until->isFuture())) {
            //early withdraw ROI deduct 5% for assured
            $fees = (new Calculator($amount))->percentage(5)->getNumber();
            if ($fees > 0) {
                $ut2 = new UserTransaction();
                $ut2->user_id = $this->user->id;
                $ut2->credit_type = 2;
                $ut2->transaction_type = 252;
                $ut2->amount = 0 - $fees;
                $ut2->related_key = $rk;
                $ut2->params = $params;
                $ut2->save();

                User::where('id', '=', $this->user->id)
                    ->decrement('credit_'.$ut2->credit_type, abs($fees));

                $acc = Account::where('account_code', '=', Account::FEES_CHARGES_ACCOUNT_CODE)
                    ->orderBy('id', 'ASC')->first();

                $acc->creditToAccount(
                    amount: $fees,
                    transaction_type: 252,
                    verified: true,
                    related_key: $rk,
                    params: $params
                );
            }
        }

        $this->roi_balance -= $amount;
    }

    public function toArray()
    {
        $arr = parent::toArray();

        $arr['status_explained'] = $this->explainStatus();

        return $arr;
    }
}
