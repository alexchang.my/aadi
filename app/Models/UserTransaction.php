<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserTransaction extends BaseModel
{
    use HasFactory;

    protected $casts = [
        'user_id' => 'integer',
        'credit_type' => 'integer',
        'transaction_type' => 'integer',
        'before' => 'float',
        'amount' => 'float',
        'after' => 'float',
        'is_bonus_type' => 'string',
        'adjusted_description' => 'string',
        'related_key' => 'string',
        'params' => 'array',
        'bonus_date_at' => 'datetime',
    ];

    protected $dates = [
        'bonus_date_at',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($q) {
            $last = static::where('user_id', '=', $q->user_id)
                ->where('credit_type', '=', $q->credit_type)
                ->orderBy('id', 'DESC')
                ->first();

            if ($last) {
                $q->before = $last->after;
            } else {
                $q->before = 0;
            }

            if ($q->amount > 0) {//CREDIT
                $q->after = bcadd($q->before, $q->amount, 5);
            } else {//DEBIT
                $q->after = bcsub($q->before, abs($q->amount), 5);
            }
        });
    }

    public static function getCreditTypeLists()
    {
        return [
            1 => trans('user.credit_1'),
            2 => trans('user.credit_2'),
        ];
    }

    public function getTransactionTypeLists($params = [])
    {
        return [
            101 => trans('user_transaction.topup', $params),

            151 => trans('user_transaction.transfer_credit_out', $params),
            152 => trans('user_transaction.transfer_credit_in', $params),

            201 => trans('user_transaction.withdraw', $params),
            202 => trans('user_transaction.withdraw_refund', $params),

            251 => trans('user_transaction.roi_withdraw', $params),
            252 => trans('user_transaction.early_roi_withdraw_fees', $params),
            253 => trans('user_transaction.roi_negative', $params),

            271 => trans('user_transaction.capital_withdraw', $params),
            272 => trans('user_transaction.early_capital_withdraw', $params),
            273 => trans('user_transaction.early_capital_withdraw_fees', $params),

            301 => trans('user_transaction.plan_buy_in', $params),

            401 => trans('user_transaction.roi_direct_sponsor_bonus', $params),

            501 => trans('user_transaction.roi_bmb', $params),

            601 => trans('user_transaction.roi_bmi', $params),

            991 => trans('user_transaction.admin_credit', $params),
            992 => trans('user_transaction.admin_debit', $params),
        ];
    }

    public function explainTransactionType()
    {
        if ($this->adjusted_description) return $this->adjusted_description;
        $lists = $this->getTransactionTypeLists($this->params);
        if (isset($lists[$this->transaction_type])) {
            return $lists[$this->transaction_type];
        } else {
            return trans('common.unknown');
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public static function getIsBonusTypeLists()
    {
        return [
            'assured' => trans('common.assured'),
            'structured' => trans('common.structured'),
        ];
    }
}
