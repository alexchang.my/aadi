<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @Deprecated
 * not using anymore
 */
class DistributionStage extends BaseModel
{
    use HasFactory;
    use SoftDeletes;

    protected $casts = [
        'plan_type' => 'string',
        'stage' => 'integer',
    ];
}
