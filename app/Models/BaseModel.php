<?php

namespace App\Models;

use App\Traits\HasAuditTrail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class BaseModel extends Model
{
    use HasAuditTrail;

    public bool $disabled_audit_trail = false;

    public function getAuditTrailDescription(): string
    {
        $c = explode('\\', get_class($this));

        return end($c).'(#'.$this->id.')';
    }

    public function __call($method, $args)
    {
        if (Str::startsWith($method, 'explain')) {
            $name = substr(Str::camel($method), 7);
            $field = Str::snake($name);

            $m = 'get'.ucfirst(substr(Str::camel($method), 7)).'Lists';
            if (method_exists(get_class($this), $m)) {
                $arr = $this->$m(...$args);

                return isset($arr[$this->$field]) ? $arr[$this->$field] : trans('common.unknown');
            } else {
                return parent::__call($method, $args);
            }
        } elseif (Str::startsWith($method, 'translate')) {
            $attr = Str::snake(substr($method, 9));

            $locale = app()->getLocale();
            if (isset($this->attributes[mb_strtolower($attr.'_'.$locale)])) {
                return $this->attributes[mb_strtolower($attr.'_'.$locale)];
            }

            $default_locale = config('app.locale');
            if (isset($this->attributes[mb_strtolower($attr.'_'.$default_locale)])) {
                return $this->attributes[mb_strtolower($attr.'_'.$default_locale)];
            }

            return parent::__call($method, $args);
        } else {
            return parent::__call($method, $args);
        }
    }

    public function setParamsAttribute($value)
    {
        if (is_array($value)) {
            $this->attributes['params'] = json_encode($value);
        } elseif (is_array(json_decode($value, true))) {
            $this->attributes['params'] = $value;
        } elseif (is_null($value)) {
            $this->attributes['params'] = null;
        } else {
            $this->attributes['params'] = json_encode($value);
        }
    }

    public function getParamsAttribute($value)
    {
        if (! $value) {
            return [];
        }
        $params = json_decode($value, true);
        foreach ($params as $key => $var) {
            if (is_array($var)) {
                unset($params[$key]);
            }
        }
//        if (!is_array($value)) {
//            $params = [];
//        }

        return $params;
    }

    public function setAttribute($key, $value)
    {
        $cc = Str::studly($key);
        $method = 'get'.$cc.'Lists';
        if (method_exists(get_called_class(), $method)) {
            $arr = $this->$method();
            if (! array_key_exists($value, $arr)) {
                throw new \Exception(trans('common.invalid_attribute', ['attribute' => $cc]));
            }
        }

        parent::setAttribute($key, $value);
    }
}
