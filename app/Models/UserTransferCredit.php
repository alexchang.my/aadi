<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserTransferCredit extends BaseModel
{
    use HasFactory;

    protected $casts = [
        'from_user_id' => 'integer',
        'to_user_id' => 'integer',
        'from_credit_type' => 'integer',
        'to_credit_type' => 'integer',
        'from_amount' => 'float',
        'to_amount' => 'float',
        'conversion_rate' => 'float',
        'related_key' => 'string',
        'params' => 'array',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($q) {
            $q->to_amount = bcmul($q->from_amount, $q->conversion_rate, \App\Config::DECIMAL_POINT);
        });
    }

    public function fromUser()
    {
        return $this->belongsTo(User::class, 'from_user_id', 'id');
    }

    public function toUser()
    {
        return $this->belongsTo(User::class, 'to_user_id', 'id');
    }
}
