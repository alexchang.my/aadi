<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class DistributionSetting extends BaseModel
{
    use HasFactory;

    protected $casts = [
        'admin_id' => 'integer',
        'plan_type' => 'string',
        'declare_fund' => 'float',
        'adjusted_percentage' => 'array',
        'approved' => 'integer',
        'status' => 'integer',
        'used_at' => 'datetime',
    ];

    protected $dates = [
        'used_at',
    ];

    public function admin()
    {
        $this->belongsTo(Admin::class, 'admin_id', 'id')->withTrashed();
    }

    public function setPlanTypeAttribute($value)
    {
        $this->attributes['plan_type'] = mb_strtolower($value);
    }

    public function setAdjustedPercentageAttribute($value)
    {
        $this->attributes['adjusted_percentage'] = json_encode($value);
    }

    public function getAdjustedPercentageAttribute($value)
    {
        return json_decode($value, true);
    }

    public static function getStatusLists()
    {
        return [
            0 => trans('common.pending'),
            1 => trans('common.used'),
            2 => trans('common.deleted'),
        ];
    }

    public function scopePending($query)
    {
        return $query->where('status', '=', 0);
    }

    public function scopeLatest($query)
    {
        return $query->orderBy('id', 'DESC');
    }
}
