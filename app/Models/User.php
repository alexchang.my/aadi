<?php

namespace App\Models;

use App\Jobs\RankCheck\CheckRank1;
use App\Jobs\RankCheck\CheckRank2;
use App\Jobs\RankCheck\CheckRank3;
use App\Notifications\PasswordReset;
use App\Traits\HasContactNumber;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasContactNumber;

    const DEFAULT_PASSWORD = 'qweasd';

    const DEFAULT_PASSWORD2 = 123123;

    const MEMBER_ID_START = 1477000;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [

    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'password2',
        'remember_token',
    ];

    protected $dates = [
        'last_login_at',
        'last_dispatch_ranking_check',
        'last_complete_ranking_check',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'id' => 'integer',
        'member_id' => 'integer',
        'username' => 'string',
        'name' => 'string',
        'email' => 'string',
        'email_verified_at' => 'datetime',
        'password' => 'string',
        'country_id' => 'integer',
        'contact_country_id' => 'integer',
        'contact_number' => 'string',
        'full_contact_number' => 'string',
        'introducer_user_id' => 'integer',
        'unilevel' => 'integer',
        'lang' => 'string',
        'avatar' => 'string',
        'credit_1' => 'float',
        'credit_2' => 'float',
        'credit_3' => 'float',
        'credit_4' => 'float',
        'credit_5' => 'float',
        'group_sales' => 'float',
        'ranking' => 'integer',
        'team_ranking_1' => 'integer',
        'team_ranking_2' => 'integer',
        'team_ranking_3' => 'integer',
        'bmb_count' => 'integer',
        'bmi_count' => 'integer',
        'last_dispatch_ranking_check' => 'datetime',
        'last_complete_ranking_check' => 'datetime',
        'last_ranking_check_set_rank' => 'integer',
        'last_ranking_1_check_at' => 'datetime',
        'last_ranking_2_check_at' => 'datetime',
        'last_ranking_3_check_at' => 'datetime',
        'bank_id' => 'integer',
        'bank_account_name' => 'string',
        'bank_account_number' => 'string',
        'national_id' => 'string',
        'first_login' => 'integer',
        'ban_until' => 'datetime',
        'new_login_at' => 'datetime',
        'last_login_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($instance) {
            if (! $instance->password) {
                $instance->password = static::DEFAULT_PASSWORD;
            }
            if (! $instance->password2) {
                $instance->password2 = static::DEFAULT_PASSWORD2;
            }
            if (! $instance->last_login_at) {
                $instance->last_login_at = now();
            }
            $instance->last_ranking_1_check_at = now();
            $instance->last_ranking_2_check_at = now();
            $instance->last_ranking_3_check_at = now();
        });
        static::created(function ($instance) {
            $instance->member_id = self::MEMBER_ID_START + $instance->id;
            $instance->save();
        });
    }

    public function introducer()
    {
        return $this->belongsTo(User::class, 'introducer_user_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function bank()
    {
        return $this->hasOne(Bank::class, 'id', 'bank_id');
    }

    public function transactions()
    {
        return $this->hasMany(UserTransaction::class, 'user_id', 'id');
    }

    public function withdrawals()
    {
        return $this->hasMany(UserWithdrawal::class, 'user_id', 'id');
    }

    public function transferCreditsFrom()
    {
        return $this->hasMany(UserTransferCredit::class, 'from_user_id', 'id');
    }

    public function transferCreditsTo()
    {
        return $this->hasMany(UserTransferCredit::class, 'to_user_id', 'id');
    }

    public function investments()
    {
        return $this->hasMany(UserInvestment::class, 'user_id', 'id');
    }

    public function investmentTransactions()
    {
        return $this->hasMany(UserInvestmentTransaction::class, 'user_id', 'id');
    }

    public function activeInvestments()
    {
        return $this->hasMany(UserInvestment::class, 'user_id', 'id')->Active();
    }

    public function activeInvestmentsIgnoreOptInStatus()
    {
        return $this->hasMany(UserInvestment::class, 'user_id', 'id')->ActiveIgnoreOptInStatus();
    }

    public function directDownlines()
    {
        return $this->hasMany(User::class, 'introducer_user_id', 'id');
    }

    public function unilevels()
    {
        return $this->hasMany(UserUnilevel::class, 'user_id', 'id');
    }

    public function uniqueUnilevels()
    {
        return $this->hasMany(UserUnilevel::class, 'user_id', 'id')->groupBy('introducer_user_id');
    }

    public function downlines()
    {
        return $this->hasMany(UserUnilevel::class, 'introducer_user_id', 'id');
    }

    public function uniqueDownlines()
    {
        return $this->hasMany(UserUnilevel::class, 'introducer_user_id', 'id')->groupBy('user_id');
    }

    //FIND $ID is in same hierarchy
    public function userInSameHierarchy($id)
    {
        return static::where('id', '=', $this->id)
            ->where(function ($q) use ($id) {
                $q->orWhere(function ($iq) use ($id) {
                    $iq->whereHas('sponsorUnilevels', function ($iiq) use ($id) {
                        $iiq->where('user_id', '=', $id);
                    });
                })->orWhere(function ($iq) use ($id) {
                    $iq->whereHas('unilevels', function ($iiq) use ($id) {
                        $iiq->where('sponsor_user_id', '=', $id);
                    });
                });
            })->exists();
    }

    //FIND $ID downline in my X GEN, false if not the same hierarchy
    public function userIdIsXGenOfMe($id)
    {
        $tree = $this->sponsorUnilevels()->where('user_id', '=', $id)->first();

        if (! $tree) {
            return false;
        }

        if ($tree->sponsor_unilevel > $tree->user_unilevel) {
            return false;
        }

        return $tree->user_unilevel - $tree->sponsor_unilevel;
    }

    //FIND $ID is X GEN upline, false if not the same hierarchy
    public function meIsXGenOfUserId($id)
    {
        $tree = $this->unilevels()->where('sponsor_user_id', '=', $id)->first();

        if (! $tree) {
            return false;
        }

        if ($tree->sponsor_unilevel > $tree->user_unilevel) {
            return false;
        }

        return $tree->user_unilevel - $tree->sponsor_unilevel;
    }

    //FIND all downline IDS
    public function getUnilevelDownlinesIds()
    {
        return $this->uniqueDownlines()->pluck('user_id');
    }

    //FIND all upline IDS
    public function getUnilevelUplinesIds()
    {
        return $this->unilevels()->pluck('sponsor_user_id');
    }

    //COUNT DOWNLINE PAX
    public function countUnilevelDownlinePaxs()
    {
        return $this->uniqueDownlines()->count();
    }

    //COUNT UPLINE PAX
    public function countUnilevelUplinePaxs()
    {
        return $this->uniqueUnilevels()->count();
    }

    //Find specified gen upline
    public function findXGenUser($gen)
    {
        return static::whereHas('uniqueDownlines', function ($iiq) use ($gen) {
            $iiq->where('user_id', '=', $this->id)
                ->where('introducer_unilevel', '=', $this->unilevel - $gen);
        })->first();
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }

    public function getCredit($id)
    {
        $field = 'credit_'.$id;

        return $this->$field;
    }

    public function isOrigin()
    {
        return $this->username == 'origin' || $this->id == 1;
    }

    public function setUsernameAttribute($value)
    {
        $this->attributes['username'] = sanitizeUsername($value);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = sanitizePassword($value);
    }

    public function setPassword2Attribute($value)
    {
        $this->attributes['password2'] = sanitizePassword($value);
    }

    public function correctPassword($password)
    {
        return password_verify($password, $this->password);
    }

    public function correctPassword2($password)
    {
        return password_verify($password, $this->password2);
    }

    public function getAvatar()
    {
        return '/img/avatar.png';
    }

    public function getIdentity()
    {
        return $this->name ?? $this->username;
    }

    public function checkUserRanking()
    {
        self::where('id', '=', $this->id)
            ->update([
               'last_dispatch_ranking_check' => now(),
            ]);
        dispatch(new CheckRank1($this));
        dispatch(new CheckRank2($this));
        dispatch(new CheckRank3($this));
    }

    public static function getRankingLists()
    {
        return [
            0 => trans('common.ranking_0'),
            1 => trans('common.ranking_1'),
            2 => trans('common.ranking_2'),
            3 => trans('common.ranking_3'),
        ];
    }

    public function explainRanking()
    {
        return static::getRankingLists()[$this->ranking] ?? trans('common.unknown');
    }

    public function isActive()
    {
        if ($this->credit_1 > 0) {
            return true;
        }

        return $this->activeInvestments()->sum('current_balance');
    }

    public function getActiveDirectPax()
    {
        return $this->directDownlines()->where(function ($q) {
            $q->orWhere('credit_1', '>', 0)
                ->orWhereHas('activeInvestments', function ($iq) {
                });
        })->count();
    }

    public function getDsbPercentage()
    {
        $active_direct_pax = $this->getActiveDirectPax();
        if ($active_direct_pax >= 5) {
            return 15;
        } elseif ($active_direct_pax >= 4) {
            return 14;
        } elseif ($active_direct_pax >= 3) {
            return 13;
        } elseif ($active_direct_pax >= 2) {
            return 12;
        } elseif ($active_direct_pax >= 1) {
            return 11;
        } else {
            return 0;
        }
    }

    public function getBmbPercentage()
    {
        if ($this->ranking >= 3) {
            return 5;
        } elseif ($this->ranking >= 2) {
            return 4;
        } elseif ($this->ranking >= 1) {
            return 3;
        } else {
            return 0;
        }
    }

    public function getBmiPercentage()
    {
        if ($this->ranking >= 3) {
            return 20;
        } elseif ($this->ranking >= 2) {
            return 15;
        } elseif ($this->ranking >= 1) {
            return 10;
        } else {
            return 0;
        }
    }
}
