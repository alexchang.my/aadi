<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class TimeTable extends BaseModel
{
    use HasFactory;

    protected $casts = [
        'utc_date' => 'date',
        'utc_day' => 'string',

        'assured_roi_day' => 'integer',
        'structured_roi_day' => 'integer',
        'assured_roi_paid' => 'integer',
        'structured_roi_paid' => 'integer',
        'assured_roi_commission_paid' => 'integer',
        'structured_roi_commission_paid' => 'integer',
        'assured_roi_fund_approved' => 'integer',
        'structured_roi_fund_approved' => 'integer',

        'daily_report_generated' => 'integer',
    ];

    protected $dates = [
        'utc_date',
    ];

    public function setUtcDayAttribute($value)
    {
        $this->attributes['utc_day'] = mb_strtolower($value);
    }

    public function getTimeFrom()
    {
        return $this->utc_date->copy()->startOfDay();
    }

    public function getTimeTo()
    {
        return $this->utc_date->copy()->endOfDay();
    }

    public static function canOperateSensitiveAction()
    {
        if (config('env.APP_DEBUG')) return true;
        return ! in_array(now('utc')->hour, [16, 17, 18, 19, 20, 21, 22, 23]);
    }

    public static function getOperateSensitiveActionErrormessage()
    {
        return 'system is in auto upkeep process, no further data shall allow to add in within this period.  Please try again after 8.00am';
    }
}
