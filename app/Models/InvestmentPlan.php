<?php

namespace App\Models;

use App\Traits\HasAllCache;
use App\Traits\UploadFile;

class InvestmentPlan extends BaseModel
{
    use UploadFile;
    use HasAllCache;

    protected $casts = [
        'code' => 'string',
        'plan_name' => 'string',
        'featured_photo' => 'string',
        'category_icon' => 'string',
        'return_percentage_from' => 'string',
        'return_percentage_to' => 'string',
        'return_cycle' => 'string',
        'fund_size' => 'string',
        'fund_description' => 'array',
        'setting' => 'array',
    ];

    public function fileColumns()
    {
        $arr = [];
        $arr['featured_photo'] = [
            'path' => '/investment_plan/featured/',
            'rename' => 'id',
        ];
        $arr['category_icon'] = [
            'path' => '/investment_plan/category/',
            'rename' => 'id',
        ];

        return $arr;
    }

    public function userInvestments()
    {
        return $this->hasMany(UserInvestment::class, 'investment_plan_id', 'id');
    }

    public function transactions()
    {
        return $this->hasMany(InvestmentTransaction::class, 'investment_plan_id', 'id');
    }

    public function setSettingAttribute($value)
    {
        $this->attributes['setting'] = json_encode($value);
    }

    public function getSettingAttribute($value)
    {
        return json_decode($value, true);
    }

    public function toArray()
    {
        $arr = parent::toArray();

        //TODO: add in all hard code value

        $arr['featured_photo'] = $this->__get('featured_photo');
        $arr['category_icon'] = $this->__get('category_icon');

        return $arr;
    }
}
