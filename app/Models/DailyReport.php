<?php

namespace App\Models;

use App\Managers\Calculator;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DailyReport extends BaseModel
{
    use HasFactory;

    protected $casts = [
        'report_date' => 'date',
        'report_datetime_from' => 'datetime',
        'report_datetime_to' => 'datetime',
        'plan_type' => 'string',
        'in_fund' => 'float',
        'out_fund' => 'float',
        'generated_fund' => 'float',
        'current_fund_size' => 'float',
        'total_investor_capital' => 'float',
        'declared_fund_size' => 'float',
        'profit_loss' => 'float',
        'payable_allocation' => 'float',
        'actual_payable' => 'float',
        'accumulate_allocation_balance' => 'float',
        'is_assured_roi_day' => 'integer',
        'is_structured_roi_day' => 'integer',
    ];

    protected $dates = [
        'report_date',
        'report_datetime_from',
        'report_datetime_to',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($instance) {
            $last_report = DailyReport::whereDate('report_date', '<', $instance->report_date->copy()->format('Y-m-d'))->orderBy('id', 'DESC')->first();

            if ($last_report) {
                $instance->report_datetime_from = $last_report->report_datetime_to->addSeconds(1);
            } else {
                $instance->report_datetime_from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', '2022-12-01 00:00:00');
            }

            $instance->report_datetime_to = $instance->report_date->endOfDay();

            $instance->calculateReport();
        });
    }

    public function scopeAssured($query)
    {
        return $query->where('plan_type', '=', 'assured');
    }

    public function scopeStructured($query)
    {
        return $query->where('plan_type', '=', 'structured');
    }

    public static function getPlanTypeLists()
    {
        return [
            'assured' => trans('common.assured'),
            'structured' => trans('common.structured'),
        ];
    }

    public function setPlanTypeAttribute($value)
    {
        $this->attributes['plan_type'] = mb_strtolower($value);
    }

    public function calculateAssuredReport()
    {
        $last_report = DailyReport::where('report_datetime_to', '<', $this->report_datetime_from)
            ->where('plan_type', '=', $this->plan_type)
            ->orderBy('report_datetime_to', 'DESC')
            ->first();

        $time = TimeTable::whereDate('utc_date', '=', $this->report_date->copy()->format('Y-m-d'))
            ->first();

        $this->in_fund = AccountTransaction::whereHas('account', function ($q) {
            $q->where('account_code', '=', Account::ASSURED_ACCOUNT_CODE);
        })
            ->where('verified', '=', 1)
            ->where('transaction_type', '=', 2)
            ->where('verified_at', '>=', $this->report_datetime_from)
            ->where('verified_at', '<=', $this->report_datetime_to)
            ->sum('amount');
        $this->out_fund = AccountTransaction::whereHas('account', function ($q) {
            $q->where('account_code', '=', Account::ASSURED_ACCOUNT_CODE);
        })
            ->where('verified', '=', 1)
            ->where('transaction_type', '=', 1)
            ->where('verified_at', '>=', $this->report_datetime_from)
            ->where('verified_at', '<=', $this->report_datetime_to)
            ->sum('amount');

        $this->generated_fund = 0;
        $this->current_fund_size = 0;
        if ($last_report) {
            $this->current_fund_size = $last_report->current_fund_size;
        }
        $this->declared_fund_size = 0;
        $this->profit_loss = 0;
        $this->payable_allocation = 0;
        $this->actual_payable = 0;
        $this->accumulate_allocation_balance = 0;

        if ($time && $time->assured_roi_day == 1) {
            $this->is_assured_roi_day = 1;

            $today_distribution_setting = DistributionSetting::where('plan_type', '=', $this->plan_type)
                ->where('status', '=', 1)
                ->where('used_at', '>=', $this->report_datetime_from)
                ->where('used_at', '<=', $this->report_datetime_to)
                ->orderBy('id', 'DESC')
                ->first();

            if ($today_distribution_setting) {
                $this->declared_fund_size = $today_distribution_setting->declare_fund;
            } else {
                $this->declared_fund_size = $this->current_fund_size; //IF NO SETTING THEN USE THIS DEFAULT DECLARED FUND SIZE
            }

            $this->profit_loss = $this->declared_fund_size;
            if ($last_report) {
                $this->profit_loss -= $last_report->current_fund_size;

                if ($last_report->total_investor_capital > 0 && $last_report->current_fund_size > 0) {
                    $multiplier = (new Calculator($last_report->total_investor_capital))->divide($last_report->current_fund_size)->getNumber();
                    $this->payable_allocation = (new Calculator($this->profit_loss))
                        ->divide(2)
                        ->times($multiplier)
                        ->getNumber();
                }
            }

            $this->generated_fund = $this->profit_loss;
        }

        $this->current_fund_size += ($this->in_fund + $this->out_fund + $this->profit_loss);
        $this->total_investor_capital = UserInvestment::where('plan_type', '=', $this->plan_type)
            ->where('fund_approve_at', '<=', $this->report_datetime_to)
            ->Active()
            ->sum('current_balance');

        $this->actual_payable = UserTransaction::whereIn('transaction_type', [401, 501, 601])
            ->where('is_bonus_type', '=', $this->plan_type)
            ->where('bonus_date_at', '>=', $this->report_datetime_from)
            ->where('bonus_date_at', '<=', $this->report_datetime_to)
            ->sum('amount');

        $this->actual_payable += UserInvestmentTransaction::where('transaction_type', '=', 101)
            ->whereHas('userInvestment', function ($q) {
                $q->where('plan_type', '=', $this->plan_type);
            })
            ->where('bonus_date_at', '>=', $this->report_datetime_from)
            ->where('bonus_date_at', '<=', $this->report_datetime_to)
            ->sum('amount');

        $this->actual_payable += UserInvestmentTransaction::where('transaction_type', '=', 102)
            ->whereHas('userInvestment', function ($q) {
                $q->where('plan_type', '=', $this->plan_type);
            })
            ->where('bonus_date_at', '>=', $this->report_datetime_from)
            ->where('bonus_date_at', '<=', $this->report_datetime_to)
            ->sum('amount');

        if ($last_report) {
            $this->accumulate_allocation_balance = $last_report->accumulate_allocation_balance;
        }

        $this->accumulate_allocation_balance += $this->payable_allocation;
        $this->accumulate_allocation_balance -= $this->actual_payable;
    }

    public function calculateStructuredReport()
    {
        $last_report = DailyReport::where('report_datetime_to', '<', $this->report_datetime_from)
            ->where('plan_type', '=', $this->plan_type)
            ->orderBy('report_datetime_to', 'DESC')
            ->first();

        $time = TimeTable::whereDate('utc_date', '=', $this->report_date->copy()->format('Y-m-d'))
            ->first();

        $this->in_fund = AccountTransaction::whereHas('account', function ($q) {
            $q->where('account_code', '=', Account::STRUCTURED_ACCOUNT_CODE);
        })
            ->where('verified', '=', 1)
            ->where('transaction_type', '=', 2)
            ->where('verified_at', '>=', $this->report_datetime_from)
            ->where('verified_at', '<=', $this->report_datetime_to)
            ->sum('amount');
        $this->out_fund = AccountTransaction::whereHas('account', function ($q) {
            $q->where('account_code', '=', Account::STRUCTURED_ACCOUNT_CODE);
        })
            ->where('verified', '=', 1)
            ->whereIn('transaction_type', [1])
//            ->whereIn('transaction_type', [1,271,272])
            ->where('verified_at', '>=', $this->report_datetime_from)
            ->where('verified_at', '<=', $this->report_datetime_to)
            ->sum('amount');

        $this->generated_fund = 0;
        $this->current_fund_size = 0;
        if ($last_report) {
            $this->current_fund_size = $last_report->current_fund_size;
        }
        $this->declared_fund_size = 0;
        $this->profit_loss = 0;
        $this->payable_allocation = 0;
        $this->actual_payable = 0;
        $this->accumulate_allocation_balance = 0;

        $today_distribution_setting = DistributionSetting::where('plan_type', '=', $this->plan_type)
            ->where('status', '=', 1)
            ->where('used_at', '>=', $this->report_datetime_from)
            ->where('used_at', '<=', $this->report_datetime_to)
            ->orderBy('id', 'DESC')
            ->first();

        if ($time && $time->structured_roi_day == 1) {
            $this->is_structured_roi_day = 1;

            if ($today_distribution_setting) {
                $this->declared_fund_size = $today_distribution_setting->declare_fund;
            } else {
                $this->declared_fund_size = $this->current_fund_size; //IF NO SETTING THEN USE THIS DEFAULT DECLARED FUND SIZE
            }

            $this->profit_loss = $this->declared_fund_size;
            if ($last_report) {
                $this->profit_loss -= $last_report->current_fund_size;

                if ($last_report->total_investor_capital > 0 && $last_report->current_fund_size > 0) {
                    $multiplier = (new Calculator($last_report->total_investor_capital))->divide($last_report->current_fund_size)->getNumber();
                    $this->payable_allocation = (new Calculator($this->profit_loss))
                        ->divide(2)
                        ->times($multiplier)
                        ->getNumber();
                }
            }

            $this->generated_fund = $this->profit_loss;
        }

        $this->current_fund_size += ($this->in_fund + $this->out_fund + $this->profit_loss);

        $capital_model = UserInvestment::where('plan_type', '=', $this->plan_type)
            ->where('fund_approve_at', '<=', $this->report_datetime_to)
            ->Active()
            ->whereNotNull('opt_in_at');

        if ($today_distribution_setting) {
            $capital_model = $capital_model->where('opt_in_at', '<', $today_distribution_setting->created_at);
        }

        $this->total_investor_capital = $capital_model->sum('current_balance');

        $this->actual_payable = UserTransaction::whereIn('transaction_type', [401, 501, 601])
            ->where('is_bonus_type', '=', $this->plan_type)
            ->where('bonus_date_at', '>=', $this->report_datetime_from)
            ->where('bonus_date_at', '<=', $this->report_datetime_to)
            ->sum('amount');

        $this->actual_payable += UserInvestmentTransaction::where('transaction_type', '=', 101)
            ->where('is_roi', '=', 1)
            ->whereHas('userInvestment', function ($q) {
                $q->where('plan_type', '=', $this->plan_type);
            })
            ->where('bonus_date_at', '>=', $this->report_datetime_from)
            ->where('bonus_date_at', '<=', $this->report_datetime_to)
            ->sum('amount');

        if ($last_report) {
            $this->accumulate_allocation_balance = $last_report->accumulate_allocation_balance;
        }

        $this->accumulate_allocation_balance += $this->payable_allocation;
        $this->accumulate_allocation_balance -= $this->actual_payable;
    }

    public function calculateReport()
    {
        if ($this->plan_type == 'assured') {
            $this->calculateAssuredReport();
        } elseif ($this->plan_type == 'structured') {
            $this->calculateStructuredReport();
        } else {
            throw new \Exception('Invalid plan type, therefore calculate report failed');
        }
    }
}
