<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class DistributionBatch extends BaseModel
{
    use HasFactory;

    protected $table = 'distribution_batch';

    protected $casts = [
        'distribution_setting_id' => 'integer',
        'distribution_date' => 'datetime',
        'plan_type' => 'string',
        'total_capital' => 'float',
        'total_roi' => 'float',
    ];

    protected $dates = [
        'distribution_date',
    ];

    public function distributionSetting()
    {
        $this->hasOne(DistributionSetting::class, 'id', 'distribution_setting_id');
    }

    public function transactions()
    {
        $this->hasMany(UserInvestmentTransaction::class, 'distribution_batch_id', 'id');
    }
}
