<?php

namespace App\Models;

use App\Traits\HasAllCache;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Country extends BaseModel
{
    use HasFactory, HasAllCache;

    protected $table = 'country';

    public function states()
    {
        return $this->hasMany(CountryLocation::class, 'country_id', 'id')->whereNull('parent_id')->Sorted();
    }

    public function area()
    {
        return $this->hasMany(CountryLocation::class, 'country_id', 'id')->whereNotNull('parent_id');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = trim(ucfirst($value));
    }

    public function setIso2Attribute($value)
    {
        $this->attributes['iso2'] = trim(mb_strtoupper($value));
    }

    public function setIso3Attribute($value)
    {
        $this->attributes['iso3'] = trim(mb_strtoupper($value));
    }

    public static function getStatusLists()
    {
        return [
            0 => trans('common.inactive'),
            1 => trans('common.active'),
        ];
    }

    public function scopeActive($query)
    {
        return $query->whereIn('status', [1]);
    }

    public function cacheAllQuery()
    {
        return $this->Active()->with(['states', 'states.area'])->orderBy('name', 'ASC');
    }

    public static function getCountriesForSelect($cache = false)
    {
        $collections = $cache ? collect(static::loadAllFromCache()) : static::Active()->get();

        return $collections->pluck('name', 'id');
    }

    public static function getExtForSelect($cache = false)
    {
        $collections = $cache ? collect(static::loadAllFromCache()) : static::Active()->get();

        return $collections->pluck('ext', 'id');
    }

    public static function forgetCache()
    {
        cache()->forget((new self())->getAllCacheKey());
    }
}
