<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class AdminGroupPermission extends BaseModel
{
    use HasFactory;

    public function group()
    {
        return $this->belongsTo(AdminGroup::class, 'admin_group_id', 'id');
    }

    public static function getPermissionTagLists()
    {
        return include base_path('lang/'.app()->getLocale().'/permission.php');
    }

    public function setPermissionTagAttribute($value)
    {
        $this->attributes['permission_tag'] = trim(mb_strtolower($value));
    }
}
