<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class AdminGroup extends BaseModel
{
    use HasFactory;

    protected $with = [
        'permissions',
    ];

    public function permissions()
    {
        return $this->hasMany(AdminGroupPermission::class, 'admin_group_id', 'id');
    }

    public function admins()
    {
        return $this->hasMany(Admin::class, 'admin_group_id', 'id');
    }

    public static function getPermissionTagLists()
    {
        $file = include resource_path('lang/'.app()->getLocale().'/permission.php');

        return $file;
    }
}
