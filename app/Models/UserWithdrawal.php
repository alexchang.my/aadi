<?php

namespace App\Models;

use App\Traits\HasAuditTrail;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserWithdrawal extends BaseModel
{
    use HasFactory;
    use HasAuditTrail;

    const ORDER_NUMBER_PREFIX = 'W';

    const ORDER_NUMBER_STARTING = 1000000;

    const ORDER_NUMBER_SUFFIX = '';

    protected $casts = [
        'order_number' => 'string',
        'admin_id' => 'integer',
        'user_id' => 'integer',
        'country_id' => 'integer',
        'network' => 'string',
        'credit_type' => 'integer',
        'amount' => 'float',
        'conversion_rate' => 'float',
        'admin_fees_percentage' => 'float',
        'to_crypto_symbol' => 'string',
        'to_crypto_address' => 'string',
        'to_crypto_amount' => 'float',
        'admin_fees_crypto' => 'float',
        'receivable_crypto_amount' => 'float',
        'status' => 'integer',
        'remark' => 'string',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($instance) {
            if (! $instance->related_key) {
                $instance->related_key = \Str::uuid();
            }

            if ($instance->to_crypto_symbol) {
                $instance->to_crypto_amount = bcmul($instance->amount, $instance->conversion_rate, 18);
            }

            if ($instance->admin_fees_percentage > 0) {
                $instance->admin_fees_crypto = bcmul(bcdiv($instance->to_crypto_amount, 100, 18), $instance->admin_fees_percentage, 18);
            }

            $instance->receivable_crypto_amount = bcsub($instance->to_crypto_amount, ($instance->admin_fees_crypto ?? 0), 18);

            $count = static::count() + 1;
            $instance->order_number = static::ORDER_NUMBER_PREFIX.(static::ORDER_NUMBER_STARTING + $count).static::ORDER_NUMBER_SUFFIX;
        });
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function setToCryptoSymbolAttribute($value)
    {
        $this->attributes['to_crypto_symbol'] = mb_strtoupper($value);
    }

    public static function getStatusLists()
    {
        return [
            0 => trans('common.pending'),
            1 => trans('common.completed'),
            2 => trans('common.canceled'),
        ];
    }

    public function triggerWithdrawSuccess()
    {
        $opex = Account::where('account_code', '=', Account::OPEX_ACCOUNT_CODE)->orderBy('id', 'ASC')->first();

        $rk = $this->related_key;
        $params = [
            'user_withdrawal_id' => $this->id,
            'username' => $this->user->username,
            'user_id' => $this->user->id,
        ];

        $opex->debitFromAccount(
            amount: $this->amount,
            transaction_type: 202,
            created_by_admin: $this->admin,
            verified_by_admin: $this->admin,
            verified: true,
            related_key: $rk,
            params: $params
        );
    }

    public function toArray()
    {
        $arr = parent::toArray();

        $arr['status_explained'] = $this->explainStatus();

        return $arr;
    }

    public function setNetworkAttribute($value)
    {
        $this->attributes['network'] = trim(mb_strtoupper($value));
    }
}
