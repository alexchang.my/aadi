<?php

namespace App\Models;

use App\Traits\MultiLanguage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyBank extends BaseModel
{
    use HasFactory, MultiLanguage, SoftDeletes;

    public function multiLanguageColumns()
    {
        return [
            'name',
        ];
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function fillBank(Bank $bank)
    {
        $this->bank_id = $bank->id;
        foreach (loopLanguageForColumn('name') as $lang) {
            $this->{$lang['column']} = $bank->{$lang['column']};
        }
    }
}
