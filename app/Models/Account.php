<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends BaseModel
{
    use HasFactory;
    use SoftDeletes;

    /**
     * !!!!!!!!!!!! WARNING WARNING WARNING !!!!!!!!!!!!
     * IN MOST CASE
     * DON'T SAVE THIS MODEL WHEN THE account_balance is CHANGED
     * IT WILL AUTOMATICALLY increment/decrement by account_transactions created event
     */
    const DEPOSIT_ACCOUNT_CODE = 'DEPOSIT';

    const ASSURED_ACCOUNT_CODE = 'ASSURED';

    const STRUCTURED_ACCOUNT_CODE = 'STRUCTURED';

    const OPEX_ACCOUNT_CODE = 'OPEX';

    const PL_ACCOUNT_CODE = 'PL';

    const RESERVED_ACCOUNT_CODE = 'RESERVED';

    const FEES_CHARGES_ACCOUNT_CODE = 'FEES_CHARGES';

    protected $casts = [
        'account_code' => 'string',
        'account_name' => 'string',
        'account_balance' => 'float',
    ];

    public function transactions()
    {
        return $this->hasMany(AccountTransaction::class, 'account_id', 'id');
    }

    public function setAccountCodeAttribute($value)
    {
        $this->attributes['account_code'] = mb_strtoupper($value);
    }

    /**
     * @param  float  $amount
     * @param  int  $transaction_type
     * @param  bool  $verified
     * @param  Admin|null  $created_by_admin
     * @param  Admin|null  $verified_by_admin
     * @param  string|null  $remark
     * @param  string|null  $related_key
     * @param  array  $params
     * @return void
     */
    public function creditToAccount(float $amount, int $transaction_type, bool $verified = false, Admin|null $created_by_admin = null, Admin|null $verified_by_admin = null, string $remark = null, string $related_key = null, array $params = []): void
    {
        $t = new AccountTransaction();
        $t->account_id = $this->id;
        $t->transaction_type = $transaction_type;
        $t->amount = abs($amount);
        if ($created_by_admin) {
            $t->created_by_admin_id = $created_by_admin->id;
        }
        if ($verified_by_admin) {
            $t->verified_by_admin_id = $verified_by_admin->id;
        }
        if ($remark) {
            $t->remark = $remark;
        }
        $t->related_key = $related_key;
        $t->params = $params;
        if ($verified) {
            $t->verified = 1;
            $t->verified_at = now();

            $this->account_balance += abs($amount); //JUST UPDATE MODEL ITSELF, DO NOT SAVE
        }
        $t->save();
    }

    /**
     * @param  float  $amount
     * @param  int  $transaction_type
     * @param  bool  $verified
     * @param  Admin|null  $created_by_admin
     * @param  Admin|null  $verified_by_admin
     * @param  string|null  $remark
     * @param  string|null  $related_key
     * @param  array  $params
     * @return void
     */
    public function debitFromAccount(float $amount, int $transaction_type, bool $verified = false, Admin|null $created_by_admin = null, Admin|null $verified_by_admin = null, string $remark = null, string $related_key = null, array $params = []): void
    {
        $t = new AccountTransaction();
        $t->account_id = $this->id;
        $t->transaction_type = $transaction_type;
        $t->amount = bcsub(0, abs($amount));
        if ($created_by_admin) {
            $t->created_by_admin_id = $created_by_admin->id;
        }
        if ($verified_by_admin) {
            $t->verified_by_admin_id = $verified_by_admin->id;
        }
        if ($remark) {
            $t->remark = $remark;
        }
        $t->related_key = $related_key;
        $t->params = $params;
        if ($verified) {
            $t->verified = 1;
            $t->verified_at = now();

            $this->account_balance -= abs($amount); //JUST UPDATE MODEL ITSELF, DO NOT SAVE
        }
        $t->save();
    }

    public static function getAccountCodeLists()
    {
        return [
            static::DEPOSIT_ACCOUNT_CODE => trans('common.deposit_account'),
            static::ASSURED_ACCOUNT_CODE => trans('common.assured_account'),
            static::STRUCTURED_ACCOUNT_CODE => trans('common.structured_account'),
            static::OPEX_ACCOUNT_CODE => trans('common.opex_account'),
            static::PL_ACCOUNT_CODE => trans('common.pl_account'),
            static::RESERVED_ACCOUNT_CODE => trans('common.reserved_account'),
            static::FEES_CHARGES_ACCOUNT_CODE => trans('common.fees_charges_account'),
        ];
    }

    public static function accountAllowedActionLists()
    {
        return [
            static::DEPOSIT_ACCOUNT_CODE => [
                'transfer_to_assured_account',
                'transfer_to_structured_account',
                'debit',
                'credit',
            ],
            static::ASSURED_ACCOUNT_CODE => [
                'transfer_to_opex_account',
                'transfer_to_structured_account',
                'debit',
                'credit',
            ],
            static::STRUCTURED_ACCOUNT_CODE => [
                'transfer_to_opex_account',
                'transfer_to_assured_account',
                'debit',
                'credit',
            ],
            static::OPEX_ACCOUNT_CODE => [
                'debit',
                'credit',
            ],
            static::PL_ACCOUNT_CODE => [
                'debit',
                'credit',
            ],
            static::RESERVED_ACCOUNT_CODE => [
                'debit',
                'credit',
            ],
            static::FEES_CHARGES_ACCOUNT_CODE => [
                'debit',
                'credit',
            ],
        ];
    }

    public static function getAllowedActions($act)
    {
        $all = static::accountAllowedActionLists();
        if (isset($all[$act])) {
            return $all[$act];
        } else {
            return [];
        }
    }

    public function isTheActionAllowed($act)
    {
        return in_array($act, static::accountAllowedActionLists()[$this->account_code]) ?? false;
    }
}
