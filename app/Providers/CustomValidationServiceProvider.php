<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Validator;

class CustomValidationServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot()
    {
        Validator::extend('isNumber', function ($attribute, $value, $parameters) {
            return preg_match('/\D/', $value);
        });

        Validator::extend('isFund', function ($attribute, $value, $parameters) {
            return is_numeric($value);
        });

        Validator::extend('isContactNumber', function ($attribute, $value, $parameters) {
            return preg_match('/^[0-9]{9,12}$/', $value);
        });

        Validator::extend('isUsername', function ($attribute, $value, $parameters) {
            return preg_match('/^[A-Za-z0-9_]{5,36}$/', $value);
        });

        Validator::extend('isPassword', function ($attribute, $value, $parameters) {
            return preg_match('/^(.*?){6,24}$/', $value);
        });

        Validator::extend('isPassword2', function ($attribute, $value, $parameters) {
            return preg_match('/^[0-9]{6}$/', $value);
        });

        Validator::extend('isYesNo', function ($attribute, $value, $parameters) {
            return in_array($value, [0, 1]);
        });
    }
}
