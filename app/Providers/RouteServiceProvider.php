<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api/user')
                ->middleware('api-user')
                ->namespace($this->namespace)
                ->group(base_path('routes/api/user.php'));

            Route::prefix('api/admin')
                ->middleware('api-admin')
                ->namespace($this->namespace)
                ->group(base_path('routes/api/admin.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        $rate_limit = config('env.APP_ENV') === 'local' ? 20000 : 60;
        RateLimiter::for('api-user', function (Request $request) use ($rate_limit) {
            return Limit::perMinute($rate_limit)->by($request->user()?->id ?: $request->ip());
        });
        RateLimiter::for('api-admin', function (Request $request) use ($rate_limit) {
            return Limit::perMinute($rate_limit)->by($request->user()?->id ?: $request->ip());
        });
    }
}
