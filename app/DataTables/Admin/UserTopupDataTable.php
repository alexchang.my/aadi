<?php

namespace App\DataTables\Admin;

use App\Models\UserTopup;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class UserTopupDataTable extends DataTable
{
    public function getQuery(): Model|Builder|Collection|null
    {
        return (new UserTopup())->with(['user']);
    }

    public function getUnSortable(): array
    {
        return [
            //'unsortable_column1'
        ];
    }

    public function mappings(): array
    {
        return [
            'user_username' => function ($q) {
                return $q->user->username ?? '-';
            },
            'status_explained' => function ($q) {
                return $q->explainStatus();
            },
        ];
    }

    public function filters(): void
    {
        //if (request()->filled('f_username')) {
            //$this->model = $this->model->where('username', 'LIKE', '%' . request()->get('f_username') . '%');
        //}
    }

    public function permissions(): array
    {
        return [
            'manage_topup',
            //'manage_user',
            //'manage_user_advanced',
        ];
    }
}
