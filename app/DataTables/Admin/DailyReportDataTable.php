<?php

namespace App\DataTables\Admin;

use App\Models\DailyReport;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class DailyReportDataTable extends DataTable
{
    public function getQuery(): Model|Builder|Collection|null
    {
        return new DailyReport();
    }

    public function getUnSortable(): array
    {
        return [
            //'unsortable_column1'
        ];
    }

    public function mappings(): array
    {
        return [
            //'sample' => function ($q) {
            //return $q->model_column_name ?? '-';
            //}
        ];
    }

    public function filters(): void
    {
        //if (request()->filled('f_username')) {
            //$this->model = $this->model->where('username', 'LIKE', '%' . request()->get('f_username') . '%');
        //}
    }

    public function permissions(): array
    {
        return [
            'assured_daily_report',
            //'manage_user',
            //'manage_user_advanced',
        ];
    }
}
