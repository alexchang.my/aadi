<?php

namespace App\DataTables\Admin;

use App\Models\AccountTransaction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class AccountTransactionDataTable extends DataTable
{
    public function getQuery(): Model|Builder|Collection|null
    {
        return (new AccountTransaction())->with(['createdByAdmin', 'verifiedByAdmin']);
    }

    public function getUnSortable(): array
    {
        return [
            //'unsortable_column1'
        ];
    }

    public function mappings(): array
    {
        return [
            'created_by_admin_username' => function ($q) {
                return $q->createdByAdmin->username ?? '-';
            },
            'verified_by_admin_username' => function ($q) {
                return $q->verifiedByAdmin->username ?? '-';
            },
            'transaction_type_explained' => function ($q) {
                return $q->explainTransactionType();
            },
            //'sample' => function ($q) {
            //return $q->model_column_name ?? '-';
            //}
        ];
    }

    public function filters(): void
    {
        //if (request()->filled('f_username')) {
            //$this->model = $this->model->where('username', 'LIKE', '%' . request()->get('f_username') . '%');
        //}
    }

    public function permissions(): array
    {
        return [
            'deposit_account',
            'assured_account',
            'structured_account',
            'opex_account',
            'pl_account',
            'reserved_account',
        ];
    }
}
