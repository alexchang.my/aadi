<?php

namespace App\DataTables\Admin;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class UserDataTable extends DataTable
{
    public function getQuery(): Model|Builder|Collection|null
    {
        return (new User())->withSum([
            'activeInvestments as total_investments',
        ], 'current_balance')->with(['introducer']);
    }

    public function getUnSortable(): array
    {
        return [
            'total_investments',
        ];
    }

    public function mappings(): array
    {
        return [
            'total_investments' => function ($q) {
                return $q->total_investments ?? 0;
            },
            'gender' => function ($q) {
                if ($q->gender == 1) {
                    return trans('common.gender_male');
                } elseif ($q->gender == 2) {
                    return trans('common.gender_female');
                } else {
                    return trans('common.gender_unknown');
                }
            },
            'introducer_username' => function ($q) {
                return $q->introducer->username ?? '-';
            },
            'is_origin' => function ($q) {
                return $q->isOrigin();
            },
            'ranking_explained' => function ($q) {
                return $q->explainRanking();
            },
        ];
    }

    public function filters(): void
    {
        if (request()->filled('f_username')) {
            $this->model = $this->model->where('username', 'LIKE', '%'.request()->get('f_username').'%');
        }
    }

    public function permissions(): array
    {
        return [
            'manage_user', 'manage_user_advanced',
        ];
    }
}
