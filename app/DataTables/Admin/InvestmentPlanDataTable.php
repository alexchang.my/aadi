<?php

namespace App\DataTables\Admin;

use App\Models\InvestmentPlan;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class InvestmentPlanDataTable extends DataTable
{
    public function getQuery(): Model|Builder|Collection|null
    {
        return (new InvestmentPlan())->withSum(['transactions as current_balance' => function ($query) {
        }], 'amount');
    }

    public function getUnSortable(): array
    {
        return [
            'current_balance',
            //'unsortable_column1'
        ];
    }

    public function mappings(): array
    {
        return [
            'current_balance' => function ($q) {
                return $q->current_balance ?? 0;
            },
            //'sample' => function ($q) {
            //return $q->model_column_name ?? '-';
            //}
        ];
    }

    public function filters(): void
    {
        //if (request()->filled('f_username')) {
            //$this->model = $this->model->where('username', 'LIKE', '%' . request()->get('f_username') . '%');
        //}
    }

    public function permissions(): array
    {
        return [
            'manage_investment_plan',
            //'manage_user',
            //'manage_user_advanced',
        ];
    }
}
