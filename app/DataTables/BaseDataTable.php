<?php

namespace App\DataTables;

use App\Exports\CollectionExportFromCollection;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection as NormalCollection;
use Illuminate\Support\Str;

abstract class BaseDataTable
{
    public int $total_records = 0;

    public int $total_pages = 0;

    public int $ipp = 30;

    protected string $class = '';

    protected mixed $model = null;

    protected Collection|NormalCollection $records;

    protected string $sorted_column = 'id';

    protected string $sorted = 'DESC';

    protected array $default_export_ignore_column = [
        'actions', 'action',
    ];

    public function __construct()
    {
        if (request()->filled('ipp')) {
            $this->ipp = request()->get('ipp');
        }

        $this->model = $this->getQuery();

        $this->queryFilter();

        $this->querySorting();

        $this->queryCount();

        if (request()->get('export') == 1) {
        } else {
            $this->queryFetch(request()->get('p', 1));

            $this->transform();
        }
    }

    //COUNT PAGE AND OTHER DATA
    protected function queryCount()
    {
        $this->total_records = $this->model->count();
        if ($this->total_records > $this->ipp) {
            $this->total_pages = ceil($this->total_records / $this->ipp);
        } else {
            $this->total_pages = 1;
        }
    }

    //FETCH, FINALLY
    protected function queryFetch($page)
    {
        $this->records = $this->model->skip(($page - 1) * $this->ipp)->take($this->ipp)->get();
    }

    //DEFAULT FILTER
    protected function queryFilter()
    {
        /**
         * AUTO QUERY FILTER BY COLUMN NAME
         * 1. f-like-<username>
         * 2. f-has-like-<user>-<username>
         * 3. f-has-<user>-<username>
         * 4. f-date-from-<created_at>
         * 5. f-date-to-<created_at>
         * 6. f-date-<created_at>
         * 7. f-<username>
         */
        foreach (request()->all() as $field => $var) {
            if (! is_array($var) && mb_strlen($var) > 0) {
                $params = explode('-', $field);

                if (Str::startsWith($field, 'f-like-')) {
                    $this->model = $this->model->where($params[2], 'LIKE', '%'.$var.'%');
                } elseif (Str::startsWith($field, 'f-has-like-')) {
                    $this->model = $this->model->whereHas($params[3], function ($q) use ($params, $var) {
                        $q->where($params[4], 'LIKE', '%'.$var.'%');
                    });
                } elseif (Str::startsWith($field, 'f-has-')) {
                    $this->model = $this->model->whereHas($params[2], function ($q) use ($params, $var) {
                        $q->where($params[3], '=', $var);
                    });
                } elseif (Str::startsWith($field, 'f-date-from')) {
                    $this->model = $this->model->where($params[3], '>=', $var.' 00:00:00');
                } elseif (Str::startsWith($field, 'f-date-to')) {
                    $this->model = $this->model->where($params[3], '<=', $var.' 23:59:59');
                } elseif (Str::startsWith($field, 'f-date')) {
                    $this->model = $this->model
                        ->where($params[2], '>=', $var.' 00:00:00')
                        ->where($params[2], '<=', $var.' 23:59:59');
                } elseif (Str::startsWith($field, 'f-')) {
                    $this->model = $this->model->where($params[1], '=', $var);
                }
            }
        }

        //CUSTOM FILTER
        if (method_exists($this, 'filters')) {
            $this->filters();
        }
    }

    protected function querySorting()
    {
        if (request()->filled('sorting_column')) {
            if (method_exists($this, 'getUnSortable')) {
                if (! in_array(request()->get('sorting_column'), $this->getUnSortable())) {
                    $this->sorted_column = request()->get('sorting_column');
                }
            } else {
                $this->sorted_column = request()->get('sorting_column');
            }
        }

        if (in_array(mb_strtoupper(request()->get('sorting')), ['DESC', 'ASC'])) {
            $this->sorted = mb_strtoupper(request()->get('sorting'));
        }

        $this->model = $this->model->orderBy($this->sorted_column, $this->sorted);
    }

    protected function transform(): void
    {
        if (method_exists($this, 'mappings')) {
            foreach ($this->mappings() as $column => $cb) {
                for ($i = 0; $i < $this->records->count(); $i++) {
                    $this->records[$i][$column] = $cb($this->records[$i]);
                }
            }
        }
    }

    public function getExport()
    {
        $per_page = 5000;

        $rows = collect();
        $header_columns = [];
        $body_columns = [];

        foreach (request()->get('headers') as $header) {
            if (! isset($header['export_ignore']) || (isset($header['export_ignore']) && $header['export_ignore'] != true)) {
                if (isset($header['column'])) {
                    $column_name = $header['column'];
                } else {
                    $column_name = $header['name'];
                }

                if (! in_array($column_name, $this->default_export_ignore_column)) {
                    $header_columns[] = $header['label'];
                    $body_columns[] = $column_name;
                }
            }
        }

        $this->model->chunk($per_page, function ($records) use ($rows, $body_columns) {
            $this->records = $records;

            $this->transform();

            foreach ($this->records as $r) {
                $rows->add($r->only($body_columns));
            }
        });

        $c = new CollectionExportFromCollection($rows, $header_columns, $body_columns);

        return $c->download(request()->filled('export_file_name') ? request()->get('export_file_name') : now()->timestamp.'.csv');
    }

    public function getRecords(): Collection
    {
        return $this->records;
    }

    /**
     * @return Model|Builder|Collection|null
     */
    abstract public function getQuery(): Model|Builder|Collection|null;

    abstract public function getUnSortable(): array;

    abstract public function mappings(): array;

    abstract public function filters(): void;

    abstract public function permissions(): array;
}
