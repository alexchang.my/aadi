<?php

namespace App\DataTables\User;

use App\Models\UserTransaction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class UserTransactionDataTable extends DataTable
{
    public function getQuery(): Model|Builder|Collection|null
    {
        return (new UserTransaction())->where('user_id', '=', request()->user()->id);
    }

    public function getUnSortable(): array
    {
        return [
            //'unsortable_column1'
        ];
    }

    public function mappings(): array
    {
        return [
            'transaction_type_explained' => function ($q) {
                return $q->explainTransactionType();
            },
            //'sample' => function ($q) {
            //return $q->model_column_name ?? '-';
            //}
        ];
    }

    public function filters(): void
    {
        //if (request()->filled('f_username')) {
            //$this->model = $this->model->where('username', 'LIKE', '%' . request()->get('f_username') . '%');
        //}
    }

    public function permissions(): array
    {
        return [
            //'manage_user',
            //'manage_user_advanced',
        ];
    }
}
