<?php

namespace App\Console\Commands;

use App\Jobs\ApproveFund;
use App\Jobs\AssuredRoi\DistributeRoi as DistributeAssuredRoi;
use App\Jobs\DistributeCommission;
use App\Jobs\StructuredRoi\DistributeRoi as DistributeStructuredRoi;
use App\Models\TimeTable;
use Illuminate\Console\Command;
use Log;

class DailyWorker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily_worker
    {date? : Date in format Y-m-d}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto daily work';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            ini_set('max_execution_time', 0); //0=NOLIMIT

            $d = $this->argument('date');
            if ($d) {
                $date = \Carbon\Carbon::createFromFormat('Y-m-d', $d, 'utc')->startOfDay();
            } else {
                $date = now('utc')->startOfDay();
            }

            $time = TimeTable::whereDate('utc_date', '=', $date->format('Y-m-d'))
                ->orderBy('id', 'ASC')
                ->first();

            //TODAY THE DAY TO PAY ROI
            if ($time->assured_roi_day == 1 || $time->structured_roi_day == 1) {
                if (
                    ($time->assured_roi_day == 1 && $time->assured_roi_paid == 0) ||
                    ($time->structured_roi_day == 1 && $time->structured_roi_paid == 0)
                ) {
                    if ($time->assured_roi_day == 1 && $time->assured_roi_paid == 0) {
                        $this->info('Dispatching assured ROI');
                        DistributeAssuredRoi::dispatch($time);
                    }
                    if ($time->structured_roi_day == 1 && $time->structured_roi_paid == 0) {
                        $this->info('Dispatching structured ROI');
                        DistributeStructuredRoi::dispatch($time);
                    }
                } elseif (
                    ($time->assured_roi_day == 1 && $time->assured_roi_commission_paid == 0) ||
                    ($time->structured_roi_day == 1 && $time->structured_roi_commission_paid == 0)
                ) {
                    $this->info('Dispatching commission after ROI');
                    //SHARED 1 script and the paid will update by the job
                    DistributeCommission::dispatch($time);
                } elseif ($time->assured_roi_fund_approved == 0 || $time->structured_roi_fund_approved == 0) {
                    $this->info('Dispatching approve fund ROI');
                    //SHARED 1 script and the approved will update by the job
                    ApproveFund::dispatch($time);
                }
            }

            $msg = sprintf('Successfully '.$this->signature.' at %s', \Carbon\Carbon::now()->format('Y-m-d H:i:s'));
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                return makeResponse(true, $msg);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            $msg = sprintf('Error while '.$this->signature.', file: %s, line: %s, message: %s', $e->getFile(), $e->getLine(), $e->getMessage());
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                addError($msg);

                return makeResponse(false, $msg);
            }
        }
    }
}
