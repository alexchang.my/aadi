<?php

namespace App\Console\Commands\Regenerate;

use App\Models\Account;
use App\Models\DailyReport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Log;

class RegenerateDailyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'regenerate:daily_report
    {date? : Date in format Y-m-d}';

    /**
     * The console command description.0
     *
     * @var string
     */
    protected $description = 'Regenerate daily report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            ini_set('max_execution_time', 0); //0=NOLIMIT

            \DB::beginTransaction();

            $d = $this->ask('Date? Y-m-d');
            if ($d == 'case_2023-02-06') {
                $dates = ['2023-02-06', '2023-02-07', '2023-02-08', '2023-02-09', '2023-02-10', '2023-02-11', '2023-02-12'];
                $plan_type = 'structured';

                foreach ($dates as $date) {
                    $date = \Carbon\Carbon::createFromFormat('Y-m-d', $date);

                    $report = DailyReport::where('plan_type', '=', $plan_type)
                        ->whereDate('report_date', '=', $date)
                        ->first();

                    if ($report) {
                        $this->info('Regenerating report for ' . $date->copy()->format('Y-m-d'));
                        $report->calculateReport();
                        $report->save();
                    }
                }
            } else {
                $date = \Carbon\Carbon::createFromFormat('Y-m-d', $d);
                $plan_type = $this->ask('Plan type? structured or assured only');

                $this->info($date);

                $report = DailyReport::where('plan_type', '=', $plan_type)
                    ->whereDate('report_date', '=', $date)
                    ->first();

                if ($report) {
                    $this->info('Regenerating');
                    $report->calculateReport();
                    $report->save();
                }
            }

            \DB::commit();

            $msg = sprintf('Successfully '.$this->signature.' at %s', \Carbon\Carbon::now()->format('Y-m-d H:i:s'));
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                return makeResponse(true, $msg);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            $msg = sprintf('Error while '.$this->signature.', file: %s, line: %s, message: %s', $e->getFile(), $e->getLine(), $e->getMessage());
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                addError($msg);

                return makeResponse(false, $msg);
            }
        }
    }

    public function fetchUser()
    {
        $response = Http::get('https://random-data-api.com/api/users/random_user');

        return $response->json();
    }
}
