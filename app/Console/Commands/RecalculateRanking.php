<?php

namespace App\Console\Commands;

use App\Jobs\RankCheck\CheckRank1;
use App\Jobs\RankCheck\CheckRank2;
use App\Jobs\RankCheck\CheckRank3;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Log;

class RecalculateRanking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recalculate_ranking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalculate all user ranking';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            ini_set('max_execution_time', 0); //0=NOLIMIT

            \DB::beginTransaction();
            $today = now()->startOfDay();

            for ($i = 1; $i <= 3; $i++) {
                $datetime_field = 'last_ranking_' . $i . '_check_at';
                if (config('env.APP_DEBUG')) {
                    $this->info($datetime_field);
                }

                $ranks_check = User::where($datetime_field, '<', $today)
                    ->orderBy('unilevel', 'DESC')
                    ->orderBy('id', 'DESC');
                if ($ranks_check->count()) {
                    if (config('env.APP_DEBUG')) {
                        $this->info($datetime_field . ' have user waiting to count');
                    }

                    foreach ($ranks_check->get() as $user) {
                        $user->last_dispatch_ranking_check = now();
                        $user->{$datetime_field} = now();
                        $user->save();

                        if ($i == 1) {
                            dispatch(new CheckRank1($user));
                        } else if ($i == 2) {
                            dispatch(new CheckRank2($user));
                        } else if ($i == 3) {
                            dispatch(new CheckRank3($user));
                        }
                    }
                }
            }

            \DB::commit();

            $msg = sprintf('Successfully '.$this->signature.' at %s', \Carbon\Carbon::now()->format('Y-m-d H:i:s'));
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                return makeResponse(true, $msg);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            $msg = sprintf('Error while '.$this->signature.', file: %s, line: %s, message: %s', $e->getFile(), $e->getLine(), $e->getMessage());
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                addError($msg);

                return makeResponse(false, $msg);
            }
        }
    }

    public function fetchUser()
    {
        $response = Http::post('https://api.namefake.com');

        return $response->json();
    }
}
