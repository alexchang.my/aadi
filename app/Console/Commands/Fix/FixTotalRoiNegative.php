<?php

namespace App\Console\Commands\Fix;

use App\Models\Account;
use App\Models\AccountTransaction;
use App\Models\DailyReport;
use App\Models\DistributionBatch;
use App\Models\UserInvestment;
use App\Models\UserInvestmentTransaction;
use Illuminate\Console\Command;
use Log;

class FixTotalRoiNegative extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:total_roi_negative';

    /**
     * The console command description
     *
     * @var string
     */
    protected $description = 'Fix total ROI negative';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            ini_set('max_execution_time', 0); //0=NOLIMIT

            \DB::beginTransaction();

            foreach (UserInvestment::orderBy('id', 'ASC')->get() as $ui) {
                $ui->total_roi = 0;
                $uits = UserInvestmentTransaction::whereIn('transaction_type', [101,102])
                    ->where('user_investment_id', '=', $ui->id)->orderBy('id', 'ASC')->get();
                foreach ($uits as $uit) {
                    if ($uit->transaction_type == 102) {
                        $ui->total_roi = bcsub($ui->total_roi, abs($uit->amount), 5);
                    } else {
                        $ui->total_roi = bcadd($ui->total_roi, $uit->amount, 5);
                    }
                }

                $ui->save();
            }

            \DB::commit();

            $msg = sprintf('Successfully '.$this->signature.' at %s', \Carbon\Carbon::now()->format('Y-m-d H:i:s'));
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                return makeResponse(true, $msg);
            }
        } catch (\Exception $e) {
            \DB::rollBack();

            $msg = sprintf('Error while '.$this->signature.', file: %s, line: %s, message: %s', $e->getFile(), $e->getLine(), $e->getMessage());
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                addError($msg);

                return makeResponse(false, $msg);
            }
        }
    }
}
