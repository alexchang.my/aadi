<?php

namespace App\Console\Commands\Fix;

use App\Models\Account;
use App\Models\AccountTransaction;
use App\Models\DailyReport;
use App\Models\DistributionBatch;
use App\Models\UserInvestment;
use App\Models\UserInvestmentTransaction;
use Illuminate\Console\Command;
use Log;

class FixRecalculateRoiBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:recalculate_roi_balance';

    /**
     * The console command description
     *
     * @var string
     */
    protected $description = 'Fix: Recalculate ROI balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            ini_set('max_execution_time', 0); //0=NOLIMIT

            \DB::beginTransaction();

            foreach (UserInvestment::with(['user'])->orderBy('id', 'ASC')->get() as $ui) {
                //101 = ROI earned
                //102 = ROI loss
                $total_roi_received = UserInvestmentTransaction::where('user_investment_id', '=', $ui->id)->whereIn('transaction_type', [101,102])->sum('amount');
                //251 = ROI withdraw
                $total_withdraw_roi = abs(UserInvestmentTransaction::where('user_investment_id', '=', $ui->id)->where('transaction_type', [251])->sum('amount'));
                $roi_balance = $total_roi_received - $total_withdraw_roi;

                $message_prefix = $ui->user->username . '(' . $ui->contract_id . '): ';
                $updated = false;
                if ($total_roi_received != $ui->total_roi) {
                    $updated = true;
                    $ui->total_roi = $total_roi_received;
                    $this->info($message_prefix . ' total ROI received got different = ' . $ui->total_roi . ' / ' . $total_roi_received);
                }

                if ($roi_balance != $ui->roi_balance) {
                    $updated = true;
                    $ui->roi_balance = $roi_balance;
                    $this->info($message_prefix . ' Total received ROI = ' . $total_roi_received);
                    $this->info($message_prefix . ' ROI balance got different = ' . $ui->roi_balance . ' / ' . $roi_balance);
                }

                if ($updated) {
                    $ui->save();
                }
            }

            \DB::commit();

            $msg = sprintf('Successfully '.$this->signature.' at %s', \Carbon\Carbon::now()->format('Y-m-d H:i:s'));
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                return makeResponse(true, $msg);
            }
        } catch (\Exception $e) {
            \DB::rollBack();

            $msg = sprintf('Error while '.$this->signature.', file: %s, line: %s, message: %s', $e->getFile(), $e->getLine(), $e->getMessage());
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                addError($msg);

                return makeResponse(false, $msg);
            }
        }
    }
}
