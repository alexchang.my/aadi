<?php

namespace App\Console\Commands\Fix;

use App\Models\DailyReport;
use App\Models\TimeTable;
use Illuminate\Console\Command;
use App\Jobs\ApproveFund;
use App\Jobs\AssuredRoi\DistributeRoi as DistributeAssuredRoi;
use App\Jobs\DistributeCommission;
use App\Jobs\StructuredRoi\DistributeRoi as DistributeStructuredRoi;
use Log;

class Fix2Days extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:fix_2_days';

    /**
     * The console command description
     *
     * @var string
     */
    protected $description = 'Fix accidentally 2 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            ini_set('max_execution_time', 0); //0=NOLIMIT

            $this->runSubDate();

            $msg = sprintf('Successfully '.$this->signature.' at %s', \Carbon\Carbon::now()->format('Y-m-d H:i:s'));
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                return makeResponse(true, $msg);
            }
        } catch (\Exception $e) {
            $msg = sprintf('Error while '.$this->signature.', file: %s, line: %s, message: %s', $e->getFile(), $e->getLine(), $e->getMessage());
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                addError($msg);

                return makeResponse(false, $msg);
            }
        }
    }

    /**
     * @throws \Exception
     */
    public function runSubDate()
    {
        try {
            \DB::beginTransaction();

            //ALL REPORT/transaction add 2 day
            \DB::unprepared("UPDATE `time_tables`
                SET `assured_roi_day` = 1,
                    `structured_roi_day` = 1
            ");
            \DB::unprepared("UPDATE `account_transactions`
                SET `created_at` = DATE_ADD(`created_at`, INTERVAL 2 DAY),
                    `updated_at` = DATE_ADD(`updated_at`, INTERVAL 2 DAY)
            ");
            \DB::unprepared("UPDATE `account_transactions`
                SET `verified_at` = DATE_ADD(`verified_at`, INTERVAL 2 DAY)
                WHERE `verified_at` IS NOT NULL
            ");
            \DB::unprepared("UPDATE `daily_reports`
                SET `report_date` = DATE_ADD(`report_date`, INTERVAL 2 DAY),
                    `report_datetime_from` = DATE_ADD(`report_datetime_from`, INTERVAL 2 DAY),
                    `report_datetime_to` = DATE_ADD(`report_datetime_to`, INTERVAL 2 DAY),
                    `created_at` = DATE_ADD(`created_at`, INTERVAL 2 DAY),
                    `updated_at` = DATE_ADD(`updated_at`, INTERVAL 2 DAY)
            ");
            \DB::unprepared("UPDATE `distribution_batch`
                SET `distribution_date` = DATE_ADD(`distribution_date`, INTERVAL 2 DAY),
                    `created_at` = DATE_ADD(`created_at`, INTERVAL 2 DAY),
                    `updated_at` = DATE_ADD(`updated_at`, INTERVAL 2 DAY)
            ");
            \DB::unprepared("UPDATE `distribution_settings`
                SET `created_at` = DATE_ADD(`created_at`, INTERVAL 2 DAY),
                    `updated_at` = DATE_ADD(`updated_at`, INTERVAL 2 DAY)
            ");
            \DB::unprepared("UPDATE `distribution_settings`
                SET `used_at` = DATE_ADD(`used_at`, INTERVAL 2 DAY)
                WHERE `used_at` IS NOT NULL
            ");
            \DB::unprepared("UPDATE `distribution_stages`
                SET `created_at` = DATE_ADD(`created_at`, INTERVAL 2 DAY),
                    `updated_at` = DATE_ADD(`updated_at`, INTERVAL 2 DAY)
            ");
            \DB::unprepared("UPDATE `investment_transactions`
                SET `created_at` = DATE_ADD(`created_at`, INTERVAL 2 DAY),
                    `updated_at` = DATE_ADD(`updated_at`, INTERVAL 2 DAY)
            ");
            \DB::unprepared("UPDATE `users`
                SET `last_dispatch_ranking_check` = NULL,
                    `last_complete_ranking_check` = NULL
            ");
            \DB::unprepared("UPDATE `user_investments`
                SET `fund_approve_at` = DATE_ADD(`fund_approve_at`, INTERVAL 2 DAY),
                    `last_distribute_roi_at` = DATE_ADD(`last_distribute_roi_at`, INTERVAL 2 DAY),
                    `created_at` = DATE_ADD(`created_at`, INTERVAL 2 DAY),
                    `updated_at` = DATE_ADD(`updated_at`, INTERVAL 2 DAY)
            ");
            \DB::unprepared("UPDATE `user_investments`
                SET `opt_in_at` = DATE_ADD(`fund_approve_at`, INTERVAL 2 DAY)
                WHERE `opt_in_at` IS NOT NULL
            ");
            \DB::unprepared("UPDATE `user_investment_transactions`
                SET `created_at` = DATE_ADD(`created_at`, INTERVAL 2 DAY),
                    `updated_at` = DATE_ADD(`updated_at`, INTERVAL 2 DAY)
            ");
            \DB::unprepared("UPDATE `user_investment_transactions`
                SET `bonus_date_at` = DATE_ADD(`bonus_date_at`, INTERVAL 2 DAY)
                WHERE `bonus_date_at` IS NOT NULL
            ");
            \DB::unprepared("UPDATE `user_transactions`
                SET `created_at` = DATE_ADD(`created_at`, INTERVAL 2 DAY),
                    `updated_at` = DATE_ADD(`updated_at`, INTERVAL 2 DAY)
            ");
            \DB::unprepared("UPDATE `user_transactions`
                SET `bonus_date_at` = DATE_ADD(`bonus_date_at`, INTERVAL 2 DAY)
                WHERE `bonus_date_at` IS NOT NULL
            ");

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            throw new \Exception($e);
        }
    }
}
