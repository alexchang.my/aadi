<?php

namespace App\Console\Commands\Fix;

use App\Models\Account;
use App\Models\AccountTransaction;
use App\Models\DailyReport;
use App\Models\DistributionBatch;
use App\Models\UserInvestment;
use App\Models\UserInvestmentTransaction;
use App\Models\UserTransaction;
use Illuminate\Console\Command;
use Log;

class FixDailyReportActualPayable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:daily_report_actual_payable';

    /**
     * The console command description
     *
     * @var string
     */
    protected $description = 'Fix daily report actual payable';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            ini_set('max_execution_time', 0); //0=NOLIMIT

            \DB::beginTransaction();

            foreach (DailyReport::orderBy('plan_type', 'ASC')->orderBy('id', 'ASC')->get() as $dr) {
                $dr->actual_payable = UserTransaction::whereIn('transaction_type', [401, 501, 601])
                    ->where('is_bonus_type', '=', $dr->plan_type)
                    ->where('bonus_date_at', '>=', $dr->report_datetime_from)
                    ->where('bonus_date_at', '<=', $dr->report_datetime_to)
                    ->sum('amount');

                $dr->actual_payable += UserInvestmentTransaction::where('transaction_type', '=', 101)
                    ->where('is_roi', '=', 1)
                    ->whereHas('userInvestment', function ($q) use ($dr) {
                        $q->where('plan_type', '=', $dr->plan_type);
                    })
                    ->where('bonus_date_at', '>=', $dr->report_datetime_from)
                    ->where('bonus_date_at', '<=', $dr->report_datetime_to)
                    ->sum('amount');

                $dr->actual_payable += UserInvestmentTransaction::where('transaction_type', '=', 102)
                    ->whereHas('userInvestment', function ($q) use ($dr) {
                        $q->where('plan_type', '=', $dr->plan_type);
                    })
                    ->where('bonus_date_at', '>=', $dr->report_datetime_from)
                    ->where('bonus_date_at', '<=', $dr->report_datetime_to)
                    ->sum('amount');

                $this->info($dr->report_datetime_from . '/' . $dr->report_datetime_to);
                $this->info($dr->actual_payable . '/' . $dr->id);
                $dr->save();
            }

            \DB::commit();

            $msg = sprintf('Successfully '.$this->signature.' at %s', \Carbon\Carbon::now()->format('Y-m-d H:i:s'));
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                return makeResponse(true, $msg);
            }
        } catch (\Exception $e) {
            \DB::rollBack();

            $msg = sprintf('Error while '.$this->signature.', file: %s, line: %s, message: %s', $e->getFile(), $e->getLine(), $e->getMessage());
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                addError($msg);

                return makeResponse(false, $msg);
            }
        }
    }
}
