<?php

namespace App\Console\Commands\Fix;

use App\Models\Account;
use App\Models\AccountTransaction;
use App\Models\DailyReport;
use App\Models\DistributionBatch;
use Illuminate\Console\Command;
use Log;

class FixProfitAndReservedAccountForStructured extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:profit_and_reserved_account_for_structured';

    /**
     * The console command description
     *
     * @var string
     */
    protected $description = 'Fix profit and reserved account for structured';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            ini_set('max_execution_time', 0); //0=NOLIMIT

            \DB::beginTransaction();

            $codes = [Account::PL_ACCOUNT_CODE, Account::RESERVED_ACCOUNT_CODE];
            $accounts_to_be_clear = Account::whereIn('account_code', $codes)->get()->pluck('id');

            AccountTransaction::whereIn('account_id', $accounts_to_be_clear)->whereIn('transaction_type', [311,312])
                ->delete();

            foreach (Account::whereIn('account_code', $codes)->orderBy('id', 'ASC')->get() as $acc) {
                $acc->account_balance = AccountTransaction::where('account_id', '=', $acc->id)
                    ->sum('amount');
                $acc->save();
            }

            foreach (DistributionBatch::where('plan_type', '=', 'structured')->orderBy('id', 'ASC')->get() as $batch) {
                $daily_report = DailyReport::where('plan_type', '=', $batch->plan_type)->where('report_date', '=', $batch->distribution_date)
                    ->orderBy('id', 'ASC')
                    ->first();

                if (!$daily_report) throw new \Exception($batch->distribution_date . ' daily report not found');

                if ($daily_report->profit_loss > 0) {
                    $amount = $daily_report->profit_loss / 2;
                    $profit_sharing = ($amount / 100) * 75;
                    $reserved = ($amount / 100) * 25;

                    if ($profit_sharing > 0) {
                        $pl_acc = Account::where('account_code', '=', Account::PL_ACCOUNT_CODE)->orderBy('id', 'DESC')->first();
                        if ($pl_acc) {
                            $pl_acc->creditToAccount(
                                amount: $profit_sharing,
                                transaction_type: 311,
                                verified: true,
                                related_key: $daily_report->id,
                                params: [
                                    'generated_fund' => $daily_report->generated_fund,
                                ]
                            );
                        }
                    }

                    if ($reserved > 0) {
                        $reserved_acc = Account::where('account_code', '=', Account::RESERVED_ACCOUNT_CODE)->orderBy('id', 'DESC')->first();
                        if ($reserved_acc) {
                            $reserved_acc->creditToAccount(
                                amount: $reserved,
                                transaction_type: 312,
                                verified: true,
                                related_key: $daily_report->id,
                                params: [
                                    'generated_fund' => $daily_report->generated_fund,
                                ]
                            );
                        }
                    }
                }
            }

            \DB::commit();

            $msg = sprintf('Successfully '.$this->signature.' at %s', \Carbon\Carbon::now()->format('Y-m-d H:i:s'));
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                return makeResponse(true, $msg);
            }
        } catch (\Exception $e) {
            \DB::rollBack();

            $msg = sprintf('Error while '.$this->signature.', file: %s, line: %s, message: %s', $e->getFile(), $e->getLine(), $e->getMessage());
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                addError($msg);

                return makeResponse(false, $msg);
            }
        }
    }
}
