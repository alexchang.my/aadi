<?php

namespace App\Console\Commands\Reporting;

use App\Models\Account;
use App\Models\DailyReport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Log;

class DailyStructuredReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reporting:daily_structured_report
    {date? : Date in format Y-m-d}';

    /**
     * The console command description.0
     *
     * @var string
     */
    protected $description = 'Daily structured report generator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            ini_set('max_execution_time', 0); //0=NOLIMIT

            \DB::beginTransaction();

            $d = $this->argument('date');
            if ($d) {
                $date = \Carbon\Carbon::createFromFormat('Y-m-d', $d)->startOfDay();
            } else {
                $date = now()->startOfDay();
            }

            $this->info($date);

            $report = DailyReport::where('plan_type', '=', 'structured')
                ->whereDate('report_date', '=', $date)
                ->first();

            $new = false;
            if (! $report) {
                $report = new DailyReport();
                $report->report_date = $date;
                $report->plan_type = 'structured';
                $report->save();
                $new = true;
            } else {
                $report->calculateReport();
                $report->save();
            }

            if ($new) {
                if ($report->generated_fund > 0) {
                    //Trader Declare - Current Fund Size = A
                    //actually generated_fund = A
                    //A x 50% x 75% --> Profit Sharing Account
                    //A x 50% x 25% --> Reserved Account
                    $pl_amount = (($report->generated_fund / 2) / 100) * 75;
                    $reserved_amount = (($report->generated_fund / 2) / 100) * 25;

                    if ($pl_amount > 0) {
                        $pl_acc = Account::where('account_code', '=', Account::PL_ACCOUNT_CODE)->orderBy('id', 'DESC')->first();
                        if ($pl_acc) {
                            $pl_acc->creditToAccount(
                                amount: $pl_amount,
                                transaction_type: 311,
                                verified: true,
                                related_key: $report->id,
                                params: [
                                    'generated_fund' => $report->generated_fund,
                                ]
                            );
                        }
                    }

                    if ($reserved_amount > 0) {
                        $reserved_acc = Account::where('account_code', '=', Account::RESERVED_ACCOUNT_CODE)->orderBy('id', 'DESC')->first();
                        if ($reserved_acc) {
                            $reserved_acc->creditToAccount(
                                amount: $reserved_amount,
                                transaction_type: 312,
                                verified: true,
                                related_key: $report->id,
                                params: [
                                    'generated_fund' => $report->generated_fund,
                                ]
                            );
                        }
                    }
                }
            }

            \DB::commit();

            $msg = sprintf('Successfully '.$this->signature.' at %s', \Carbon\Carbon::now()->format('Y-m-d H:i:s'));
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                return makeResponse(true, $msg);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            $msg = sprintf('Error while '.$this->signature.', file: %s, line: %s, message: %s', $e->getFile(), $e->getLine(), $e->getMessage());
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                addError($msg);

                return makeResponse(false, $msg);
            }
        }
    }

    public function fetchUser()
    {
        $response = Http::get('https://random-data-api.com/api/users/random_user');

        return $response->json();
    }
}
