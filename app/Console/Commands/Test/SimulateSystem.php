<?php

namespace App\Console\Commands\Test;

use App\Models\DailyReport;
use App\Models\TimeTable;
use Illuminate\Console\Command;
use App\Jobs\ApproveFund;
use App\Jobs\AssuredRoi\DistributeRoi as DistributeAssuredRoi;
use App\Jobs\DistributeCommission;
use App\Jobs\StructuredRoi\DistributeRoi as DistributeStructuredRoi;
use Log;

class SimulateSystem extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:simulate_system';

    /**
     * The console command description
     *
     * @var string
     */
    protected $description = 'Simulate system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            ini_set('max_execution_time', 0); //0=NOLIMIT

            $date = now('utc')->startOfDay();

            $this->runSubDate($date);

            $still_working = true;
            $assured_roi_run = false;
            $structured_roi_run = false;
            $commission_run = false;
            $approve_fund_run = false;
            $assured_report_run = false;
            $structured_report_run = false;
            $wait_counter = 0;
            while ($still_working) {
                $time = TimeTable::whereDate('utc_date', '=', $date->copy()->format('Y-m-d'))
                    ->orderBy('id', 'ASC')
                    ->first();

                if ($time->assured_roi_paid == 0 || $time->structured_roi_paid == 0) {
                    if (!$assured_roi_run) {
                        $this->info('Distributing assured ROI...');
                        $assured_roi_run = true;
                        DistributeAssuredRoi::dispatch($time);
                    }
                    if (!$structured_roi_run) {
                        $this->info('Distributing structured ROI...');
                        $structured_roi_run = true;
                        DistributeStructuredRoi::dispatch($time);
                    }
                    sleep(2);
                    $this->info('Assured/Structured ROI still running(' . $time->assured_roi_paid . '/' . $time->structured_roi_paid . ')');
                } else if ($time->assured_roi_commission_paid == 0 || $time->structured_roi_commission_paid == 0) {
                    if (!$commission_run) {
                        $this->info('Distributing commission...');
                        $commission_run = true;
                        DistributeCommission::dispatch($time);
                    }
                    sleep(2);
                    $this->info('Assured/Structured commission still running(' . $time->assured_roi_commission_paid . '/' . $time->structured_roi_commission_paid . ')');
                } else if ($time->assured_roi_fund_approved == 0 || $time->structured_roi_fund_approved == 0) {
                    if (!$approve_fund_run) {
                        $this->info('Approving fund...');
                        $approve_fund_run = true;
                        ApproveFund::dispatch($time);
                    }
                    $this->info('Assured/Structured fund approval still running(' . $time->assured_roi_fund_approved . '/' . $time->structured_roi_fund_approved . ')');
                } else {
                    $assured_report = DailyReport::whereDate('report_date', '=', $date->copy()->format('Y-m-d'))
                        ->where('plan_type', '=', 'assured')
                        ->orderBy('id', 'ASC')
                        ->first();

                    $structured_report = DailyReport::whereDate('report_date', '=', $date->copy()->format('Y-m-d'))
                        ->where('plan_type', '=', 'structured')
                        ->orderBy('id', 'ASC')
                        ->first();

                    if ($assured_report && $structured_report) {
                        $this->call('recalculate_ranking');
                        $this->info('Recalculate ranking dispatched');
                        $still_working = false;
                    } else {
                        if (!$assured_report && !$assured_report_run) {
                            $assured_report_run = true;
                            $this->info('Daily assured report generating...');
                            $this->call('reporting:daily_assured_report', ['date' => $date->copy()->format('Y-m-d')]);
                        }

                        if (!$structured_report && !$structured_report_run) {
                            $structured_report_run = true;
                            $this->info('Daily structured report generating...');
                            $this->call('reporting:daily_structured_report', ['date' => $date->copy()->format('Y-m-d')]);
                        }

                        $this->info('Waiting for ' . $date->copy()->format('Y-m-d') . ' report generate');
                    }
                }

                if ($still_working) {
                    $wait_counter++;
                    $this->info('Wait 10 seconds to re-check (' . $wait_counter . ')');
                    $this->info('=======================');
                    sleep(10);
                }
            }

            $msg = sprintf('Successfully '.$this->signature.' at %s', \Carbon\Carbon::now()->format('Y-m-d H:i:s'));
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                return makeResponse(true, $msg);
            }
        } catch (\Exception $e) {
            $msg = sprintf('Error while '.$this->signature.', file: %s, line: %s, message: %s', $e->getFile(), $e->getLine(), $e->getMessage());
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                addError($msg);

                return makeResponse(false, $msg);
            }
        }
    }

    /**
     * @throws \Exception
     */
    public function runSubDate($date)
    {
        try {
            \DB::beginTransaction();

            //ALL REPORT/transaction sub 1 day
            \DB::unprepared("UPDATE `time_tables`
                SET `assured_roi_day` = 1,
                    `structured_roi_day` = 1
            ");
            \DB::unprepared("UPDATE `account_transactions`
                SET `created_at` = DATE_SUB(`created_at`, INTERVAL 1 DAY),
                    `updated_at` = DATE_SUB(`updated_at`, INTERVAL 1 DAY)
            ");
            \DB::unprepared("UPDATE `account_transactions`
                SET `verified_at` = DATE_SUB(`verified_at`, INTERVAL 1 DAY)
                WHERE `verified_at` IS NOT NULL
            ");
            \DB::unprepared("UPDATE `daily_reports`
                SET `report_date` = DATE_SUB(`report_date`, INTERVAL 1 DAY),
                    `report_datetime_from` = DATE_SUB(`report_datetime_from`, INTERVAL 1 DAY),
                    `report_datetime_to` = DATE_SUB(`report_datetime_to`, INTERVAL 1 DAY),
                    `created_at` = DATE_SUB(`created_at`, INTERVAL 1 DAY),
                    `updated_at` = DATE_SUB(`updated_at`, INTERVAL 1 DAY)
            ");
            \DB::unprepared("UPDATE `distribution_batch`
                SET `distribution_date` = DATE_SUB(`distribution_date`, INTERVAL 1 DAY),
                    `created_at` = DATE_SUB(`created_at`, INTERVAL 1 DAY),
                    `updated_at` = DATE_SUB(`updated_at`, INTERVAL 1 DAY)
            ");
            \DB::unprepared("UPDATE `distribution_settings`
                SET `created_at` = DATE_SUB(`created_at`, INTERVAL 1 DAY),
                    `updated_at` = DATE_SUB(`updated_at`, INTERVAL 1 DAY)
            ");
            \DB::unprepared("UPDATE `distribution_settings`
                SET `used_at` = DATE_SUB(`used_at`, INTERVAL 1 DAY)
                WHERE `used_at` IS NOT NULL
            ");
            \DB::unprepared("UPDATE `distribution_stages`
                SET `created_at` = DATE_SUB(`created_at`, INTERVAL 1 DAY),
                    `updated_at` = DATE_SUB(`updated_at`, INTERVAL 1 DAY)
            ");
            \DB::unprepared("UPDATE `investment_transactions`
                SET `created_at` = DATE_SUB(`created_at`, INTERVAL 1 DAY),
                    `updated_at` = DATE_SUB(`updated_at`, INTERVAL 1 DAY)
            ");
            \DB::unprepared("UPDATE `users`
                SET `last_dispatch_ranking_check` = NULL,
                    `last_complete_ranking_check` = NULL
            ");
            \DB::unprepared("UPDATE `user_investments`
                SET `fund_approve_at` = DATE_SUB(`fund_approve_at`, INTERVAL 1 DAY),
                    `last_distribute_roi_at` = DATE_SUB(`last_distribute_roi_at`, INTERVAL 1 DAY),
                    `created_at` = DATE_SUB(`created_at`, INTERVAL 1 DAY),
                    `updated_at` = DATE_SUB(`updated_at`, INTERVAL 1 DAY)
            ");
            \DB::unprepared("UPDATE `user_investments`
                SET `opt_in_at` = DATE_SUB(`fund_approve_at`, INTERVAL 1 DAY)
                WHERE `opt_in_at` IS NOT NULL
            ");
            \DB::unprepared("UPDATE `user_investment_transactions`
                SET `created_at` = DATE_SUB(`created_at`, INTERVAL 1 DAY),
                    `updated_at` = DATE_SUB(`updated_at`, INTERVAL 1 DAY)
            ");
            \DB::unprepared("UPDATE `user_investment_transactions`
                SET `bonus_date_at` = DATE_SUB(`bonus_date_at`, INTERVAL 1 DAY)
                WHERE `bonus_date_at` IS NOT NULL
            ");
            \DB::unprepared("UPDATE `user_transactions`
                SET `created_at` = DATE_SUB(`created_at`, INTERVAL 1 DAY),
                    `updated_at` = DATE_SUB(`updated_at`, INTERVAL 1 DAY)
            ");
            \DB::unprepared("UPDATE `user_transactions`
                SET `bonus_date_at` = DATE_SUB(`bonus_date_at`, INTERVAL 1 DAY)
                WHERE `bonus_date_at` IS NOT NULL
            ");

            $time = TimeTable::whereDate('utc_date', '=', $date->copy()->format('Y-m-d'))
                ->orderBy('id', 'ASC')
                ->first();

            $time->assured_roi_paid = 0;
            $time->structured_roi_paid = 0;
            $time->assured_roi_commission_paid = 0;
            $time->structured_roi_commission_paid = 0;
            $time->assured_roi_fund_approved = 0;
            $time->structured_roi_fund_approved = 0;
            $time->save();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            throw new \Exception($e);
        }
    }
}
