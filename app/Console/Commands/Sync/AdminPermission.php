<?php

namespace App\Console\Commands\Sync;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Log;

class AdminPermission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:admin_permission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync admin permission json to php array';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            ini_set('max_execution_time', 0); //0=NOLIMIT

            foreach (config('app.locales') as $locale => $language) {
                $path = base_path('lang/'.$locale.'/permission.php');
                @unlink($path);
                $file = fopen($path, 'a', 1);
                fwrite($file, '<?php'.PHP_EOL.PHP_EOL.'return ['.PHP_EOL);

                $json = json_decode(file_get_contents(base_path('resources/scripts/admin/lang/'.$locale.'/_permission.json')), true);

                foreach ($json as $key => $value) {
                    fwrite($file, "    '".$key."' => '".$value."',".PHP_EOL);
                }

                fwrite($file, '];'.PHP_EOL);
                fclose($file);
            }

            $msg = sprintf('Successfully '.$this->signature.' at %s', \Carbon\Carbon::now()->format('Y-m-d H:i:s'));
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                return makeResponse(true, $msg);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            $msg = sprintf('Error while '.$this->signature.', file: %s, line: %s, message: %s', $e->getFile(), $e->getLine(), $e->getMessage());
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                addError($msg);

                return makeResponse(false, $msg);
            }
        }
    }

    public function fetchUser()
    {
        $response = Http::post('https://api.namefake.com');

        return $response->json();
    }
}
