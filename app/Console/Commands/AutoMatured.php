<?php

namespace App\Console\Commands;

use App\Models\DistributionStage;
use App\Models\UserInvestment;
use Illuminate\Console\Command;
use Log;

class AutoMatured extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto_matured';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto matured';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            ini_set('max_execution_time', 0); //0=NOLIMIT

            \DB::beginTransaction();

            $records = UserInvestment::Active()
                ->whereNotNull('lock_capital_until')
                ->where('lock_capital_until', '<', now())
                ->orderBy('id', 'ASC');

            if (config('env.APP_ENV') == 'local') {
                $this->info($records->count() . ' records found');
            }
            if ($records->count()) {
                foreach ($records->get() as $record) {
                    if (config('env.APP_ENV') == 'local') {
                        $this->info($record->contract_id . '(#' . $record->id . '): matured ' . ' ' . $record->current_balance);
                    }
                    $record->withdrawCapital();
                    $record->save();
                }
            }

            \DB::commit();

            $msg = sprintf('Successfully '.$this->signature.' at %s', \Carbon\Carbon::now()->format('Y-m-d H:i:s'));
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                return makeResponse(true, $msg);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            $msg = sprintf('Error while '.$this->signature.', file: %s, line: %s, message: %s', $e->getFile(), $e->getLine(), $e->getMessage());
            Log::info($msg);

            if (app()->runningInConsole()) {
                $this->comment(PHP_EOL.$msg.PHP_EOL);
            } else {
                addError($msg);

                return makeResponse(false, $msg);
            }
        }
    }
}
