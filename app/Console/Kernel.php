<?php

namespace App\Console;

use App\Console\Commands\AutoMatured;
use App\Console\Commands\ClearSystem;
use App\Console\Commands\DailyReset;
use App\Console\Commands\DailyWorker;
use App\Console\Commands\Fix\Fix2Days;
use App\Console\Commands\Fix\FixDailyReportActualPayable;
use App\Console\Commands\Fix\FixProfitAndReservedAccountForStructured;
use App\Console\Commands\Fix\FixRecalculateRoiBalance;
use App\Console\Commands\Fix\FixTotalRoiNegative;
use App\Console\Commands\FixUnilevel;
use App\Console\Commands\Generate\AddDataTable;
use App\Console\Commands\RebuildUniLevel;
use App\Console\Commands\RecalculateRanking;
use App\Console\Commands\Regenerate\RegenerateDailyReport;
use App\Console\Commands\Reporting\DailyAssuredReport;
use App\Console\Commands\Reporting\DailyStructuredReport;
use App\Console\Commands\Spam\SpamUser;
use App\Console\Commands\Sync\AdminPermission;
use App\Console\Commands\Test\CheckUnilevel;
use App\Console\Commands\Test\SimulateSystem;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by the application.
     *
     * @var array
     */
    protected $commands = [
        ClearSystem::class,
        AddDataTable::class,
        AdminPermission::class,
        SpamUser::class,
        FixUnilevel::class,
        RebuildUniLevel::class,
        RecalculateRanking::class,
        CheckUnilevel::class,
        DailyAssuredReport::class,
        DailyReset::class,
        DailyWorker::class,
        SimulateSystem::class,
        Fix2Days::class,
        FixProfitAndReservedAccountForStructured::class,
        FixTotalRoiNegative::class,
        FixDailyReportActualPayable::class,
        RegenerateDailyReport::class,
        FixRecalculateRoiBalance::class,
        AutoMatured::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(AutoMatured::class)
            ->withoutOverlapping()
            ->evenInMaintenanceMode()
            ->everyMinute();

        $schedule->command(DailyWorker::class)
            ->withoutOverlapping()
            ->evenInMaintenanceMode()
            ->everyFiveMinutes()
            ->when(function () {
                //DAILY UTC 4PM (Malaysia 12AM)
                return now('UTC')->hour == 16;
            });

        $schedule->command(DailyAssuredReport::class)
            ->withoutOverlapping()
            ->evenInMaintenanceMode()
            ->everyFiveMinutes()
            ->when(function () {
                //DAILY UTC 11PM (Malaysia 7AM)
                return now('UTC')->hour == 23;
            });

        $schedule->command(DailyStructuredReport::class)
            ->withoutOverlapping()
            ->evenInMaintenanceMode()
            ->everyFiveMinutes()
            ->when(function () {
                //DAILY UTC 11PM (Malaysia 7AM)
                return now('UTC')->hour == 23;
            });

        $schedule->command(RecalculateRanking::class)
            ->withoutOverlapping()
            ->evenInMaintenanceMode()
            ->everyFiveMinutes()
            ->when(function () {
                //DAILY UTC 7PM (Malaysia 3AM)
                return now('UTC')->hour == 19;
            });

        $schedule->command('sanctum:prune-expired --hours=24')->daily();

//        $schedule->command(DailyReport::class)
//            ->between('00:00', '01:00')
//            ->withoutOverlapping()
//            ->evenInMaintenanceMode()
//            ->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
