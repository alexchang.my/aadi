<?php

namespace App\Traits;

use App\Models\Country;

trait HasContactNumber
{
    public function contactCountry()
    {
        return $this->hasOne(Country::class, 'id', 'contact_country_id');
    }

    public function getContactNumber()
    {
        return ($this->contactCountry ? $this->contactCountry->ext : '').$this->contact_number;
    }

    public function setContactNumberAttribute($value)
    {
        $this->attributes['contact_number'] = sanitizeContactNumber($value, $this->contactCountry);
        $this->buildFullContactNumber();
    }

    public function setContactCountryIdAttribute($value)
    {
        $this->attributes['contact_country_id'] = $value;
        $this->buildFullContactNumber();
    }

    public function buildFullContactNumber()
    {
        $number = $this->getContactNumber();

        if ($this->contactCountry) {
            if ($this->contactCountry && $this->contactCountry->iso2 == 'MY') {
                $number = trim(preg_replace('/\D/', '', $number));
                $number = ltrim(preg_replace('/\+60/', '', $number), '0');
                $number = ltrim(preg_replace('/^60/', '', $number), '0');
                $number = '0'.$number;
            } else {
                //DEFAULT USE MALAYSIA PATTERN
                $number = trim(preg_replace('/\D/', '', $number));
                $number = ltrim(preg_replace('/\+60/', '', $number), '0');
                $number = ltrim(preg_replace('/^60/', '', $number), '0');
                $number = '0'.$number;
            }
        } else {
            //DEFAULT USE MALAYSIA PATTERN
            $number = trim(preg_replace('/\D/', '', $number));
            $number = ltrim(preg_replace('/\+60/', '', $number), '0');
            $number = ltrim(preg_replace('/^60/', '', $number), '0');
            $number = '0'.$number;
        }

        $this->full_contact_number = $number;
    }
}
