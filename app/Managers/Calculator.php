<?php

namespace App\Managers;

use App\Config;

class Calculator
{
    public function __construct(public float $number, public int $decimal = Config::DECIMAL_POINT)
    {
    }

    /**
     * @param  int  $decimal
     * @return void
     */
    public function setDecimal(int $decimal): void
    {
        $this->decimal = $decimal;
    }

    /**
     * @param  float|int  $num
     * @return self
     */
    public function plus(float|int $num): self
    {
        $this->number = bcadd($this->number, $num, $this->decimal);

        return $this;
    }

    /**
     * @param  float|int  $num
     * @return self
     */
    public function minus(float|int $num): self
    {
        $this->number = bcsub($this->number, $num, $this->decimal);

        return $this;
    }

    /**
     * @param  float|int  $num
     * @return self
     */
    public function times(float|int $num): self
    {
        $this->number = bcmul($this->number, $num, $this->decimal);

        return $this;
    }

    /**
     * @param  float|int  $num
     * @return self
     */
    public function divide(float|int $num): self
    {
        $this->number = bcdiv($this->number, $num, $this->decimal);

        return $this;
    }

    /**
     * @param  float|int  $percentage
     * @return self
     */
    public function percentage(float|int $percentage): self
    {
        $this->divide(100)->times($percentage);

        return $this;
    }

    /**
     * @return int
     */
    public function getDecimal(): int
    {
        return $this->decimal;
    }

    /**
     * @return float|int
     */
    public function getNumber(): float|int
    {
        return $this->number;
    }

    /**
     * @return float|int
     */
    public function getAnswer(): float|int
    {
        return $this->getNumber();
    }
}
