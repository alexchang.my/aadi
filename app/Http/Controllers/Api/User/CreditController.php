<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\User;
use App\Models\UserTopup;
use App\Models\UserTransaction;
use App\Models\UserTransferCredit;
use App\Models\UserWithdrawal;
use Illuminate\Http\Request;

class CreditController extends Controller
{
    public const MIN_TRANSFER = 1;

    public const MIN_WITHDRAWAL_AMOUNT = 50;

    public const WITHDRAWAL_ADMIN_FEES_PERCENTAGE = 2;

    public function transferC1(Request $request)
    {
        $rules = [
            'username' => ['required', 'exists:users,username'],
            'amount' => ['required', 'integer', 'isFund', 'min:1'],
        ];

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $cid = 1;
            $cfield = 'credit_'.$cid;
            $transfer_conversion_rate = 1;

            $me = User::lockForUpdate()
                ->find($request->user()->id);

            if (! $me) {
                throw new \Exception(trans('common.x_not_found', ['x' => trans('common.user')]));
            }

            if ($request->get('username') == $me->username) {
                throw new \Exception(trans('common.cannot_transfer_to_x', ['x' => trans('common.yourself')]));
            }

            $user = User::lockForUpdate()
                ->where('username', '=', $request->get('username'))
                ->first();

            if (! $user) {
                throw new \Exception(trans('common.x_not_found', ['x' => trans('common.user')]));
            }

            if (! $me->correctPassword2($request->get('current_password2'))) {
                throw new \Exception(trans('common.incorrect_password2'));
            }

            $amount = (int) abs($request->get('amount', 0)); //REMOVE DECIMAL

            if ($amount < static::MIN_TRANSFER) {
                throw new \Exception(trans('common.minimum_transfer_x', ['x' => '$'.fundFormat(static::MIN_TRANSFER, 2)]));
            }

            if ($amount > $me->{$cfield}) {
                throw new \Exception(trans('common.insufficient_x', ['x' => trans('common.credit_'.$cid)]));
            }

            $rk = \Str::uuid();

            $params = [
                'from_username' => $me->username,
                'from_user_id' => $me->id,
                'to_username' => $user->username,
                'to_user_id' => $user->id,
                'from_credit_type' => $cid,
                'to_credit_type' => $cid,
                'conversion_rate' => $transfer_conversion_rate,
            ];

            $transfer = new UserTransferCredit();
            $transfer->from_user_id = $me->id;
            $transfer->to_user_id = $user->id;
            $transfer->from_credit_type = $cid;
            $transfer->to_credit_type = $cid;
            $transfer->from_amount = $amount;
            $transfer->conversion_rate = $transfer_conversion_rate;
            $transfer->related_key = $rk;
            $transfer->params = $params;
            $transfer->save();

            $ut1 = new UserTransaction();
            $ut1->user_id = $me->id;
            $ut1->credit_type = $cid;
            $ut1->transaction_type = 151;
            $ut1->amount = bcsub(0, $transfer->from_amount, \App\Config::DECIMAL_POINT);
            $ut1->related_key = $rk;
            $ut1->params = $params;
            $ut1->save();

            $ut2 = new UserTransaction();
            $ut2->user_id = $user->id;
            $ut2->credit_type = $cid;
            $ut2->transaction_type = 152;
            $ut2->amount = $transfer->to_amount;
            $ut2->related_key = $rk;
            $ut2->params = $params;
            $ut2->save();

            $me->{$cfield} = bcsub($me->{$cfield}, $transfer->from_amount, \App\Config::DECIMAL_POINT);
            $me->save();

            $tfield = 'credit_'.$transfer->to_credit_type;

            $user->{$tfield} = bcadd($user->{$tfield}, $transfer->to_amount, \App\Config::DECIMAL_POINT);
            $user->save();

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function topupC1SubmitForm(Request $request)
    {
        $rules = [
            'amount' => ['required',  'isFund'],
            'transaction_id' => ['required'],
            'receipt' => ['required', 'image'],
            'network' => ['required', 'in:bep20,trc20']
        ];

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $setting = Setting::where('setting', '=', 'company_' . $request->get('network') . '_usdt_address')->first();

            $cid = 1;
            $conversion_rate = 1;
            $symbol = 'USDT';

            $amount = abs($request->get('amount', 0));

            if ($amount <= 0) {
                throw new \Exception(trans('common.please_keyin_x', ['x' => trans('common.usdt')]));
            }

            $hash = trim($request->get('transaction_id'));
            $tx = UserTopup::where('status', '!=', 2)->where('transaction_id', '=', $hash)->count();

            if ($tx > 0) {
                throw new \Exception('Duplicate transaction ID');
            }

            $me = $request->user();

            $model = new UserTopup();
            $model->user_id = $me->id;
            $model->country_id = $me->country_id;
            $model->credit_type = $cid;
            $model->topup_amount = $amount;
            $model->conversion_rate = $conversion_rate;
            $model->from_crypto_symbol = $symbol;
            $model->to_crypto_address = $setting->setting_value;
            $model->transaction_id = $hash;
            $model->payment_method = 999; //USDT
            $model->save();

            $model->receipt = $request->file('receipt');
            $model->save();

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function transferC2(Request $request)
    {
        $rules = [
            'username' => ['required', 'exists:users,username'],
            'amount' => ['required', 'integer', 'isFund', 'min:1'],
        ];

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $cid = 2;
            $cfield = 'credit_'.$cid;
            $transfer_conversion_rate = 1;

            $me = User::lockForUpdate()
                ->find($request->user()->id);

            if (! $me) {
                throw new \Exception(trans('common.x_not_found', ['x' => trans('common.user')]));
            }

            if ($request->get('username') == $me->username) {
                throw new \Exception(trans('common.cannot_transfer_to_x', ['x' => trans('common.yourself')]));
            }

            $user = User::lockForUpdate()
                ->where('username', '=', $request->get('username'))
                ->first();

            if (! $user) {
                throw new \Exception(trans('common.x_not_found', ['x' => trans('common.user')]));
            }

            if (! $me->correctPassword2($request->get('current_password2'))) {
                throw new \Exception(trans('common.incorrect_password2'));
            }

            $amount = (int) abs($request->get('amount', 0)); //REMOVE DECIMAL

            if ($amount < static::MIN_TRANSFER) {
                throw new \Exception(trans('common.minimum_transfer_x', ['x' => '$'.fundFormat(static::MIN_TRANSFER, 2)]));
            }

            if ($amount > $me->{$cfield}) {
                throw new \Exception(trans('common.insufficient_x', ['x' => trans('common.credit_'.$cid)]));
            }

            $rk = \Str::uuid();

            $params = [
                'from_username' => $me->username,
                'from_user_id' => $me->id,
                'to_username' => $user->username,
                'to_user_id' => $user->id,
                'from_credit_type' => $cid,
                'to_credit_type' => $cid,
                'conversion_rate' => $transfer_conversion_rate,
            ];

            $transfer = new UserTransferCredit();
            $transfer->from_user_id = $me->id;
            $transfer->to_user_id = $user->id;
            $transfer->from_credit_type = $cid;
            $transfer->to_credit_type = $cid;
            $transfer->from_amount = $amount;
            $transfer->conversion_rate = $transfer_conversion_rate;
            $transfer->related_key = $rk;
            $transfer->params = $params;
            $transfer->save();

            $ut1 = new UserTransaction();
            $ut1->user_id = $me->id;
            $ut1->credit_type = $cid;
            $ut1->transaction_type = 151;
            $ut1->amount = bcsub(0, $transfer->from_amount, \App\Config::DECIMAL_POINT);
            $ut1->related_key = $rk;
            $ut1->params = $params;
            $ut1->save();

            $ut2 = new UserTransaction();
            $ut2->user_id = $user->id;
            $ut2->credit_type = $cid;
            $ut2->transaction_type = 152;
            $ut2->amount = $transfer->to_amount;
            $ut2->related_key = $rk;
            $ut2->params = $params;
            $ut2->save();

            $me->{$cfield} = bcsub($me->{$cfield}, $transfer->from_amount, \App\Config::DECIMAL_POINT);
            $me->save();

            $tfield = 'credit_'.$transfer->to_credit_type;

            $user->{$tfield} = bcadd($user->{$tfield}, $transfer->to_amount, \App\Config::DECIMAL_POINT);
            $user->save();

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function withdrawalC2buildForm(Request $request)
    {
        return makeResponse(true, null, [
            '',
        ]);
    }

    public function withdrawalC2SubmitForm(Request $request)
    {
        $rules = [
            'amount' => ['required',  'isFund'],
            'to_crypto_address' => ['required', 'string'],
            'network' => ['required', 'in:BEP20,TRC20']
        ];

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $cid = 2;
            $cfield = 'credit_'.$cid;
            $conversion_rate = 1;
            $symbol = 'USDT';

            $amount = abs($request->get('amount', 0));

            if ($amount <= 0) {
                throw new \Exception(trans('common.please_keyin_x', ['x' => trans('common.usdt')]));
            }

            $me = $request->user();

            if (! $me->correctPassword2($request->get('current_password2'))) {
                throw new \Exception(trans('common.incorrect_password2'));
            }

            if ($amount < static::MIN_WITHDRAWAL_AMOUNT) {
                throw new \Exception(trans('common.minimum_withdraw_x_amount', ['x' => '$ '.fundFormat(static::MIN_WITHDRAWAL_AMOUNT, 2)]));
            }

            if ($me->{$cfield} < $amount) {
                throw new \Exception(trans('common.insufficient_x', ['x' => trans('common.'.$cfield)]));
            }

            $model = new UserWithdrawal();
            $model->user_id = $me->id;
            $model->country_id = $me->country_id;
            $model->credit_type = $cid;
            $model->network = $request->get('network');
            $model->amount = $amount;
            $model->conversion_rate = $conversion_rate;
            $model->to_crypto_symbol = $symbol;
            $model->to_crypto_address = $request->get('to_crypto_address');
            $model->admin_fees_percentage = static::WITHDRAWAL_ADMIN_FEES_PERCENTAGE;
            $model->save();

            $ut = new UserTransaction();
            $ut->user_id = $me->id;
            $ut->credit_type = $cid;
            $ut->transaction_type = 201;
            $ut->related_key = $model->related_key;
            $ut->amount = $model->amount;
            $ut->params = [
                'to_crypto_symbol' => $model->to_crypto_symbol,
                'to_crypto_address' => $model->to_crypto_address,
                'to_crypto_amount' => $model->to_crypto_amount,
            ];
            $ut->save();

            User::where('id', '=', $me->id)
                ->decrement($cfield, $model->amount);

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function topupC1LoadHistory(Request $request)
    {
        return makeResponse(true, null, [
            'models' => $this->loadTopupHistory($request, 1),
        ]);
    }

    protected function loadTopupHistory(Request $request, $id)
    {
        return UserTopup::where('credit_type', '=', $id)
                ->where('user_id', '=', $request->user()->id)
                ->orderBy('id', 'DESC')
                ->get();
    }

    public function withdrawalC2LoadHistory(Request $request)
    {
        $data = [];
        $data['models'] = $this->loadWithdrawalHistory($request, 2);

        if ($request->get('setting')) {
            $data['setting'] = [
                'min_withdrawal_amount' => static::MIN_WITHDRAWAL_AMOUNT,
                'withdrawal_admin_fees_percentage' => static::WITHDRAWAL_ADMIN_FEES_PERCENTAGE,
            ];
        }

        return makeResponse(true, null, $data);
    }

    protected function loadWithdrawalHistory(Request $request, $id)
    {
        return UserWithdrawal::where('credit_type', '=', $id)
                ->where('user_id', '=', $request->user()->id)
                ->orderBy('id', 'DESC')
                ->get();
    }
}
