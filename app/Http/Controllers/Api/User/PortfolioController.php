<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\InvestmentPlan;
use App\Models\UserInvestment;
use App\Models\UserInvestmentTransaction;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    protected const EXISTING_USER_INVESTMENTS = [
        1 => [//Origin
            'ASGF' => 10000,
            'AASF' => 10000,
        ],
        4 => [//Success
            'ASGF' => 120000,
            'AASF' => 10000,
        ],
        6 => [//Wpquah
            'ASGF' => 50000,
            'AASF' => 10000,
        ],
        7 => [//Tyson
            'ASGF' => 100000,
            'AASF' => 50000,
        ],
    ];

    protected const EXISTING_EARNING_PERCENTAGES = [
        'ASGF' => [
            '2018-10-01' => 20.16,
            '2018-11-01' => 19.27,
            '2018-12-01' => 10.33,
            '2019-01-01' => -4.54,
            '2019-02-01' => -6.13,
            '2019-03-01' => 2.69,
            '2019-04-01' => 28.57,
            '2019-05-01' => -1.85,
            '2019-06-01' => 19.66,
            '2019-07-01' => 11.17,
            '2019-08-01' => 1.86,
            '2019-09-01' => -9.34,
            '2019-10-01' => 26.71,
            '2019-11-01' => 27.12,
            '2019-12-01' => 8.86,
            '2020-01-01' => 17.24,
            '2020-02-01' => 3.39,
            '2020-03-01' => -4.29,
            '2020-04-01' => 15.43,
            '2020-05-01' => 6.27,
            '2020-06-01' => 0.87,
            '2020-07-01' => 22.25,
            '2020-08-01' => 14.41,
            '2020-09-01' => -1.32,
            '2020-10-01' => 70.36,
            '2020-11-01' => 81.92,
            '2020-12-01' => 78.64,
            '2021-01-01' => 52.18,
            '2021-02-01' => 9.43,
            '2021-03-01' => -0.37,
            '2021-04-01' => -9.21,
            '2021-05-01' => 40.06,
            '2021-06-01' => 1.64,
            '2021-07-01' => 3.93,
            '2021-08-01' => 2.75,
            '2021-09-01' => 17.51,
            '2021-10-01' => 5.59,
            '2021-11-01' => 8.77,
            '2021-12-01' => 15.23,
            '2022-01-01' => 71.69,
            '2022-02-01' => 22.67,
            '2022-03-01' => 20.53,
            '2022-04-01' => 26.87,
            '2022-05-01' => 79.43,
            '2022-06-01' => 108.07,
            '2022-07-01' => -9.57,
            '2022-08-01' => 64.21,
            '2022-09-01' => 15.87,
            '2022-10-01' => 8.57,
            '2022-11-01' => 12.36,
            '2022-12-01' => 10.92,
        ],
        'AASF' => [
            '2020-10-01' => 3.47,
            '2020-11-01' => 11.16,
            '2020-12-01' => 5.49,
            '2021-01-01' => 2.40,
            '2021-02-01' => 3.03,
            '2021-03-01' => 7.13,
            '2021-04-01' => 4.97,
            '2021-05-01' => 10.64,
            '2021-06-01' => 2.63,
            '2021-07-01' => 6.83,
            '2021-08-01' => 7.67,
            '2021-09-01' => 5.85,
            '2021-10-01' => 5.37,
            '2021-11-01' => 3.12,
            '2021-12-01' => 4.37,
            '2022-01-01' => 10.35,
            '2022-02-01' => 4.43,
            '2022-03-01' => 5.37,
            '2022-04-01' => 10.59,
            '2022-05-01' => 7.20,
            '2022-06-01' => 4.46,
            '2022-07-01' => 4.68,
            '2022-08-01' => 5.94,
            '2022-09-01' => 7.55,
            '2022-10-01' => 11.43,
            '2022-11-01' => 6.59,
        ],
    ];

    public function fetchExisting(Request $request)
    {
        $me = $request->user();
        return makeResponse(true, null, [
            'investments' => static::EXISTING_USER_INVESTMENTS[$me->id] ?? null,
            'histories' => self::EXISTING_EARNING_PERCENTAGES,
        ]);
    }

    public function fetch(Request $request)
    {
        $me = $request->user();

        $model = UserInvestment::with(['investmentPlan'])
            ->where('user_id', '=', $me->id)
            ->orderBy('id', 'DESC')
            ->get();

        return makeResponse(true, null, [
            'model' => $model,
        ]);
    }

    public function fund(Request $request, $code)
    {
        $me = $request->user();

        $plan = InvestmentPlan::where('code', '=', $code)
            ->first();

        $model = UserInvestment::with(['investmentPlan'])
            ->where('user_id', '=', $me->id)
            ->whereHas('investmentPlan', function ($q) use ($code) {
                $q->where('code', '=', $code);
            })
            ->orderBy('id', 'DESC')
            ->get();

        return makeResponse(true, null, [
            'plan' => $plan,
            'investments' => $model,
        ]);
    }

    public function contract(Request $request, $contract_id)
    {
        $me = $request->user();

        $data = [
            'model' => UserInvestment::with(['investmentPlan'])
                ->withSum('withdrawnTransactions AS withdrawn_amount', 'amount')
                ->where('user_id', '=', $me->id)
                ->where('contract_id', '=', $contract_id)
                ->first(),
        ];

        if ($data['model']) {
            $data['transactions'] = UserInvestmentTransaction::where('user_id', '=', $me->id)
                ->whereIn('transaction_type', [101, 102])
                ->where('user_investment_id', '=', $data['model']->id)
//                ->where('is_roi', '=', 1)
                ->orderBy('id', 'DESC')
                ->get();
        }

        return makeResponse(true, null, $data);
    }

    public function withdrawCapital(Request $request, $contract_id)
    {
        try {
            \DB::beginTransaction();

            $me = $request->user();

            $model = UserInvestment::lockForUpdate()
                ->with(['investmentPlan'])
                ->where('user_id', '=', $me->id)
                ->where('contract_id', '=', $contract_id)
                ->whereIn('status', [
                    UserInvestment::STATUS_PROCESSING,
                    UserInvestment::STATUS_PENDING,
                ])
                ->first();

            if (! $model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            if (! $me->correctPassword2($request->get('password2'))) {
                throw new \Exception(trans('common.incorrect_password2'));
            }

            $model->withdrawCapital();
            $model->save();

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function withdrawRoi(Request $request, $contract_id)
    {
        try {
            \DB::beginTransaction();

            $me = $request->user();

            $model = UserInvestment::lockForUpdate()
                ->with(['investmentPlan'])
                ->where('user_id', '=', $me->id)
                ->where('contract_id', '=', $contract_id)
                ->first();

            if (! $model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            if (! $me->correctPassword2($request->get('password2'))) {
                throw new \Exception(trans('common.incorrect_password2'));
            }

            //ROI WITHDRAWAL
            $amount = abs($request->get('withdraw_roi_amount', 0));
            if ($amount <= 0 || $model->roi_balance <= 0 || $model->roi_balance < $amount) {
                throw new \Exception(trans('common.insufficient_x', ['x' => $model->plan_type == 'structured' ? trans('common.realized_roi') : trans('common.unrealized_roi')]));
            }

            $model->withdrawRoi($amount);
            $model->save();

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function optOut(Request $request, $contract_id)
    {
        try {
            \DB::beginTransaction();

            $me = $request->user();

            $model = UserInvestment::lockForUpdate()
                ->with(['investmentPlan'])
                ->where('user_id', '=', $me->id)
                ->where('contract_id', '=', $contract_id)
                ->where('status', '=', 2)
                ->first();

            if (! $model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            if (! $me->correctPassword2($request->get('current_password2'))) {
                throw new \Exception(trans('common.incorrect_password2'));
            }

            if ($model->opt_in != 1) {
                throw new \Exception(trans('common.please_opt_in_first'));
            }

            $model->processOptOut();
            $model->save();

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function optIn(Request $request, $contract_id)
    {
        try {
            \DB::beginTransaction();

            $me = $request->user();

            $model = UserInvestment::lockForUpdate()
                ->with(['investmentPlan'])
                ->where('user_id', '=', $me->id)
                ->where('contract_id', '=', $contract_id)
                ->where('status', '=', 2)
                ->first();

            if (! $model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            if (! $me->correctPassword2($request->get('current_password2'))) {
                throw new \Exception(trans('common.incorrect_password2'));
            }

            if ($model->opt_in != 0) {
                throw new \Exception(trans('common.please_opt_out_first'));
            }

            $model->processOptIn();
            $model->save();

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }
}
