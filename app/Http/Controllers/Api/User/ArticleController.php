<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function fetch(Request $request)
    {
        $article = Article::find($request->get('id'));

        return makeResponse(true, null, ['article' => $article]);
    }
}
