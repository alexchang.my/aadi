<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\InvestmentPlan;
use App\Models\User;
use App\Models\UserInvestment;
use App\Models\UserInvestmentTransaction;
use App\Models\UserTransaction;
use Illuminate\Http\Request;

class PlanInvestController extends Controller
{
    public function buildForm(Request $request)
    {
        $me = $request->user();

        $data = [];
        $data['min_invest'] = 100;
        $data['model'] = InvestmentPlan::where('code', '=', $request->get('code'))->first();
        if ($data['model']) {
            $data['min_invest'] = $data['model']->setting['initial_investment_amount'];
        }

        return makeResponse(true, null, $data);
    }

    public function submitForm(Request $request)
    {
        $rules = [
            'buy_in_amount' => ['required', 'isFund'],
            'credit_1_amount' => ['nullable', 'isFund'],
        ];

        if ($request->filled('credit_2_amount') && $request->get('credit_2_amount', 0) > 0) {
            $rules['credit_2_amount'] = ['nullable', 'isFund'];
        }

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $me = $request->user();

            if (! $me->correctPassword2($request->get('current_password2'))) {
                throw new \Exception(trans('common.incorrect_password2'));
            }

            $plan = InvestmentPlan::where('code', '=', $request->get('code'))->first();
            if (! $plan) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $min_invest = $plan->setting['initial_investment_amount'];

            $total = abs($request->get('buy_in_amount', 0));
            $c1 = abs($request->get('credit_1_amount', 0));
            $c2 = abs($request->get('credit_2_amount', 0));

            if ($total < $min_invest) {
                throw new \Exception(trans('common.minimum_buy_in_x', ['x' => $min_invest]));
            }

            if ($total % $plan->setting['investment_multiplication_of_amount'] != 0) {
                throw new \Exception(trans('common.buy_in_amount_must_be_multiplication_of_x', ['x' => $plan->setting['investment_multiplication_of_amount']]));
            }

            if (bcadd($c1, $c2) != $total) {
                throw new \Exception(trans('common.x_plus_y_must_be_equal_to_z', ['x' => trans('common.credit_1_short'), 'y' => trans('common.credit_2_short'), 'z' => $total]));
            }

            if ($c1 > 0 && $me->credit_1 < $c1) {
                throw new \Exception(trans('common.insufficient_x', ['x' => trans('common.credit_1_short')]));
            }

            if ($c2 > 0 && $me->credit_2 < $c2) {
                throw new \Exception(trans('common.insufficient_x', ['x' => trans('common.credit_2_short')]));
            }

            $rk = \Str::uuid();
            $params = [
                'code' => $plan->code,
                'plan_name' => $plan->plan_name,
            ];

            if ($c1 > 0) {
                $ut1 = new UserTransaction();
                $ut1->user_id = $me->id;
                $ut1->credit_type = 1;
                $ut1->transaction_type = 301;
                $ut1->amount = 0 - $c1;
                $ut1->params = $params;
                $ut1->related_key = $rk;
                $ut1->save();

                User::where('id', '=', $me->id)
                    ->decrement('credit_1', $c1);
            }

            if ($c2 > 0) {
                $ut2 = new UserTransaction();
                $ut2->user_id = $me->id;
                $ut2->credit_type = 2;
                $ut2->transaction_type = 301;
                $ut2->amount = 0 - $c2;
                $ut2->params = $params;
                $ut2->related_key = $rk;
                $ut2->save();

                User::where('id', '=', $me->id)
                    ->decrement('credit_2', $c2);
            }

            $ui = new UserInvestment();
            $ui->user_id = $me->id;
            $ui->fillInvestmentPlan($plan);
            $ui->total_invest = $total;
            $ui->current_balance = $total;
            $ui->status = 0;
            $ui->related_key = $rk;
            $ui->params = $params;
            $ui->save();

            $uit = new UserInvestmentTransaction();
            $uit->user_id = $me->id;
            $uit->user_investment_id = $ui->id;
            $uit->investment_plan_id = $plan->id;
            $uit->transaction_type = 1;
            $uit->amount = $total;
            $uit->active = 0;
            $uit->related_key = $rk;
            $uit->params = $params;
            $uit->save();

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }
}
