<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function checkAccountname(Request $request)
    {
        try {
            $user = User::where('username', '=', $request->get('username'))
                ->first();

            if (! $user) {
                return makeResponse(true, null);
            }

            return makeResponse(true, null, [
                'name' => $user->name ?? null,
            ]);
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }
}
