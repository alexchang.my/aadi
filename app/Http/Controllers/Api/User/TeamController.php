<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserInvestment;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function fetchData(Request $request)
    {
        $me = $request->user();

        $cache_key = 'user-team-data-'.$me->id;

        if (! config('APP_ENV') != 'local' && cache()->has($cache_key)) {
            return makeResponse(true, null, cache()->get($cache_key));
        } else {
            $data = [];

            $data['direct_team_size'] = User::where('introducer_user_id', '=', $me->id)->count();
            $data['direct_team_portfolio'] = UserInvestment::whereHas('user', function ($q) use ($me) {
                $q->where('introducer_user_id', '=', $me->id);
            })->Active()->sum('current_balance');
            $data['team_size'] = $me->downlines()->count();
            $data['team_portfolio'] = UserInvestment::whereHas('user', function ($q) use ($me) {
                $q->whereHas('unilevels', function ($iq) use ($me) {
                    $iq->where('introducer_user_id', '=', $me->id);
                });
            })->Active()->sum('current_balance');
            $data['direct_team'] = User::where('introducer_user_id', '=', $me->id)
                ->withSum(['activeInvestments as total_investments'], 'current_balance')
                ->orderBy('id', 'DESC')
                ->get(['id', 'member_id', 'username', 'name', 'ranking', 'downline_ranking_1', 'downline_ranking_2', 'downline_ranking_3']);

            cache()->set($cache_key, $data, now()->addMinutes(10));

            return makeResponse(true, null, $data);
        }
    }

    public function hierarchy(Request $request)
    {
        $me = $request->user();

        if ($request->filled('id') && $request->filled('children_only')) {
            $user = User::whereHas('unilevels', function ($q) use ($me) {
                $q->where('introducer_user_id', '=', $me->id);
            })->find($request->get('id'));

            if (! $user) {
                return makeResponse(true, null);
            }

            $temp_childs = $user->directDownlines()
                ->withCount(['directDownlines as direct_downlines_count'])
                ->withSum(['activeInvestmentsIgnoreOptInStatus as total_investments'], 'current_balance')
                ->orderBy('id', 'ASC')->get();

            if (! $temp_childs) {
                return makeResponse(true, null);
            }

            $childs = [];
            if ($temp_childs) {
                foreach ($temp_childs as $c) {
                    $childs[] = [
                        'id' => $c->id,
                        'text' => $c->username,
                        'active_investment' => $c->total_investments,
                        'has_children' => $c->direct_downlines_count > 0,
                        'ranking' => $c->ranking,
                    ];
                }
            }

            return makeResponse(true, null, [
                'childrens' => $childs,
//                'c' => $temp_childs,
            ]);
        } else {
            $temp_childs = $me->directDownlines()
                ->withCount(['directDownlines as direct_downlines_count'])
                ->withSum(['activeInvestmentsIgnoreOptInStatus as total_investments'], 'current_balance')
                ->orderBy('id', 'ASC')->get();
            $childs = [];
            if ($temp_childs) {
                foreach ($temp_childs as $c) {
                    $childs[] = [
                        'id' => $c->id,
                        'text' => $c->username,
                        'active_investment' => $c->total_investments,
                        'has_children' => $c->direct_downlines_count > 0,
                        'ranking' => $c->ranking,
                    ];
                }
            }

            $data = [
                'id' => $me->id,
                'text' => $me->username,
                'active_investment' => $me->activeInvestmentsIgnoreOptInStatus()->sum('current_balance'),
                'has_children' => (bool) $temp_childs,
                'ranking' => $me->ranking,
                'childrens' => $childs,
            ];

            return makeResponse(true, null, $data);
        }
    }
}
