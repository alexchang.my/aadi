<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Managers\UserManager;
use App\Models\AccountLoginLog;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class AccountController extends Controller
{
    public function registerBuildForm(Request $request)
    {
        $data = [];
        if ($request->filled('referral_member_id')) {
            $referral = User::where('member_id', '=', $request->get('referral_member_id'))->first();
            if ($referral) {
                $data['referral']['username'] = $referral->username;
                $data['referral']['member_id'] = $referral->member_id;
            }
        }

        return makeResponse(true, null, $data);
    }

    public function register(Request $request)
    {
        $rules = [
            'step' => ['required', 'in:1,2'],
            'username' => ['required', 'isUsername', 'unique:users,username'],
            'email' => ['required', 'email'],
            'contact_country_id' => ['required', 'exists:country,id,status,1'],
            'contact_number' => ['required', 'isContactNumber'],
            'referral_member_id' => ['required', 'exists:users,member_id'],
        ];

        if ($request->get('step') == 2) {
            $rules['password'] = ['required', 'confirmed', 'isPassword'];
            $rules['password2'] = ['required', 'confirmed', 'isPassword2'];
        }

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $referral = User::where('member_id', '=', $request->get('referral_member_id'))->first();

            if (! $referral) {
                throw new \Exception(trans('common.record_not_found'));
            }

            if ($request->get('step') == 1) {
                return makeResponse(true, null);
            }

            $user = new User();
            $user->introducer_user_id = $referral->id;
            $user->unilevel = $referral->unilevel + 1;
            $user->username = $request->get('username');
            $user->email = $request->get('email');
            $user->country_id = $request->get('contact_country_id');
            $user->contact_country_id = $request->get('contact_country_id');
            $user->contact_number = $request->get('contact_number');
            $user->password = $request->get('password');
            $user->password2 = $request->get('password2');
            $user->save();

            $token_result = $user->createToken('Personal Access Token');

            $log = new AccountLoginLog();
            $log->setAccount($user);
            $log->login_type = 'login';
            $log->save();

            $user->last_login_at = $log->created_at;
            $user->save();

            $um = new UserManager($user);
            $um->triggerRegister($referral);

            \DB::commit();

            $cache_key = 'user-team-data-'.$referral->id;
            cache()->forget($cache_key);

            return makeResponse(true, null, [
                'token' => $token_result->plainTextToken,
            ]);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => ['required', 'isUsername'],
            'password' => ['required', 'isPassword'],
        ]);

        try {
            \DB::beginTransaction();

            $user = User::where('username', '=', $request->get('username'))
                ->first();

            if (! $user) {
                throw new \Exception(trans('common.incorrect_credentials'));
            }

            if (! $user->correctPassword($request->get('password'))) {
                return makeResponseErrorForField('password', trans('common.incorrect_credentials'));
            }

            //SINGLE LOGIN
            $user->tokens()->delete();

            $token_result = $user->createToken('Personal Access Token');

            $log = new AccountLoginLog();
            $log->setAccount($user);
            $log->login_type = 'login';
            $log->save();

            $user->last_login_at = $log->created_at;
            $user->save();

            \DB::commit();

            return makeResponse(true, null, [
                'token' => $token_result->plainTextToken,
            ]);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function logout(Request $request)
    {
        if ($request->user()) {
            $request->user()->tokens()->delete();
        }

        session()->flush();

        return makeResponse(true, null);
    }

    public function fetch(Request $request)
    {
        $user = $request->user();

        if (! $user->last_login_at || ! $user->last_login_at->isToday()) {
            try {
                \DB::beginTransaction();

                $log = new AccountLoginLog();
                $log->setAccount($user);
                $log->login_type = 'auto_login';
                $log->save();

                $user->last_login_at = $log->created_at;
                $user->save();

                \DB::commit();
            } catch (\Exception $e) {
                \DB::rollBack();

                return makeResponse(false, $e);
            }
        }

        return response()->json($request->user());
    }

    public function changeLanguage(Request $request)
    {
        if ($request->user()) {
            $user = $request->user();
            $user->lang = $request->get('language');
            if (! array_key_exists($user->lang, config('app.locales'))) {
                $user->lang = config('app.fallback_locale');
            }
            $user->save();
        }

        return makeResponse(true, null);
    }

    public function profileSubmit(Request $request)
    {
        $me = $request->user();

        $rules = [
            'email' => ['required', 'email', 'unique:users,email,'.$me->id],
            'name' => ['required', 'string', 'min:1', 'max:42'],
            'contact_country_id' => ['required', 'exists:country,id,status,1'],
            'contact_number' => ['required', 'isContactNumber'],
            'current_password2' => ['required', 'isPassword2'],
        ];

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            if (! $me->correctPassword2($request->get('current_password2'))) {
                throw new \Exception(trans('common.incorrect_password2'));
            }

            $me->email = $request->get('email');
            $me->name = $request->get('name');
            $me->contact_country_id = $request->get('contact_country_id');
            $me->contact_number = $request->get('contact_number');
            $me->save();

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function securitySubmit(Request $request)
    {
        $me = $request->user();

        $rules = [
            'current_password2' => ['required', 'isPassword2'],
        ];

        if ($request->filled('new_password')) {
            $rules['new_password'] = ['required', 'confirmed', 'isPassword'];
        }

        if ($request->filled('new_password2')) {
            $rules['new_password2'] = ['required', 'confirmed', 'isPassword2'];
        }

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            if (! $me->correctPassword2($request->get('current_password2'))) {
                throw new \Exception(trans('common.incorrect_password2'));
            }

            if ($request->filled('new_password')) {
                $me->password = $request->get('new_password');
            }

            if ($request->filled('new_password2')) {
                $me->password2 = $request->get('new_password2');
            }

            $me->save();

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function forgetPassword(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
        ]);

        try {
            \DB::beginTransaction();

            $status = Password::sendResetLink(
                $request->only('email')
            );

            \DB::commit();

            if ($status === Password::RESET_LINK_SENT) {
                return makeResponse(true, null);
            } else {
                throw new \Exception(trans($status));
            }
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
            'username' => ['required', 'isUsername', 'exists:users,username'],
            'password' => ['required', 'isPassword', 'confirmed'],
            'password2' => ['required', 'isPassword2', 'confirmed'],
        ]);

        try {
            \DB::beginTransaction();

            $status = Password::reset(
                $request->only('email', 'password', 'password_confirmation', 'token', 'username'),
                function ($user) use ($request) {
                    $user->password = $request->get('password');
                    $user->password2 = $request->get('password2');
                    $user->save();

                    event(new PasswordReset($user));
                }
            );

            \DB::commit();

            if ($status === Password::PASSWORD_RESET) {
                return makeResponse(true, null);
            } else {
                throw new \Exception(trans($status));
            }
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }
}
