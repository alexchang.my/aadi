<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $data = [];
        $data['articles'] = Article::loadAllFromCache();

        return makeResponse(true, null, $data);
    }
}
