<?php

namespace App\Http\Controllers\Api\Admin\Account;

use App\Http\Controllers\Controller;
use App\Models\DailyReport;
use App\Models\DistributionSetting;
use App\Models\InvestmentPlan;
use App\Models\TimeTable;
use Illuminate\Http\Request;

class TraderDeclareController extends Controller
{
    public function buildForm(Request $request)
    {
        $data = [];
        $data['setting'] = DistributionSetting::where('plan_type', '=', $request->get('plan_type'))->Pending()->Latest()->first();

        $data['plans'] = InvestmentPlan::withSum(['userInvestments as total_investments' => function ($query) use ($request, $data) {
            $query->Active();
            if ($request->get('plan_type') == 'structured') {
                if ($data['setting']) {
                    $query->whereNotNull('opt_in_at')
                        ->where('opt_in_at', '<', $data['setting']->created_at);
                }
            }
        }], 'current_balance')->orderBy('id', 'ASC')->get();

        $data['last_daily_report'] = DailyReport::where('plan_type', '=', $request->get('plan_type'))->orderBy('id', 'DESC')->first();

        return makeResponse(true, null, $data);
    }

    public function saveTraderDeclare(Request $request)
    {
        $rules = [
            'plan_type' => ['required', 'in:assured,structured'],
            'declare_fund' => ['required', 'isFund'],
        ];

        $admin = $request->user();
        if (!$admin->hasPermission('trader_declare')) {
            throw new \Exception(trans('common.permission_denied'));
        }

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            if (! TimeTable::canOperateSensitiveAction()) {
                throw new \Exception(TimeTable::getOperateSensitiveActionErrormessage());
            }

            $me = $request->user();

            $setting = DistributionSetting::where('plan_type', '=', $request->get('plan_type'))->Pending()->Latest()
                ->first();

            if (!$setting) {
                $setting = new DistributionSetting();
                $setting->declare_fund = 0;
                $setting->admin_id = $me->id;
                $setting->plan_type = $request->get('plan_type');
                $setting->status = 0;
                $setting->save();
            }

            $setting->declare_fund = $request->get('declare_fund');

            if ($setting->approved == 1) {
                $setting->approved = 0;
            }
            $setting->save();

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function saveRoiAdjustment(Request $request)
    {
        $rules = [
            'plan_type' => ['required', 'in:assured,structured'],
        ];

        $plans = InvestmentPlan::orderBy('id', 'ASC')->get();
        $plans = $plans->filter(function ($p) use ($request) {
            if ($p->setting && $p->setting['plan_type'] === $request->get('plan_type')) {
                return true;
            }

            return false;
        });

        $admin = $request->user();
        if (!$admin->hasPermission('roi_adjustment')) {
            throw new \Exception(trans('common.permission_denied'));
        }

        foreach ($plans as $plan) {
            $f = 'adjusted_pay_rate_'.mb_strtoupper($plan->code);

            if ($request->filled($f) && $request->get($f) != '') {
                $rules[$f] = ['required', 'isFund'];
            }
        }

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            if (! TimeTable::canOperateSensitiveAction()) {
                throw new \Exception(TimeTable::getOperateSensitiveActionErrormessage());
            }

            $me = $request->user();

            $setting = DistributionSetting::where('plan_type', '=', $request->get('plan_type'))->Pending()->Latest()
                ->first();

            if (!$setting) {
                $setting = new DistributionSetting();
                $setting->declare_fund = 0;
                $setting->admin_id = $me->id;
                $setting->plan_type = $request->get('plan_type');
                $setting->status = 0;
                $setting->save();
            }

            $percentages = [];

            foreach ($plans as $plan) {
                $f = 'adjusted_pay_rate_'.mb_strtoupper($plan->code);

                if ($request->filled($f) && $request->get($f) != '') {
                    $p = $request->get($f);
                    if ($p != '' && $p) {
                        //NOW ALLOW ANY PERCENTAGE
                        $percentages[mb_strtoupper($plan->code)] = $p;
                        // if ($p >= $plan->setting['min_return_percentage_per_cycle'] && $p <= $plan->setting['max_return_percentage_per_cycle']) {
                        //     $percentages[mb_strtoupper($plan->code)] = $p;
                        // } else {
                        //     throw new \Exception($plan->plan_name.' adjusted percentage must within '.$plan->setting['min_return_percentage_per_cycle'].' to '.$plan->setting['max_return_percentage_per_cycle']);
                        // }
                    }
                }
            }

            if (count($percentages)) {
                $setting->adjusted_percentage = $percentages;
            }

            if ($setting->approved == 1) {
                $setting->approved = 0;
            }
            $setting->save();

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function toggleApproveDeclare(Request $request)
    {
        $rules = [
            'plan_type' => ['required', 'in:assured,structured'],
        ];

        $admin = $request->user();
        if (!$admin->hasPermission('approve_roi_adjustment')) {
            throw new \Exception(trans('common.permission_denied'));
        }

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            if (! TimeTable::canOperateSensitiveAction()) {
                throw new \Exception(TimeTable::getOperateSensitiveActionErrormessage());
            }

            $setting = DistributionSetting::where('plan_type', '=', $request->get('plan_type'))->Pending()->Latest()
                ->first();

            if (!$setting) {
                throw new \Exception(trans('common.record_not_found'));
            }

            if ($setting->declare_fund <= 0) {
                throw new \Exception('Please declare fund');
            }

            if ($setting->approved == 0) {
                $setting->approved = 1;
            } else {
                $setting->approved = 0;
            }

            $setting->save();

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

}
