<?php

namespace App\Http\Controllers\Api\Admin\Account;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\AccountTransaction;
use App\Models\TimeTable;
use Illuminate\Http\Request;

class AccountingController extends Controller
{
    public function fetchBalance(Request $request)
    {
        return makeResponse(true, null, [
            'account' => Account::where('account_code', '=', $request->get('account_code'))->orderBy('id', 'ASC')->first(),
        ]);
    }

    public function submitAdjust(Request $request)
    {
        $rules = [
            'account_code' => ['required', 'exists:accounts,account_code'],
            'amount' => ['required', 'isFund'],
        ];

        $rules['account_action'] = ['required', 'in:'.implode(',', Account::getAllowedActions($request->get('account_code')))];

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            if (! TimeTable::canOperateSensitiveAction()) {
                throw new \Exception(TimeTable::getOperateSensitiveActionErrormessage());
            }

            $acc = Account::where('account_code', '=', $request->get('account_code'))->orderBy('id', 'ASC')->first();

            $amount = abs($request->get('amount', 0));

            if (! $acc || $amount <= 0) {
                throw new \Exception(trans('common.unknown_error'));
            }

            if (! $acc->isTheActionAllowed($request->get('account_action'))) {
                throw new \Exception(trans('common.permission_denied'));
            }

            $admin = $request->user();

            $rk = \Str::uuid();
            $remark = $request->get('remark', null);
            $accounts = Account::getAccountCodeLists();

            switch ($request->get('account_action')) {
                case 'transfer_to_assured_account':
                    $assured = Account::where('account_code', '=', Account::ASSURED_ACCOUNT_CODE)->orderBy('id', 'ASC')->first();

                    $params = [
                        'from_account' => $accounts[$acc->account_code],
                        'from_account_id' => $acc->id,
                        'from_account_code' => $acc->account_code,
                        'to_account' => $accounts[$assured->account_code],
                        'to_account_id' => $assured->id,
                        'to_account_code' => $assured->account_code,
                    ];

                    $acc->debitFromAccount(
                        amount: $amount,
                        transaction_type: 1,
                        created_by_admin: $admin,
                        verified_by_admin: $admin,
                        verified: true,
                        remark: $remark,
                        related_key: $rk,
                        params: $params
                    );

                    $assured->creditToAccount(
                        amount: $amount,
                        transaction_type: 2,
                        created_by_admin: $admin,
                        remark: $remark,
                        related_key: $rk,
                        params: $params
                    );
                    break;
                case 'transfer_to_structured_account':
                    $structured = Account::where('account_code', '=', Account::STRUCTURED_ACCOUNT_CODE)->orderBy('id', 'ASC')->first();

                    $params = [
                        'from_account' => $accounts[$acc->account_code],
                        'from_account_id' => $acc->id,
                        'from_account_code' => $acc->account_code,
                        'to_account' => $accounts[$structured->account_code],
                        'to_account_id' => $structured->id,
                        'to_account_code' => $structured->account_code,
                    ];

                    $acc->debitFromAccount(
                        amount: $amount,
                        transaction_type: 1,
                        created_by_admin: $admin,
                        verified_by_admin: $admin,
                        verified: true,
                        remark: $remark,
                        related_key: $rk,
                        params: $params
                    );

                    $structured->creditToAccount(
                        amount: $amount,
                        transaction_type: 2,
                        created_by_admin: $admin,
                        remark: $remark,
                        related_key: $rk,
                        params: $params
                    );
                    break;
                case 'transfer_to_opex_account':
                    $opex = Account::where('account_code', '=', Account::OPEX_ACCOUNT_CODE)->orderBy('id', 'ASC')->first();

                    $params = [
                        'from_account' => $accounts[$acc->account_code],
                        'from_account_id' => $acc->id,
                        'from_account_code' => $acc->account_code,
                        'to_account' => $accounts[$opex->account_code],
                        'to_account_id' => $opex->id,
                        'to_account_code' => $opex->account_code,
                    ];

                    $acc->debitFromAccount(
                        amount: $amount,
                        transaction_type: 1,
                        created_by_admin: $admin,
                        verified_by_admin: $admin,
                        verified: true,
                        remark: $remark,
                        related_key: $rk,
                        params: $params
                    );

                    $opex->creditToAccount(
                        amount: $amount,
                        transaction_type: 2,
                        created_by_admin: $admin,
                        remark: $remark,
                        related_key: $rk,
                        params: $params
                    );

                    break;
                case 'credit':
                    $acc->creditToAccount(
                        amount: $amount,
                        transaction_type: 101,
                        created_by_admin: $admin,
                        remark: $remark,
                        related_key: $rk,
                    );
                    break;
                case 'debit':
                    $acc->debitToAccount(
                        amount: $amount,
                        transaction_type: 151,
                        created_by_admin: $admin,
                        remark: $remark,
                        related_key: $rk,
                    );
                    break;
                default:
                    throw new \Exception(trans('common.permission_denied'));
                    break;
            }

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function verifyTransaction(Request $request)
    {
        try {
            \DB::beginTransaction();

            if (! TimeTable::canOperateSensitiveAction()) {
                throw new \Exception(TimeTable::getOperateSensitiveActionErrormessage());
            }

            $transaction = AccountTransaction::find($request->get('id'));

            if (! $transaction || ! $transaction->canVerify()) {
                throw new \Exception(trans('common.permission_denied'));
            }

            $transaction->verified = 1;
            $transaction->verified_at = now();
            $transaction->verified_by_admin_id = $request->user()->id;
            $transaction->triggerVerified();
            $transaction->save();

            if ($request->get('verified') === true) {
            } else {
                $return_to_account = Account::find($transaction->params['from_account_id']);
                $return_from_account = $transaction->account;

                $params = [
                    'return_to_account' => $return_to_account->account_name,
                    'return_to_account_id' => $return_to_account->id,
                    'return_from_account' => $return_from_account->account_name,
                    'return_from_account_id' => $return_from_account->id,
                ];

                $rk = $transaction->related_key;
                $amount = abs($transaction->amount);
                $admin = $request->user();

                $return_from_account->debitFromAccount(
                    amount: 0 - $amount,
                    transaction_type: 3,
                    created_by_admin: $admin,
                    verified_by_admin: $admin,
                    verified: true,
                    related_key: $rk,
                    params: $params
                );

                $return_to_account->creditToAccount(
                    amount: $amount,
                    transaction_type: 4,
                    created_by_admin: $admin,
                    verified_by_admin: $admin,
                    verified: true,
                    related_key: $rk,
                    params: $params
                );
            }

            \DB::commit();

            return makeResponse(true, null);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }
}
