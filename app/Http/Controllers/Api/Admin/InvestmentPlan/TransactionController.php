<?php

namespace App\Http\Controllers\Api\Admin\InvestmentPlan;

use App\Http\Controllers\Controller;
use App\Models\InvestmentTransaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function loadStatistics(Request $request)
    {
        $data = [];

        $data['pending_fund_acceptance'] = InvestmentTransaction::Pending()->where('transaction_type', '=', 1)->whereHas('investmentPlan', function ($q) use ($request) {
            $q->where('code', '=', $request->get('code'));
        })->sum('amount');
        $data['accepted_fund'] = InvestmentTransaction::Accepted()->where('transaction_type', '=', 1)->whereHas('investmentPlan', function ($q) use ($request) {
            $q->where('code', '=', $request->get('code'));
        })->sum('amount');
        $data['early_withdrawal'] = InvestmentTransaction::Accepted()->where('transaction_type', '=', 2)->whereHas('investmentPlan', function ($q) use ($request) {
            $q->where('code', '=', $request->get('code'));
        })->sum('amount');
        $data['matured'] = InvestmentTransaction::Accepted()->where('transaction_type', '=', 3)->whereHas('investmentPlan', function ($q) use ($request) {
            $q->where('code', '=', $request->get('code'));
        })->sum('amount');

        return makeResponse(true, null, ['statistics' => $data]);
    }
}
