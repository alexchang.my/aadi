<?php

namespace App\Http\Controllers\Api\Admin\InvestmentPlan;

use App\Http\Controllers\Controller;
use App\Models\InvestmentPlan;
use Illuminate\Http\Request;

class InvestmentPlanController extends Controller
{
    public function buildForm(Request $request)
    {
        $data = [];

        $data['model'] = InvestmentPlan::find($request->get('id'));

        return makeResponse(true, null, $data);
    }

    public function submitForm(Request $request)
    {
        $rules = [
            'plan_name' => ['required', 'string', 'min:1', 'max:64'],
            'upload_featured_photo' => ['nullable', 'image'],
            'upload_category_icon' => ['nullable', 'image'],
            'return_percentage_from' => ['required'],
            'return_percentage_to' => ['required'],
            'return_cycle' => ['required', 'string'],
            'fund_size' => ['required', 'isFund'],
            'fund_description' => ['nullable'],
        ];

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $model = InvestmentPlan::find($request->get('id'));

            if (! $model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->plan_name = $request->get('plan_name');
            if ($request->hasFile('upload_featured_photo')) {
                $model->featured_photo = $request->file('upload_featured_photo');
            }
            if ($request->hasFile('upload_category_icon')) {
                $model->category_icon = $request->file('upload_category_icon');
            }
            $model->return_percentage_from = $request->get('return_percentage_from');
            $model->return_percentage_to = $request->get('return_percentage_to');
            $model->return_cycle = $request->get('return_cycle');
            $model->fund_size = $request->get('fund_size');
            $model->fund_description = $request->get('fund_description');
            $model->save();

            \DB::commit();

            return makeResponse(true, null, ['model' => $model]);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function uploadImageByFile(Request $request)
    {
        try {
            \DB::beginTransaction();

            $file = $request->file('image');

            $img = \Image::make($file);

            $img->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $ext = $file->guessClientExtension();
            $path = getUploadFileBasePath('investment_plan/fund_description/'.now()->format('Y/m/'));
            $file_name = now()->format('d').'-'.generateRandomUniqueName(16);

            getUploadStorage()->put($path.$file_name.'.'.$ext, $img->stream(), 'public');

            \DB::commit();

            return makeResponse(true, null, [
                'url' => getUploadStorage()->url($path.$file_name.'.'.$ext),
            ]);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }
}
