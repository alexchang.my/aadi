<?php

namespace App\Http\Controllers\Api\Admin\Other;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function buildForm(Request $request)
    {
        try {
            $data = [];
            if ($request->filled('id')) {
                $data['model'] = Country::find($request->get('id'));
            }

            $data['status'] = Country::getStatusLists();

            return makeResponse(true, null, $data);
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }

    public function submitForm(Request $request)
    {
        $rules = [
            'id' => ['required', 'exists:country,id'],
            'ext' => ['required', 'string', 'min:1', 'max:8'],
            'currency_prefix' => ['nullable', 'string', 'min:1', 'max:8'],
            'conversion_rate' => ['required', 'isFund'],
            'status' => ['required', 'in:'.implode(',', array_keys(Country::getStatusLists()))],
        ];

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $model = Country::find($request->get('id'));

            if (! $model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->ext = $request->get('ext');
            $model->currency_prefix = $request->get('currency_prefix');
            $model->conversion_rate = $request->get('conversion_rate', 0);
            $model->status = $request->get('status', 0);
            $model->save();

            \DB::commit();

            return makeResponse(true, null, [
                'model' => $model,
            ]);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }
}
