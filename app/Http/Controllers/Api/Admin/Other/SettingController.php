<?php

namespace App\Http\Controllers\Api\Admin\Other;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function buildForm(Request $request)
    {
        return makeResponse(true, null, [
            'model' => Setting::find($request->get('id')),
        ]);
    }

    public function submitForm(Request $request)
    {
        $this->validate($request, [
            'id' => ['required', 'exists:settings,id'],
            'setting_value' => ['required'],
        ]);

        try {
            \DB::beginTransaction();

            $model = Setting::find($request->get('id'));

            if (! $model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->setting_value = $request->get('setting_value');
            $model->save();

            \DB::commit();

            return makeResponse(true, null, ['model' => $model]);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function uploadImageByFile(Request $request)
    {
        try {
            \DB::beginTransaction();

            $file = $request->file('image');

            $img = \Image::make($file);

            $img->resize(700, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $ext = $file->guessClientExtension();
            $path = getUploadFileBasePath('setting/'.now()->format('Y/m/'));
            $file_name = now()->format('d').'-'.generateRandomUniqueName(16);

            getUploadStorage()->put($path.$file_name.'.'.$ext, $img->stream(), 'public');

            \DB::commit();

            return makeResponse(true, null, [
                'url' => getUploadStorage()->url($path.$file_name.'.'.$ext),
            ]);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }
}
