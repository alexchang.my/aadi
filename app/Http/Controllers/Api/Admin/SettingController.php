<?php

namespace App\Http\Controllers\Api\Admin;

use App\Config;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function fetch(Request $request)
    {
        $settings = [];
        $settings['country'] = Country::where('status', '=', 1)->get();
        $settings['default_country_code'] = Config::DEFAULT_COUNTRY_CODE;
        $settings['settings'] = Setting::get();

        return makeResponse(true, null, [
            'settings' => $settings,
        ]);
    }

    public function submit(Request $request)
    {
        $this->validate($request, [
            'id' => ['required', 'exists:settings,id'],
            'setting_value' => ['required'],
        ]);

        try {
            \DB::beginTransaction();

            $model = Setting::find($request->get('id'));

            if (! $model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->setting_value = $request->get('setting_value');
            $model->save();

            \DB::commit();

            return makeResponse(true, null, ['model' => $model]);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }
}
