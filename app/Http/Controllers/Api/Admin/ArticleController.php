<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function buildForm(Request $request)
    {
        $data = [];

        $data['categories'] = Article::getCategoryLists();

        if ($request->filled('id')) {
            $data['article'] = Article::find($request->get('id'));
        }

        return makeResponse(true, null, $data);
    }

    public function submitForm(Request $request)
    {
        $rules = [
            'subject' => ['required', 'string', 'min:1', 'max:48'],
            'content' => ['nullable', 'string'],
            'description' => ['required', 'string', 'min:1', 'max:255'],
            'category' => ['required', 'in:'.implode(',', array_keys(Article::getCategoryLists()))],
        ];

        if ($request->filled('id')) {
            $rules['id'] = ['required', 'exists:articles,id'];
            $rules['cover'] = ['nullable'];
        } else {
            $rules['cover'] = ['required', 'image'];
        }

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            if ($request->filled('id')) {
                $model = Article::find($request->get('id'));

                if (! $model) {
                    throw new \Exception(trans('common.record_not_found'));
                }
            } else {
                $model = new Article();
            }

            $model->subject = $request->get('subject');
            $model->content = $request->get('content');
            $model->description = $request->get('description');
            $model->category = $request->get('category');

            if (! $model->exists) {
                $model->save();
            }

            if ($request->hasFile('cover')) {
                $model->cover = $request->file('cover');
            }

            $model->save();

            \DB::commit();

            return makeResponse(true, null, ['article' => $model]);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function deleteModel(Request $request, $id)
    {
        $model = Article::find($id);

        if (! $model) {
            throw new \Exception(trans('common.record_not_found'));
        }

        $model->delete();

        return makeResponse(true, null);
    }

    public function uploadImageByFile(Request $request)
    {
        try {
            \DB::beginTransaction();

            $file = $request->file('image');

            $img = \Image::make($file);

            $img->resize(700, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $ext = $file->guessClientExtension();
            $path = getUploadFileBasePath('article/'.now()->format('Y/m/'));
            $file_name = now()->format('d').'-'.generateRandomUniqueName(16);

            getUploadStorage()->put($path.$file_name.'.'.$ext, $img->stream(), 'public');

            \DB::commit();

            return makeResponse(true, null, [
                'url' => getUploadStorage()->url($path.$file_name.'.'.$ext),
            ]);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }
}
