<?php

namespace App\Http\Controllers\Api\Admin\Withdrawal;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserTransaction;
use App\Models\UserWithdrawal;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function buildForm(Request $request)
    {
        try {
            $data = [];
            $data['model'] = UserWithdrawal::with(['user', 'country'])->find($request->get('id'));

            return makeResponse(true, null, $data);
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }

    public function submitForm(Request $request)
    {
        $rules = [
            'id' => ['required', 'exists:user_withdrawals,id,status,0'],
            'status' => ['required', 'in:1,2'],
        ];

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $model = UserWithdrawal::where('status', '=', 0)->find($request->get('id'));

            if (! $model) {
                throw new \Exception(trans('common.permission_denied'));
            }

            $model->status = $request->get('status', 0);

            //REFUND
            if ($model->status == 2) {
                $agt = new UserTransaction();
                $agt->user_id = $model->user->id;
                $agt->credit_type = 1;
                $agt->transaction_type = 202;
                $agt->amount = $model->amount;
                $agt->related_key = $model->related_key;
                $agt->params = $model->params;
                $agt->save();

                User::where('id', '=', $model->user->id)->increment('credit_'.$agt->credit_type, abs($agt->amount));
            }

            $model->admin_id = $request->user()->id;
            $model->save();

            if ($model->status == 1) {
                $model->triggerWithdrawSuccess();
            }

            \DB::commit();

            return makeResponse(true, null, [
                'model' => $model,
            ]);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }
}
