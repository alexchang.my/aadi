<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\AccountLoginLog;
use App\Models\Admin;
use App\Models\UserTopup;
use App\Models\UserWithdrawal;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function fetch(Request $request)
    {
        $admin = $request->user();

        $data = [
            'notifications' => []
        ];

        if ($admin) {
            $permissions = $admin->getEnablePermissions();
            if (isset($permissions['manage_topup'])) $data['notifications']['pending_topup'] = UserTopup::where('status', '=', 0)->count();
            if (isset($permissions['manage_withdrawal'])) $data['notifications']['pending_withdrawal'] = UserWithdrawal::where('status', '=', 0)->count();
        }

        return makeResponse(true, null, $data);
    }
}
