<?php

namespace App\Http\Controllers\Api\Admin\User;

use App\Http\Controllers\Controller;
use App\Models\UserTopup;
use Illuminate\Http\Request;

class TopupController extends Controller
{
    public function buildForm(Request $request)
    {
        try {
            $data = [];
            $data['model'] = UserTopup::with(['user', 'country'])->find($request->get('id'));

            return makeResponse(true, null, $data);
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }

    public function submitForm(Request $request)
    {
        $rules = [
            'id' => ['required', 'exists:user_topups,id,status,0'],
            'status' => ['required', 'in:1,2'],
        ];

//        if ($request->get('status') == 1) {
//            $rules['transaction_id'] = ['required', 'unique:user_topups,transaction_id'];
//        }

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $model = UserTopup::where('status', '=', 0)->find($request->get('id'));

            if (! $model || $model->status != 0) {
                throw new \Exception(trans('common.permission_denied'));
            }

//            $tx = UserTopup::where('transaction_id', '=', $request->get('transaction_id'))->first();
//
//            if ($request->get('status') == 1 && $tx) {
//                throw new \Exception('Duplicate transaction ID');
//            }

            $model->status = $request->get('status', 0);

            if ($model->status == 1) {
//                $model->transaction_id = $request->get('transaction_id');
                $model->triggerTopupSuccess();
            }

            $model->admin_id = $request->user()->id;
            $model->save();

            \DB::commit();

            return makeResponse(true, null, [
                'model' => $model,
            ]);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }
}
