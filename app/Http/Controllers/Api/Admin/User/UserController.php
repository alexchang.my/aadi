<?php

namespace App\Http\Controllers\Api\Admin\User;

use App\Http\Controllers\Controller;
use App\Models\AccountStatusLog;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function buildForm(Request $request)
    {
        return makeResponse(true, null, [
            'model' => \App\Models\User::find($request->get('id')),
        ]);
    }

    public function submitForm(Request $request)
    {
        $rules = [
            'email' => ['required', 'email'],
            'contact_country_id' => ['required', 'exists:country,id,status,1'],
            'contact_number' => ['required', 'isContactNumber'],
            'new_password' => ['nullable', 'isPassword', 'confirmed'],
            'new_password2' => ['nullable', 'isPassword2', 'confirmed'],
        ];

        $this->validate($request, $rules);

        try {
            \DB::beginTransaction();

            $model = User::find($request->get('id'));

            if (! $model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $model->name = $request->get('name');
            $model->email = $request->get('email');
            $model->country_id = $request->get('contact_country_id');
            $model->contact_country_id = $request->get('contact_country_id');
            $model->contact_number = $request->get('contact_number');
            if ($request->filled('new_password')) {
                $model->password = $request->get('new_password');
            }
            if ($request->filled('new_password2')) {
                $model->password2 = $request->get('new_password2');
            }

            $model->force_ranking = $request->get('force_ranking', 0);
            if (! $model->force_ranking) {
                $model->force_ranking = 0;
            }

            $recheck_ranking = false;
            if ($model->isDirty('force_ranking')) {
                $recheck_ranking = true;
            }

            $model->save();

            \DB::commit();

            if ($recheck_ranking) {
                $model->checkUserRanking();
            }

            return makeResponse(true, null, [
                'model' => $model,
            ]);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

    public function loginAccount(Request $request)
    {
        try {
            $model = User::find($request->get('id'));

            if (! $model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $token_result = $model->createToken('Personal Access Token');

            return makeResponse(true, null, ['token' => $token_result->plainTextToken]);
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }

    public function toggleBanStatus(Request $request)
    {
        try {
            \DB::beginTransaction();

            $model = User::find($request->get('id'));

            if (! $model) {
                throw new \Exception(trans('common.record_not_found'));
            }

            $act = new AccountStatusLog();
            $act->admin_id = $request->user()->id;
            $act->user_id = $model->id;
            if ($model->ban == 0) {
                $act->operation = 'ban';
                $act->save();

                $model->ban = 1;
                $model->save();
            } else {
                $act->operation = 'unban';
                $act->save();

                $model->ban = 0;
                $model->save();
            }

            $model->tokens()->delete();

            \DB::commit();

            return makeResponse(true, null, ['model' => $model]);
        } catch (\Exception $e) {
            \DB::rollBack();

            return makeResponse(false, $e);
        }
    }

//    public function delete(Request $request)
//    {
//        try {
//            \DB::beginTransaction();
//
//            $model = null;
//
//            if ($request->filled('id')) {
//                $model = Agent::find($request->get('id'));
//            }
//
//            if (! $model) {
//                throw new \Exception(trans('common.record_not_found'));
//            }
//
//            $model->delete();
//
//            \DB::commit();
//
//            return makeResponse(true, null);
//        } catch (\Exception $e) {
//            \DB::rollBack();
//
//            return makeResponse(false, $e);
//        }
//    }
}
