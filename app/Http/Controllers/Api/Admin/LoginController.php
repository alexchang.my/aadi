<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Login
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => ['required', 'isUsername'],
            'password' => ['required', 'isPassword'],
        ]);

        try {
            $admin = Admin::where('username', '=', $request->get('username'))
                ->first();

            if (! $admin) {
                throw new \Exception(trans('common.incorrect_credentials'));
            }

            if (! $admin->correctPassword($request->get('password'))) {
                return makeResponseErrorForField('password', trans('common.incorrect_credentials'));
            }

            //SINGLE LOGIN
            $admin->tokens()->delete();

            $token_result = $admin->createToken('Personal Access Token');

            return makeResponse(true, null, [
                'token' => $token_result->plainTextToken,
            ]);
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }
}
