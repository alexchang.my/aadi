<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\AccountLoginLog;
use App\Models\AccountTransaction;
use App\Models\DailyReport;
use App\Models\InvestmentTransaction;
use App\Models\User;
use App\Models\UserInvestment;
use App\Models\UserTopup;
use App\Models\UserWithdrawal;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        try {
            $new = [];
            $login = [];

            $now = now();
            $start = now()->startOfYear();
            $end = now()->endOfYear();

            $period = CarbonPeriod::create($start, '1 month', $end);

            foreach ($period as $dt) {
                $s = $dt->copy()->startOfMonth();
                $e = $dt->copy()->endOfMonth();

                $new[] = $this->getRegisterData($s, $e, $now->copy()->format('Y-m') != $dt->copy()->format('Y-m'));
                $login[] = $this->getLoginData($s, $e, $now->copy()->format('Y-m') != $dt->copy()->format('Y-m'));
            }

            $statistics = [
                'pending_deposit_verification' => UserTopup::where('status', '=', 0)->sum('topup_amount'),
                'pending_trader_fund_acceptance' => AccountTransaction::whereHas('account', function ($q) { return $q->where('account_code', '=', Account::ASSURED_ACCOUNT_CODE); })->where('verified', '=', 0)->sum('amount'),
                'pending_withdrawal_verification' => UserWithdrawal::where('status', '=', 0)->sum('amount'),
                'opex_outstanding_amount' => Account::where('account_code', '=', Account::OPEX_ACCOUNT_CODE)->sum('account_balance'),
                'payable_amount' =>
                    UserInvestment::sum('roi_balance') +
                    User::sum(\DB::raw('`credit_1` + `credit_2`')) +
                    InvestmentTransaction::whereIn('transaction_type', [1,2,3])->sum('amount') +
//                    Account::where('account_code', '=', Account::STRUCTURED_ACCOUNT_CODE)->orderBy('id', 'DESC')->first()->account_balance +
//                    Account::where('account_code', '=', Account::ASSURED_ACCOUNT_CODE)->orderBy('id', 'DESC')->first()->account_balance +
                    Account::where('account_code', '=', Account::PL_ACCOUNT_CODE)->orderBy('id', 'DESC')->first()->account_balance,
//                    InvestmentTransaction::Pending()->where('transaction_type', '=', 1)->sum('amount'),
                'health_a' =>
                    Account::where('account_code', '=', Account::DEPOSIT_ACCOUNT_CODE)->orderBy('id', 'DESC')->first()->account_balance +
                    DailyReport::where('plan_type', '=', 'assured')->orderBy('id', 'DESC')->first()->current_fund_size +
                    DailyReport::where('plan_type', '=', 'structured')->orderBy('id', 'DESC')->first()->current_fund_size +
                    AccountTransaction::whereHas('account', function ($query) {
                        $query->whereIn('account_code', [Account::ASSURED_ACCOUNT_CODE, Account::STRUCTURED_ACCOUNT_CODE]);
                    })->where('verified', '=', 0)->sum('amount'),
//                    InvestmentTransaction::Pending()->where('transaction_type', '=', 1)->sum('amount'),
                'active_assured_funds' => UserInvestment::where('plan_type', '=', 'assured')->Active()->sum('current_balance'),
                'active_structured_funds_opt_in' => UserInvestment::where('plan_type', '=', 'structured')->Active()->where('opt_in', '=', 1)->sum('current_balance'),
                'active_structured_funds_opt_out' => UserInvestment::where('plan_type', '=', 'structured')->whereIn('status', [2,3])->where('opt_in', '=', 0)->sum('current_balance'),
            ];

            $accounts = Account::orderBy('id' ,'ASC')->get()->keyBy('account_code');

            return makeResponse(true, null, [
                'new' => $new,
                'login' => $login,
                'statistics' => $statistics,
                'accounts' => $accounts
            ]);
        } catch (\Exception $e) {
            return makeResponse(false, $e);
        }
    }

    protected function getRegisterData(Carbon $from, Carbon $to, $can_cache)
    {
        $data = null;
        $cache_key = 'admin_register_data_'.$from->copy()->format('Y_m');

        if ($can_cache) {
            $data = cache()->get($cache_key);
        }

        if (($can_cache && $data === null) || ! $can_cache) {
            $count = User::where('created_at', '>=', $from)->where('created_at', '<=', $to)->count();

            if ($can_cache && $data === null) {
                cache()->rememberForever($cache_key, function () use ($count) {
                    return $count;
                });
            }

            $data = $count;
        }

        return (int) $data;
    }

    protected function getLoginData($from, $to, $can_cache)
    {
        $data = null;
        $cache_key = 'admin_login_data_'.$from->copy()->format('Y_m');

        if ($can_cache) {
            $data = cache()->get($cache_key);
        }

        if (($can_cache && $data === null) || ! $can_cache) {
            $count = AccountLoginLog::whereRaw(\DB::raw('`model` = "'.getClass(new User(), true)).'"')
                ->where('created_at', '>=', $from)->where('created_at', '<=', $to)->count();

            if ($can_cache && $data === null) {
                cache()->rememberForever($cache_key, function () use ($count) {
                    return $count;
                });
            }

            $data = $count;
        }

        return (int) $data;
    }
}
