1. vuejs using image must be variable, the workaround is wrap with single quote like below, else vite will error on prod build, EG:
```
    <img :src="'/img/logo-light.png'" class="h-36 mx-auto text-center">
```
2. after change resources/scripts/admin/lang/*/_permission.json need to run `php artisan sync:admin_permission` to keep php side updated
