module.exports = {
    root: true,
    env: {
        node: true
    },
    extends: [
        // 'eslint:recommended',
        'plugin:vue/vue3-recommended',
        'prettier'
    ],
    rules: {
        'vue/script-setup-uses-vars': 'error',
        'vue/no-v-html': [0],
    },
    plugins: ['vue'],
}
