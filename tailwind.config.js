/** @type {import('tailwindcss').Config} */
module.exports = {
    darkMode: 'class',
    content: [
        "./resources/views/app.blade.php",
        "./resources/scripts/**/*.{vue,js,ts,jsx,tsx}",
    ],
    theme: {
        extend: {
            fontFamily: {
                'cabin': ['Cabin'],
                'montserrat': ['Montserrat'],
                'roboto': ['Roboto', 'sans-serif'],
            },
            colors: {
                "primary": "rgb(100,25,230)",
                "secondary": "rgb(217,38,169)",
                "accent": "rgb(31,178,166)",
                "neutral": "rgb(25,29,36)",
                "base-100": "rgb(42,48,60)",
                "info": "rgb(58,191,248)",
                "success": "rgb(54,211,153)",
                "warning": "rgb(251,189,35)",
                "error": "rgb(248,114,114)",
            }
        },
    },
    plugins: [],
}
