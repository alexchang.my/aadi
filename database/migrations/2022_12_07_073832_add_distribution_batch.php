<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distribution_batch', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('distribution_setting_id')->nullable()->default(null);
            $table->date('distribution_date')->nullable()->default(null);
            $table->decimal('total_capital', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('total_roi', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->uuid('related_key')->nullable()->default(null);
            $table->json('params')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('distribution_setting_id')->references('id')->on('distribution_settings')->onDelete('set null');
        });
        Schema::table('user_investment_transactions', function (Blueprint $table) {
            $table->unsignedBigInteger('distribution_batch_id')->nullable()->default(null)->after('investment_plan_id');

            $table->foreign('distribution_batch_id', 'uit_dbi_foreign')->references('id')->on('distribution_batch')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_investment_transactions', function (Blueprint $table) {
            $table->dropForeign('uit_dbi_foreign');
            $table->dropColumn('distribution_batch_id');
        });
        Schema::dropIfExists('distribution_batch');
    }
};
