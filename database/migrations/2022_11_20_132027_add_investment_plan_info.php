<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_plans', function (Blueprint $table) {
            $table->id();
            $table->string('code', 18)->unique()->index();
            $table->text('plan_name')->nullable()->default(null);
            $table->text('featured_photo')->nullable()->default(null);
            $table->text('category_icon')->nullable()->default(null);
            $table->text('return_percentage_from')->nullable()->default(null);
            $table->text('return_percentage_to')->nullable()->default(null);
            $table->text('return_cycle')->nullable()->default(null);
            $table->text('fund_size')->nullable()->default(null);
            $table->longText('fund_description')->nullable()->default(null);
            $table->json('setting')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_plans');
    }
};
