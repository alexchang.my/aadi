<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_withdrawals', function (Blueprint $table) {
            $table->decimal('admin_fees_percentage', 18, 2)->default(0)->after('conversion_rate');
            $table->decimal('admin_fees_crypto', 36, 18)->default(0)->after('to_crypto_address');
            $table->decimal('receivable_crypto_amount', 36, 18)->default(0)->after('admin_fees_crypto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_withdrawals', function (Blueprint $table) {
            $table->dropColumn('admin_fees_percentage');
            $table->dropColumn('admin_fees_crypto');
            $table->dropColumn('receivable_crypto_amount');
        });
    }
};
