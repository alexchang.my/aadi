<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::table('accounts')->where('account_code', '=', 'TRADER')->update(['account_code' => 'ASSURED']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::table('accounts')->where('account_code', '=', 'ASSURED')->update(['account_code' => 'TRADER']);
    }
};
