<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_investments', function (Blueprint $table) {
            $table->string('contract_id', 42)->nullable()->default(null)->unique()->index()->after('id');
            $table->dateTime('lock_capital_from')->nullable()->default(null)->after('status');
        });

        foreach (\App\Models\UserInvestment::orderBy('id', 'ASC')->get() as $ct) {
            $ct->contract_id = $ct->code.'-'.(\App\Models\UserInvestment::CONTRACT_ID_START + $ct->id);
            $ct->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_investments', function (Blueprint $table) {
            $table->dropColumn('contract_id');
            $table->dropColumn('lock_capital_from');
        });
    }
};
