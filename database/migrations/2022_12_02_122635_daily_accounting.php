<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_reports', function (Blueprint $table) {
            $table->id();
            $table->date('report_date')->index()->unique();
            $table->decimal('in_fund', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('out_fund', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('generated_fund', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('current_fund_size', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('total_investor_capital', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('declared_fund_size', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('profit_loss', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('payable_allocation', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('actual_payable', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('accumulate_allocation_balance', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_reports');
    }
};
