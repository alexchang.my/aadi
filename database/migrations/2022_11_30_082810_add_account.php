<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->string('account_code', 64)->unique()->index();
            $table->string('account_name', 255)->nullable()->default(null);
            $table->decimal('account_balance', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('account_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('account_id')->nullable()->default(null);
            $table->unsignedBigInteger('created_by_admin_id')->nullable()->default(null);
            $table->unsignedBigInteger('verified_by_admin_id')->nullable()->default(null);
            $table->unsignedBigInteger('transaction_type');
            $table->decimal('amount', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->uuid('related_key')->nullable()->default(null);
            $table->json('params')->nullable()->default(null);
            $table->timestamp('verified_at')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('set null');
            $table->foreign('created_by_admin_id')->references('id')->on('admins')->onDelete('set null');
            $table->foreign('verified_by_admin_id')->references('id')->on('admins')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_transactions');
        Schema::dropIfExists('accounts');
    }
};
