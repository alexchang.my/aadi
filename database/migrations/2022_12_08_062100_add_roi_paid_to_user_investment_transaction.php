<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_investment_transactions', function (Blueprint $table) {
            $table->unsignedInteger('need_pay_commission')->default(0)->after('is_roi');
            $table->unsignedInteger('commission_paid')->default(0)->after('need_pay_commission');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_investment_transactions', function (Blueprint $table) {
            $table->dropColumn('need_pay_commission');
            $table->dropColumn('commission_paid');
        });
    }
};
