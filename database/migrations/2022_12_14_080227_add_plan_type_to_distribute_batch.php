<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('distribution_batch', function (Blueprint $table) {
            $table->string('plan_type', 32)->nullable()->default(null)->index()->after('distribution_date');
        });
        \DB::table('distribution_batch')->update(['plan_type' => 'assured']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('distribution_batch', function (Blueprint $table) {
            $table->dropColumn('plan_type');
        });
    }
};
