<?php

use App\Models\InvestmentTransaction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('investment_transactions', function (Blueprint $table) {
            $table->unsignedInteger('status')->index()->after('amount');
        });

        foreach (\App\Models\UserInvestment::with(['user'])->orderBy('id', 'ASC')->get() as $ui) {
            $t = InvestmentTransaction::where('user_id', '=', $ui->user_id)
                ->where('user_investment_id', '=', $ui->id)
                ->where('investment_plan_id', '=', $ui->investment_plan_id)
                ->where('transaction_type', '=', 1);

            if (! $t->count()) {
                $t = new InvestmentTransaction();
                $t->investment_plan_id = $ui->investment_plan_id;
                $t->user_investment_id = $ui->id;
                $t->user_id = $ui->user_id;
                $t->transaction_type = 1;
                $t->status = 0;
                $t->amount = $ui->total_invest;
                $t->related_key = $ui->related_key;
                $t->params = [
                    'contract_id' => $ui->contract_id,
                    'username' => $ui->user->username,
                    'member_id' => $ui->user->member_id,
                ];

                if ($ui->status != 0) {
                    $t->status = 1;
                }

                $t->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investment_transaction', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
};
