<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_investments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable()->default(null);
            $table->unsignedBigInteger('investment_plan_id')->nullable()->default(null);
            $table->string('code', 18)->unique()->index();
            $table->text('plan_name')->nullable()->default(null);
            $table->decimal('total_invest', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('current_balance', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('active_balance', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->unsignedInteger('status')->default(0);
            $table->dateTime('lock_capital_until')->nullable()->default(null);
            $table->json('setting')->nullable()->default(null);
            $table->uuid('related_key')->nullable()->default(null);
            $table->json('params')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('investment_plan_id')->references('id')->on('investment_plans')->onDelete('set null');
        });
        Schema::create('user_investment_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable()->default(null);
            $table->unsignedBigInteger('user_investment_id')->nullable()->default(null);
            $table->unsignedBigInteger('investment_plan_id')->nullable()->default(null);
            $table->unsignedInteger('transaction_type');
            $table->decimal('amount', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->unsignedInteger('active')->default(0);
            $table->uuid('related_key')->nullable()->default(null);
            $table->json('params')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('user_investment_id')->references('id')->on('user_investments')->onDelete('set null');
            $table->foreign('investment_plan_id')->references('id')->on('investment_plans')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_investment_transactions');
        Schema::dropIfExists('user_investments');
    }
};
