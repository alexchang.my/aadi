<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_transactions', function (Blueprint $table) {
            $table->string('is_bonus_type', 32)->nullable()->default(null)->after('after')->index();
        });

        \DB::table('user_transactions')->whereIn('transaction_type', [401, 501, 601])
            ->update(['is_bonus_type' => 'assured']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_transactions', function (Blueprint $table) {
            $table->dropColumn('is_bonus_type');
        });
    }
};
