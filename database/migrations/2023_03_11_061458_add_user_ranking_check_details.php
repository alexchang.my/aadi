<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('last_ranking_check_set_rank')->nullable()->default(null)->after('last_complete_ranking_check');
            $table->timestamp('last_ranking_1_check_at')->nullable()->default(null)->after('last_ranking_check_set_rank');
            $table->timestamp('last_ranking_2_check_at')->nullable()->default(null)->after('last_ranking_1_check_at');
            $table->timestamp('last_ranking_3_check_at')->nullable()->default(null)->after('last_ranking_2_check_at');
        });

        foreach (\App\Models\User::orderBy('id', 'ASC')->get() as $u) {
            $u->last_ranking_1_check_at = $u->created_at;
            $u->last_ranking_2_check_at = $u->created_at;
            $u->last_ranking_3_check_at = $u->created_at;
            $u->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('last_ranking_check_set_rank');
            $table->dropColumn('last_ranking_1_check_at');
            $table->dropColumn('last_ranking_2_check_at');
            $table->dropColumn('last_ranking_3_check_at');
        });
    }
};
