<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_investments', function (Blueprint $table) {
            $table->string('plan_type', 64)->index()->nullable()->default(null)->after('investment_plan_id');
        });
        foreach (\App\Models\UserInvestment::with(['investmentPlan'])->orderBy('id', 'ASC')->get() as $row) {
            $row->plan_type = $row->investmentPlan->setting['plan_type'];
            $row->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_investments', function (Blueprint $table) {
            $table->dropColumn('plan_type');
        });
    }
};
