<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('ranking')->default(0)->after('credit_5');
            $table->unsignedInteger('downline_ranking_1')->default(0)->after('ranking');
            $table->unsignedInteger('downline_ranking_2')->default(0)->after('downline_ranking_1');
            $table->unsignedInteger('downline_ranking_3')->default(0)->after('downline_ranking_2');
            $table->unsignedInteger('bmb_count')->default(0)->after('downline_ranking_3');
            $table->unsignedInteger('bmi_count')->default(0)->after('bmb_count');
            $table->timestamp('last_dispatch_ranking_check')->nullable()->default(null)->after('bmi_count');
            $table->timestamp('last_complete_ranking_check')->nullable()->default(null)->after('last_dispatch_ranking_check');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('ranking');
            $table->dropColumn('downline_ranking_1');
            $table->dropColumn('downline_ranking_2');
            $table->dropColumn('downline_ranking_3');
            $table->dropColumn('bmb_count');
            $table->dropColumn('bmi_count');
            $table->dropColumn('last_dispatch_ranking_check');
            $table->dropColumn('last_complete_ranking_check');
        });
    }
};
