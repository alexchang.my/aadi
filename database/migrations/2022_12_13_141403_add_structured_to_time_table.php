<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('time_tables', function (Blueprint $table) {
            $table->unsignedInteger('structured_roi_day')->default(0)->after('assured_roi_day');
            $table->unsignedInteger('structured_roi_paid')->default(0)->after('assured_roi_paid');
            $table->unsignedInteger('structured_roi_commission_paid')->default(0)->after('assured_roi_commission_paid');
            $table->unsignedInteger('structured_roi_fund_approved')->default(0)->after('assured_roi_fund_approved');
        });

        \DB::table('time_tables')->where('assured_roi_day', '=', 1)
            ->update(['structured_roi_day' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('time_tables', function (Blueprint $table) {
            $table->dropColumn('structured_roi_day');
            $table->dropColumn('structured_roi_paid');
            $table->dropColumn('structured_roi_commission_paid');
            $table->dropColumn('structured_roi_fund_approved');
        });
    }
};
