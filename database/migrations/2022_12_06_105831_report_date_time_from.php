<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('daily_reports', function (Blueprint $table) {
            $table->dateTime('report_datetime_from')->nullable()->default(null)->after('report_date');
            $table->dateTime('report_datetime_to')->nullable()->default(null)->after('report_datetime_from');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('daily_reports', function (Blueprint $table) {
            $table->dropColumn('report_datetime_from');
            $table->dropColumn('report_datetime_to');
        });
    }
};
