<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_investments', function (Blueprint $table) {
            $table->decimal('opt_in_min_amount', 18, \App\Config::DECIMAL_POINT)->default(0)->after('roi_balance');
        });

        foreach (\App\Models\UserInvestment::where('plan_type', '=', 'structured')->get() as $ui) {
            $ui->calculateOptInMinAmount();
            $ui->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_investments', function (Blueprint $table) {
            $table->dropColumn('opt_in_min_amount');
        });
    }
};
