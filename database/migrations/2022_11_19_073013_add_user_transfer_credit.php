<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_transfer_credits', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('from_user_id')->nullable()->default(null)->index();
            $table->unsignedBigInteger('to_user_id')->nullable()->default(null)->index();
            $table->unsignedInteger('from_credit_type');
            $table->unsignedInteger('to_credit_type');
            $table->decimal('from_amount', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('to_amount', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('conversion_rate', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->uuid('related_key')->nullable()->default(null);
            $table->json('params')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('from_user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('to_user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_transfer_credits');
    }
};
