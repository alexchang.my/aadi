<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->engine = 'innodb';
            $table->id();
            $table->unsignedBigInteger('country_id')->nullable()->default(null);
            $table->string('name_en', 255);
            $table->string('name_cn', 255);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('country_id')->references('id')->on('country')->onDelete('set null');
        });

        Schema::create('company_banks', function (Blueprint $table) {
            $table->engine = 'innodb';
            $table->id();
            $table->unsignedBigInteger('bank_id')->nullable()->default(null);
            $table->unsignedBigInteger('country_id')->nullable()->default(null);
            $table->string('name_en', 255);
            $table->string('name_cn', 255);
            $table->string('account_name', 255)->nullable()->default(null);
            $table->string('account_number', 255)->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('bank_id')->references('id')->on('banks')->onDelete('set null');
            $table->foreign('country_id')->references('id')->on('country')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_banks');
        Schema::dropIfExists('banks');
    }
};
