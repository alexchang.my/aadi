<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_topups', function (Blueprint $table) {
            $table->id();
            $table->string('order_number', 32)->unique()->index();
            $table->unsignedBigInteger('admin_id')->nullable()->default(null);
            $table->unsignedBigInteger('user_id')->nullable()->default(null);
            $table->unsignedBigInteger('country_id')->nullable()->default(null);
            $table->unsignedBigInteger('credit_type');
            $table->decimal('topup_amount', 36, 18)->nullable()->default(null);
            $table->decimal('conversion_rate', 18, \App\Config::DECIMAL_POINT)->nullable()->default(null);
            $table->string('from_crypto_symbol', 42)->nullable()->default(null)->index();
            $table->string('to_crypto_address', 255)->nullable()->default(null);
            $table->decimal('received_crypto_amount', 36, 18)->nullable()->default(null);
            $table->unsignedInteger('status')->default(0);
            $table->unsignedBigInteger('payment_method');
            $table->longText('receipt')->nullable()->default(null);
            $table->uuid('related_key')->nullable()->default(null);
            $table->json('params')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('country_id')->references('id')->on('country')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
