<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_locations', function (Blueprint $table) {
            $table->engine = 'innodb';
            $table->id();
            $table->unsignedBigInteger('country_id')->nullable()->default(null);
            $table->unsignedBigInteger('parent_id')->nullable()->default(null);
            $table->unsignedBigInteger('sorting')->default(0);
            $table->string('name_en', 255)->nullable()->default(null);
            $table->string('name_cn', 255)->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('country_id')->references('id')->on('country')->onDelete('set null');
            $table->foreign('parent_id')->references('id')->on('country_locations')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_locations');
    }
};
