<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_investment_transactions', function (Blueprint $table) {
            $table->dateTime('bonus_date_at')->nullable()->default(null)->after('params');
        });
        foreach (\App\Models\UserInvestmentTransaction::get() as $uit) {
            $uit->bonus_date_at = $uit->created_at;
            $uit->save();
        }
        Schema::table('user_transactions', function (Blueprint $table) {
            $table->dateTime('bonus_date_at')->nullable()->default(null)->after('params');
        });
        foreach (\App\Models\UserTransaction::get() as $uit) {
            $uit->bonus_date_at = $uit->created_at;
            $uit->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_investment_transactions', function (Blueprint $table) {
            $table->dropColumn('bonus_date_at');
        });
        Schema::table('user_transactions', function (Blueprint $table) {
            $table->dropColumn('bonus_date_at');
        });
    }
};
