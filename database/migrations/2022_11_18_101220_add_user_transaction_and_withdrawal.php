<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable()->default(null)->index();
            $table->unsignedInteger('credit_type')->index();
            $table->unsignedInteger('transaction_type');
            $table->decimal('before', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('amount', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->decimal('after', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->uuid('related_key')->nullable()->default(null);
            $table->json('params')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });

        Schema::create('user_withdrawals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('admin_id')->nullable()->default(null);
            $table->unsignedBigInteger('user_id')->nullable()->default(null);
            $table->unsignedBigInteger('bank_id')->nullable()->default(null);
            $table->unsignedBigInteger('country_id')->nullable()->default(null);
            $table->string('name_en', 255)->nullable()->default(null);
            $table->string('name_cn', 255)->nullable()->default(null);
            $table->string('bank_account_holder_name', 255);
            $table->string('bank_account_number', 255);
            $table->unsignedInteger('credit_type');
            $table->decimal('amount', 18, \App\Config::DECIMAL_POINT);
            $table->decimal('conversion_rate', 18, \App\Config::DECIMAL_POINT);
            $table->decimal('currency', 18, \App\Config::DECIMAL_POINT);
            $table->unsignedBigInteger('status')->default(0);
            $table->text('remark')->nullable()->default(null);
            $table->uuid('related_key')->nullable()->default(null);
            $table->json('params')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('bank_id')->references('id')->on('banks')->onDelete('set null');
            $table->foreign('country_id')->references('id')->on('country')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
