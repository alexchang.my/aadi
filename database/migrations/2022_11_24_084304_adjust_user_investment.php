<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_investments', function (Blueprint $table) {
            $table->decimal('total_roi', 18, \App\Config::DECIMAL_POINT)->default(0)->after('active_balance');
            $table->decimal('roi_balance', 18, \App\Config::DECIMAL_POINT)->default(0)->after('total_roi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_investments', function (Blueprint $table) {
            $table->dropColumn('total_roi');
            $table->dropColumn('roi_balance');
        });
    }
};
