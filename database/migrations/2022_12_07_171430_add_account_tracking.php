<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('investment_plan_id')->nullable()->default(null);
            $table->unsignedBigInteger('user_investment_id')->nullable()->default(null);
            $table->unsignedBigInteger('user_id')->nullable()->default(null);
            $table->unsignedInteger('transaction_type');
            $table->decimal('amount', 18, \App\Config::DECIMAL_POINT)->default(0);
            $table->uuid('related_key')->nullable()->default(null);
            $table->json('params')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('investment_plan_id')->references('id')->on('investment_plans')->onDelete('set null');
            $table->foreign('user_investment_id')->references('id')->on('user_investments')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });

        Schema::table('user_investment_transactions', function (Blueprint $table) {
            $table->unsignedInteger('is_roi')->default(0)->after('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_transactions');
        Schema::table('user_investment_transactions', function (Blueprint $table) {
            $table->dropColumn('is_roi');
        });
    }
};
