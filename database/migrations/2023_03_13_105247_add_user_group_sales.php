<?php

use App\Models\User;
use App\Models\UserInvestment;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->decimal('group_sales', 18, \App\Config::DECIMAL_POINT)->default(0)->after('credit_5');
        });

        foreach (\App\Models\User::orderBy('id', 'ASC')->get() as $user) {
            $user->group_sales = 0;
            $downlines = User::where('introducer_user_id', '=', $user->id)
                ->orderBy('id', 'ASC');
            if ($downlines->count()) {
                foreach ($downlines->get() as $g1) {
                    $ids = [$g1->id];
                    while ($ids) {
                        $user->group_sales += UserInvestment::whereIn('user_id', $ids)
                            ->sum('total_invest');
//                        $user->group_sales += UserInvestment::whereHas('user', function ($query) use ($ids) {
//                            $query->whereIn('introducer_user_id', $ids);
//                        })->sum('total_invest');

                        $ids = User::whereIn('introducer_user_id', $ids)
                            ->orderBy('id', 'ASC')->pluck('id')->toArray();
                    }
                }
            }

            $user->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('group_sales');
        });
    }
};
