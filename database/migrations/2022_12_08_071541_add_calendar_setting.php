<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_tables', function (Blueprint $table) {
            $table->engine = 'innodb';
            $table->id();
            $table->date('utc_date')->unique()->index();
            $table->string('utc_day', 12)->index();

            $table->unsignedInteger('assured_roi_day')->default(0);
            $table->unsignedInteger('assured_roi_paid')->default(0);
            $table->unsignedInteger('assured_roi_commission_paid')->default(0);
            $table->unsignedInteger('assured_roi_fund_approved')->default(0);

            $table->unsignedInteger('daily_report_generated')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_tables');
    }
};
