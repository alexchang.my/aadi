<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_investments', function (Blueprint $table) {
            $table->decimal('auto_opt_out_amount', 18, \App\Config::DECIMAL_POINT)->default(0)->after('opt_in_min_amount');
            $table->unsignedTinyInteger('already_auto_opt_out')->default(0)->after('auto_opt_out_amount');
        });

        foreach (\App\Models\UserInvestment::where('plan_type', '=', 'structured')->orderBy('id', 'ASC')->get() as $ui) {
            $ui->calculateAutoOptOutAmount();
            $ui->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_investments', function (Blueprint $table) {
            $table->dropColumn('auto_opt_out_amount');
            $table->dropColumn('already_auto_opt_out');
        });
    }
};
