<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country', function (Blueprint $table) {
            $table->id();
            $table->string('name', 64)->unique()->index();
            $table->string('iso2', 2)->unique()->index();
            $table->string('iso3', 3)->unique()->index();
            $table->string('ext', 64)->nullable()->default(null);
            $table->string('currency_prefix', 24)->nullable()->default(null);
            $table->string('currency_suffix', 24)->nullable()->default(null);
            $table->unsignedInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country');
    }
};
