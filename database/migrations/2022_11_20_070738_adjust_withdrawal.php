<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_withdrawals', function (Blueprint $table) {
            $table->dropForeign('user_withdrawals_bank_id_foreign');
        });
        Schema::table('user_withdrawals', function (Blueprint $table) {
            $table->dropColumn('bank_id');
            $table->dropColumn('name_en');
            $table->dropColumn('name_cn');
            $table->dropColumn('bank_account_holder_name');
            $table->dropColumn('bank_account_number');
            $table->dropColumn('currency');
        });

        Schema::table('user_withdrawals', function (Blueprint $table) {
            $table->string('to_crypto_symbol', 42)->after('conversion_rate')->index();
            $table->string('to_crypto_address', 255)->after('to_crypto_symbol');
            $table->decimal('to_crypto_amount', 36, 18)->after('conversion_rate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_withdrawals', function (Blueprint $table) {
            $table->dropColumn('to_crypto_symbol');
            $table->dropColumn('to_crypto_address');
            $table->dropColumn('to_crypto_amount');
        });

        Schema::table('user_withdrawals', function (Blueprint $table) {
            $table->unsignedBigInteger('bank_id')->nullable()->default(null)->after('user_id');

            $table->string('name_en', 255)->nullable()->default(null)->after('country_id');
            $table->string('name_cn', 255)->nullable()->default(null)->after('name_en');
            $table->string('bank_account_holder_name', 255)->nullable()->default(null)->after('bank_account_holder_name');
            $table->string('bank_account_number', 255)->nullable()->default(null)->after('bank_account_number');

            $table->decimal('currency', 18, \App\Config::DECIMAL_POINT)->default(0)->after('conversion_rate');
        });
        Schema::table('user_withdrawals', function (Blueprint $table) {
            $table->foreign('bank_id')->references('id')->on('banks')->onDelete('set null');
        });
    }
};
