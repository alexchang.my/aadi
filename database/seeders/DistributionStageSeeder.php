<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DistributionStageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getStages() as $s) {
            $record = \App\Models\DistributionStage::where('plan_type', '=', $s['plan_type'])->first();
            if (! $record) {
                $record = new \App\Models\DistributionStage();
                $record->plan_type = $s['plan_type'];
                $record->stage = 0;
                $record->save();
            }
        }
    }

    public function getStages()
    {
        $arr = [
            ['plan_type' => 'assured', 'stage' => 0],
            ['plan_type' => 'structured', 'stage' => 0],
        ];

        return $arr;
    }
}
