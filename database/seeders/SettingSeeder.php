<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getSettings() as $var) {
            $count = Setting::where('setting', '=', $var['setting'])->count();
            if (! $count) {
                $c = new Setting();

                foreach ($var as $k => $v) {
                    $c->$k = $v;
                }

                $c->save();
            }
        }
    }

    public function getSettings()
    {
        $arr = [];

        $arr[] = ['setting' => 'company_usdt_address', 'setting_name' => 'Company USDT address', 'setting_value' => '1234', 'setting_type' => 'text'];
        $arr[] = ['setting' => 'company_trc20_usdt_address', 'setting_name' => 'Company TRC20 USDT address', 'setting_value' => 'TEr6x8Vg4AXyXtXYjF9q2f8fT76xjjwmxg', 'setting_type' => 'text'];
        $arr[] = ['setting' => 'company_bep20_usdt_address', 'setting_name' => 'Company BEP20 USDT address', 'setting_value' => '0xea3ad06cb9b9aca2feed3a629d420561187cf7aa', 'setting_type' => 'text'];

        return $arr;
    }
}
