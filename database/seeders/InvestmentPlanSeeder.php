<?php

namespace Database\Seeders;

use App\Models\InvestmentPlan;
use Illuminate\Database\Seeder;

class InvestmentPlanSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getPlans() as $plan) {
            $p = InvestmentPlan::where('code', '=', $plan['code'])->first();
            if (! $p) {
                $p = new InvestmentPlan();
                $p->code = $plan['code'];
                $p->plan_name = $plan['plan_name'];
                $p->featured_photo = $plan['featured_photo'];
                $p->category_icon = $plan['category_icon'];
                $p->return_percentage_from = $plan['return_percentage_from'];
                $p->return_percentage_to = $plan['return_percentage_to'];
                $p->return_cycle = $plan['return_cycle'];
                $p->fund_size = $plan['fund_size'];
                $p->fund_description = $plan['fund_description'];
                $p->setting = $plan['setting'];
                $p->save();
            }
        }
    }

    protected function getPlans()
    {
        return [
            'AACF' => [
                'code' => 'AACF',
                'plan_name' => 'Aadi Assured Conservative Fund',
                'featured_photo' => 'cuploads/investment_plan/featured/AACF.jpg',
                'category_icon' => 'cuploads/investment_plan/category/AACF.png',
                'return_percentage_from' => 6,
                'return_percentage_to' => 9.96,
                'return_cycle' => 'in 3 months',
                'fund_size' => '1,000,000',
                'fund_description' => '',
                'setting' => [
                    'plan_type' => 'assured',
                    'risk_type' => 'low',
                    'min_return_percentage_per_cycle' => 0.5,
                    'max_return_percentage_per_cycle' => 0.83,
                    'capital_lock_days' => 45,
                    'distribution_cycle' => 'weekly',
                    'initial_investment_amount' => 1000,
                    'subsequence_investment_min_amount' => 100,
                    'investment_multiplication_of_amount' => 100,
                ],
            ],
            'AAGF' => [
                'code' => 'AAGF',
                'plan_name' => 'Aadi Assured Growth Fund',
                'featured_photo' => 'cuploads/investment_plan/featured/AAGF.jpg',
                'category_icon' => 'cuploads/investment_plan/category/AAGF.png',
                'return_percentage_from' => 48,
                'return_percentage_to' => 84,
                'return_cycle' => 'in 6 months',
                'fund_size' => '10,000,000',
                'fund_description' => '',
                'setting' => [
                    'plan_type' => 'assured',
                    'risk_type' => 'low',
                    'min_return_percentage_per_cycle' => 2,
                    'max_return_percentage_per_cycle' => 3.5,
                    'capital_lock_days' => 180,
                    'distribution_cycle' => 'weekly',
                    'initial_investment_amount' => 3000,
                    'subsequence_investment_min_amount' => 100,
                    'investment_multiplication_of_amount' => 100,
                ],
            ],
            'AASF' => [
                'code' => 'AASF',
                'plan_name' => 'Aadi Assured Saving Fund',
                'featured_photo' => 'cuploads/investment_plan/featured/AASF.jpg',
                'category_icon' => 'cuploads/investment_plan/category/AASF.png',
                'return_percentage_from' => 12,
                'return_percentage_to' => 60,
                'return_cycle' => 'in 3 months',
                'fund_size' => '15,000,000',
                'fund_description' => '',
                'setting' => [
                    'plan_type' => 'assured',
                    'risk_type' => 'medium',
                    'min_return_percentage_per_cycle' => 1,
                    'max_return_percentage_per_cycle' => 5,
                    'capital_lock_days' => 90,
                    'distribution_cycle' => 'weekly',
                    'initial_investment_amount' => 2000,
                    'subsequence_investment_min_amount' => 100,
                    'investment_multiplication_of_amount' => 100,
                ],
            ],
            'ASGF' => [
                'code' => 'ASGF',
                'plan_name' => 'Aadi Structured Growth Fund',
                'featured_photo' => 'cuploads/investment_plan/featured/ASGF.jpg',
                'category_icon' => 'cuploads/investment_plan/category/ASGF.png',
                'return_percentage_from' => -30,
                'return_percentage_to' => 120,
                'return_cycle' => 'in 3 months',
                'fund_size' => '30,000,000',
                'fund_description' => '',
                'setting' => [
                    'plan_type' => 'structured',
                    'risk_type' => 'high',
                    'min_return_percentage_per_cycle' => -10,
                    'max_return_percentage_per_cycle' => 40,
                    'capital_lock_days' => 90,
                    'distribution_cycle' => 'manual',
                    'initial_investment_amount' => 1000,
                    'subsequence_investment_min_amount' => 100,
                    'investment_multiplication_of_amount' => 100,
                ],
            ],
        ];
    }
}
