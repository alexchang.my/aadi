<?php

namespace Database\Seeders;

use App\Models\TimeTable;
use Illuminate\Database\Seeder;

class TimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getTimeTable() as $var) {
            $record = TimeTable::where('utc_date', '=', $var['utc_date'])
                ->first();
            if (! $record) {
                $record = new TimeTable();
                foreach ($var as $column => $value) {
                    $record->{$column} = $value;
                }
                $record->save();
            }
        }
    }

    public function getTimeTable()
    {
        $arr = [];

        for ($i = 0; $i < 1461; $i++) {
            if ($i == 0) {
                $now = $this->getStartDate();
            } else {
                $now = $this->getStartDate()->addDays($i);
            }

            $arr[] = [
                'utc_date' => $now->copy()->format('Y-m-d'),
                'utc_day' => $now->dayName,
                'assured_roi_day' => $now->isMonday(),
            ];
        }

        \Log::info($arr);

        return $arr;
    }

    public function getStartDate()
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', '2022-12-01 00:00:00', 'utc');
    }
}
