<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (config('env.APP_DEBUG') !== true) {
            throw new \Exception(trans('common.permission_denied'));
        }

        // \App\Models\User::factory(10)->create();
        $admin = new AdminSeeder();
        $country = new CountrySeeder();
        $location = new CountryLocationSeeder();
        $bank = new BankSeeder();
        $user = new UserSeeder();
        $setting = new SettingSeeder();
        $plan = new InvestmentPlanSeeder();
        $acc = new AccountSeeder();
        $stage = new DistributionStageSeeder();
        $time = new TimeTableSeeder();

        $admin->run();
        $country->run();
        $location->run();
        $bank->run();
        $user->run();
        $setting->run();
        $plan->run();
        $acc->run();
        $stage->run();
        $time->run();
    }
}
