<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->accountLists() as $acc) {
            $a = \App\Models\Account::where('account_code', '=', $acc['account_code'])
                ->first();

            if (! $a) {
                $a = new \App\Models\Account();
                foreach ($acc as $f => $v) {
                    $a->$f = $v;
                }
                $a->save();
            }
        }
    }

    protected function accountLists()
    {
        return [
            ['account_code' => 'DEPOSIT', 'account_name' => 'Deposit Account'],
            ['account_code' => 'ASSURED', 'account_name' => 'Assured Account'],
            ['account_code' => 'STRUCTURED', 'account_name' => 'Structured Account'],
            ['account_code' => 'OPEX', 'account_name' => 'OPEX Account'],
            ['account_code' => 'PL', 'account_name' => 'Profit Sharing Account'],
            ['account_code' => 'RESERVED', 'account_name' => 'Reserved Account'],
            ['account_code' => 'FEES_CHARGES', 'account_name' => 'Fees Charges Account'],
        ];
    }
}
