<?php

return [

    'APP_ENV' => env('APP_ENV', null),
    'APP_NAME' => env('APP_NAME', null),
    'APP_DEBUG' => env('APP_DEBUG', null),
    'APP_URL' => env('APP_URL', null),

];
