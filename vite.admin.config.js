import {defineConfig} from "vite";
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import {config as DotEnvConfig} from 'dotenv'
import * as fs from "fs"
import eslintPlugin from 'vite-plugin-eslint'
DotEnvConfig()

const role = 'admin';
const port = 3031;
const homeDir = process.env.HOME;
const host = process.env.APP_URL.split('//')[1];
let KEY_PATH;
if (process.env.APP_ENV === 'local') {
    if (!homeDir.startsWith('/Users/')) {//NOT MACOS, MAYBE NEED IMPROVE HERE
        KEY_PATH = {
            cert: fs.readFileSync(resolve(homeDir, `.config/valet/Certificates/${host}.pem`)),
            ca: fs.readFileSync(resolve(homeDir, 'AppData/Local/mkcert/rootCA.pem'))
        }
    } else {
        KEY_PATH = {
            key: fs.readFileSync(resolve(homeDir, `.config/valet/Certificates/${host}.key`)),
            cert: fs.readFileSync(resolve(homeDir, `.config/valet/Certificates/${host}.crt`)),
        }
    }
}

/** @type {import('vite').UserConfig} */
export default defineConfig(({ command, mode }) => {
    return {
        define: {
            __VUE_I18N_FULL_INSTALL__: true,
            __VUE_I18N_LEGACY_API__: false,
            __INTLIFY_PROD_DEVTOOLS__: false,
        },
        plugins: [
            vue(),
            eslintPlugin()
        ],
        server: {
            port,
            host,
            https: KEY_PATH,
            strictPort: true,
        },
        resolve: {
            alias: {
                '!': resolve(__dirname, './resources/scripts'),
                '~': resolve(__dirname, `./resources/scripts/${role}`),
            }
        },
        base: command === 'serve' ? '' : `/build/${role}/`,
        publicDir: 'fake_dir_so_nothing_gets_copied',
        build: {
            manifest: true,
            outDir: `public/build/${role}/`,
            emptyOutDir: true,
            rollupOptions: {
                input: `resources/scripts/${role}/app.js`,
                external: [
                    /img\/.*/
                ]
            },
            chunkSizeWarningLimit: 1600,
        },
        optimizeDeps: {
            include: [
                'vue',
                'vue-i18n',
                'pinia',
                'moment',
                'axios'
            ]
        }
    };
});
