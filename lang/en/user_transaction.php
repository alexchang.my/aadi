<?php

return [

    'topup' => 'Topup',

    'transfer_credit_out' => 'Transfer to :to_username',
    'transfer_credit_in' => 'Transfer from :from_username',

    'withdraw' => 'Withdraw',
    'withdraw_refund' => 'Withdraw refund',

    'roi_withdraw' => 'ROI withdraw (:contract_id)',
    'early_roi_withdraw_fees' => 'ROI withdraw fees (:contract_id)',
    'roi_negative' => 'ROI negative payback (:contract_id)',

    'capital_withdraw' => 'Capital withdraw (:contract_id)',
    'early_capital_withdraw' => 'Early capital withdraw (:contract_id)',
    'early_capital_withdraw_fees' => 'Early capital withdraw fees (:contract_id)',

    'plan_buy_in' => 'Buy in :code',

    'roi_direct_sponsor_bonus' => 'DSB :username (:member_id)',

    'roi_bmb' => 'BMB :username (:member_id)',

    'roi_bmi' => 'BMI :username (:member_id)',

    'admin_credit' => 'Admin credit',
    'admin_debit' => 'Admin debit',
];
