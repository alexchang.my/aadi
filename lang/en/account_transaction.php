<?php

return [

    'transfer_to_other_account_out' => 'Transfer to: :to_account',
    'transfer_to_other_account_in' => 'Transfer from: :from_account',

    'transfer_to_other_account_reject_out' => 'Transfer rejected, return to :return_to_account',
    'transfer_to_other_account_reject_in' => 'Transfer rejected, return from :return_from_account',

    'credit' => 'Fund in',
    'debit' => 'Fund out',

    'user_topup' => ':username Top up',
    'user_withdraw' => ':username Withdraw',

    'assured_earning_profit' => 'Profit (Assured)',
    'assured_earning_reserved' => 'Reserved (Assured)',

    'structured_earning_profit' => 'Profit (Structured)',
    'structured_earning_reserved' => 'Reserved (Structured)',

];
