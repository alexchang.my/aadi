<?php

return [

    'transfer_to_other_account_out' => '转账至：:to_account',
    'transfer_to_other_account_in' => '转账来自：:from_account',

    'transfer_to_other_account_reject_out' => 'Transfer rejected, return to :return_to_account',
    'transfer_to_other_account_reject_in' => 'Transfer rejected, return from :return_from_account',

    'credit' => 'Fund in',
    'debit' => 'Fund out',

    'user_topup' => '用户 :username 充值',
    'user_withdraw' => '用户 :username 提现',

    'assured_earning_profit' => '收入（Assured）',
    'assured_earning_reserved' => '储备（Assured）',

    'structured_earning_profit' => '收入（Structured）',
    'structured_earning_reserved' => '储备（Structured）',

];
