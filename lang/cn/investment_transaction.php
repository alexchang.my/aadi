<?php

return [

    'fund_approved' => 'Invest: :username (:member_id)',
    'capital_early_withdrawal' => 'Early withdrawal: :username (:member_id)',
    'fund_matured' => 'Matured: :username (:member_id)',
    'early_capital_withdraw_roi_burn' => 'Early withdrawal ROI forfeited (:contract_id)',

];
