<?php

use App\Managers\Calculator;
use App\Models\DistributionSetting;
use App\Models\User;
use App\Models\Account;
use App\Models\UserInvestment;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test', function () {
    $plan_type = 'structured';
    $time = \App\Models\TimeTable::where('utc_date', '=', '2022-12-16')->first();
    $setting = DistributionSetting::where('plan_type', '=', $plan_type)->Pending()->Latest()->first();

    $records = UserInvestment::with(['investmentPlan'])
        ->Active()
        ->where('plan_type', '=', $plan_type)
        ->where(function ($q) use ($time) {
            $q->orWhereNull('last_distribute_roi_at')
                ->orWhereDate('last_distribute_roi_at', '<', $time->getTimeFrom()->copy()->format('Y-m-d'));
        })
        ->where(function ($q) use ($time) {
            $q->whereNotNull('fund_approve_at')
                ->whereDate('fund_approve_at', '<', $time->getTimeFrom()->copy()->format('Y-m-d'));
        })
        ->where('opt_in', '=', 1)
        ->whereNotNull('opt_in_at')
        ->orderBy('id', 'ASC');
    if ($setting) {
        $records = $records->where('opt_in_at', '<=', $setting->created_at);
    }

    foreach ($records->get() as $ui) {
        if ($setting && isset($setting->adjusted_percentage[$ui->code])) {
            $roi_percentage = $setting->adjusted_percentage[$ui->code];
        } else {
            $roi_percentage = 0;
        }

        if ($roi_percentage == 0) {
            die('0 percent');
        } else if ($roi_percentage > 0) {
            die('>0 percent');
        } else if ($roi_percentage < 0) {
            $loss = (new Calculator($ui->current_balance))->percentage(abs($roi_percentage))->getNumber();

        }
        dd($roi_percentage);
    }
});

Route::any('{path}', [\App\Http\Controllers\AppController::class, 'getApp'])
    ->where('path', '^(?!(api|build|img|test)).*$');
