<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/settings', [\App\Http\Controllers\Api\Admin\SettingController::class, 'fetch']);

Route::post('/logout', [\App\Http\Controllers\Api\Admin\AccountController::class, 'logout']);
Route::post('/login', [\App\Http\Controllers\Api\Admin\AccountController::class, 'login']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('/dashboard', [\App\Http\Controllers\Api\Admin\DashboardController::class, 'index'])->middleware('adminHasPermission:dashboard_statistics');

    Route::group(['prefix' => 'account', 'namespace' => 'Account'], function () {
        Route::post('/fetch_balance', [\App\Http\Controllers\Api\Admin\Account\AccountingController::class, 'fetchBalance']);
        Route::post('/submit_adjust', [\App\Http\Controllers\Api\Admin\Account\AccountingController::class, 'submitAdjust']);
        Route::post('/verify_transaction', [\App\Http\Controllers\Api\Admin\Account\AccountingController::class, 'verifyTransaction']);

        Route::post('/trader_declare/build_form', [\App\Http\Controllers\Api\Admin\Account\TraderDeclareController::class, 'buildForm']);
        Route::post('/trader_declare/save_trader_declare', [\App\Http\Controllers\Api\Admin\Account\TraderDeclareController::class, 'saveTraderDeclare']);
        Route::post('/trader_declare/save_roi_adjustment', [\App\Http\Controllers\Api\Admin\Account\TraderDeclareController::class, 'saveRoiAdjustment']);
        Route::post('/trader_declare/toggle_approve_declare', [\App\Http\Controllers\Api\Admin\Account\TraderDeclareController::class, 'toggleApproveDeclare']);
    });

    Route::group(['prefix' => 'user', 'namespace' => 'User'], function () {
        Route::post('/user/build_form', [\App\Http\Controllers\Api\Admin\User\UserController::class, 'buildForm'])->middleware('adminHasPermission:manage_user');
        Route::post('/user/submit_form', [\App\Http\Controllers\Api\Admin\User\UserController::class, 'submitForm'])->middleware('adminHasPermission:manage_user');
        Route::post('/user/login_account', [\App\Http\Controllers\Api\Admin\User\UserController::class, 'loginAccount'])->middleware('adminHasPermission:manage_user');
        Route::post('/user/toggle_ban_status', [\App\Http\Controllers\Api\Admin\User\UserController::class, 'toggleBanStatus'])->middleware('adminHasPermission:manage_user');

        Route::post('/topup/build_form', [\App\Http\Controllers\Api\Admin\User\TopupController::class, 'buildForm'])->middleware('adminHasPermission:manage_topup');
        Route::post('/topup/submit_form', [\App\Http\Controllers\Api\Admin\User\TopupController::class, 'submitForm'])->middleware('adminHasPermission:manage_topup');

        Route::post('/withdrawal/build_form', [\App\Http\Controllers\Api\Admin\User\WithdrawalController::class, 'buildForm'])->middleware('adminHasPermission:manage_withdrawal');
        Route::post('/withdrawal/submit_form', [\App\Http\Controllers\Api\Admin\User\WithdrawalController::class, 'submitForm'])->middleware('adminHasPermission:manage_withdrawal');
    });

    Route::post('/article/build_form', [\App\Http\Controllers\Api\Admin\ArticleController::class, 'buildForm'])->middleware('adminHasPermission:manage_article');
    Route::post('/article/submit_form', [\App\Http\Controllers\Api\Admin\ArticleController::class, 'submitForm'])->middleware('adminHasPermission:manage_article');
    Route::post('/article/delete/{id}', [\App\Http\Controllers\Api\Admin\ArticleController::class, 'deleteModel'])->middleware('adminHasPermission:manage_article');
    Route::post('/article/upload_image_by_file', [\App\Http\Controllers\Api\Admin\ArticleController::class, 'uploadImageByFile'])->middleware('adminHasPermission:manage_article');

    Route::group(['prefix' => 'investment_plan', 'namespace' => 'InvestPlan'], function () {
        Route::post('/investment_plan/build_form', [\App\Http\Controllers\Api\Admin\InvestmentPlan\InvestmentPlanController::class, 'buildForm'])->middleware('adminHasPermission:manage_investment_plan');
        Route::post('/investment_plan/submit_form', [\App\Http\Controllers\Api\Admin\InvestmentPlan\InvestmentPlanController::class, 'submitForm'])->middleware('adminHasPermission:manage_investment_plan');
        Route::post('/investment_plan/upload_image_by_file', [\App\Http\Controllers\Api\Admin\InvestmentPlan\InvestmentPlanController::class, 'uploadImageByFile'])->middleware('adminHasPermission:manage_investment_plan');

        Route::post('/transaction/load_statistics', [\App\Http\Controllers\Api\Admin\InvestmentPlan\TransactionController::class, 'loadStatistics'])->middleware('adminHasPermission:manage_investment_plan');
    });

    Route::group(['prefix' => 'other', 'namespace' => 'Other'], function () {
        Route::post('/bank/build_form', [\App\Http\Controllers\Api\Admin\Other\BankController::class, 'buildForm'])->middleware('adminHasPermission:manage_bank');
        Route::post('/bank/submit_form', [\App\Http\Controllers\Api\Admin\Other\BankController::class, 'submitForm'])->middleware('adminHasPermission:manage_bank');
        Route::post('/bank/delete', [\App\Http\Controllers\Api\Admin\Other\BankController::class, 'delete'])->middleware('adminHasPermission:manage_bank');

        Route::post('/setting/build_form', [\App\Http\Controllers\Api\Admin\Other\SettingController::class, 'buildForm'])->middleware('adminHasPermission:manage_setting');
        Route::post('/setting/submit_form', [\App\Http\Controllers\Api\Admin\Other\SettingController::class, 'submitForm'])->middleware('adminHasPermission:manage_setting');
        Route::post('/setting/upload_image_by_file', [\App\Http\Controllers\Api\Admin\Other\SettingController::class, 'uploadImageByFile'])->middleware('adminHasPermission:manage_setting');

        Route::post('/company_bank/build_form', [\App\Http\Controllers\Api\Admin\Other\CompanyBankController::class, 'buildForm'])->middleware('adminHasPermission:manage_company_bank');
        Route::post('/company_bank/submit_form', [\App\Http\Controllers\Api\Admin\Other\CompanyBankController::class, 'submitForm'])->middleware('adminHasPermission:manage_company_bank');
        Route::post('/company_bank/delete', [\App\Http\Controllers\Api\Admin\Other\CompanyBankController::class, 'delete'])->middleware('adminHasPermission:manage_company_bank');

        Route::post('/country/build_form', [\App\Http\Controllers\Api\Admin\Other\CountryController::class, 'buildForm'])->middleware('adminHasPermission:manage_country');
        Route::post('/country/submit_form', [\App\Http\Controllers\Api\Admin\Other\CountryController::class, 'submitForm'])->middleware('adminHasPermission:manage_country');
    });

    Route::group(['prefix' => 'management', 'namespace' => 'Management'], function () {
        Route::post('/admin/build_form', [\App\Http\Controllers\Api\Admin\Management\AdminController::class, 'buildForm'])->middleware('adminHasPermission:manage_admin');
        Route::post('/admin/submit_form', [\App\Http\Controllers\Api\Admin\Management\AdminController::class, 'submitForm'])->middleware('adminHasPermission:manage_admin');
        Route::post('/admin/delete', [\App\Http\Controllers\Api\Admin\Management\AdminController::class, 'delete'])->middleware('adminHasPermission:manage_admin');

        Route::post('/admin_group/build_form', [\App\Http\Controllers\Api\Admin\Management\AdminGroupController::class, 'buildForm'])->middleware('adminHasPermission:manage_admin_group');
        Route::post('/admin_group/submit_form', [\App\Http\Controllers\Api\Admin\Management\AdminGroupController::class, 'submitForm'])->middleware('adminHasPermission:manage_admin_group');
        Route::post('/admin_group/delete', [\App\Http\Controllers\Api\Admin\Management\AdminGroupController::class, 'delete'])->middleware('adminHasPermission:manage_admin_group');
    });

    Route::post('/dt', [\App\Http\Controllers\Api\Admin\DataTableController::class, 'load']);

    Route::post('/me', [\App\Http\Controllers\Api\Admin\AccountController::class, 'fetch']);
    Route::post('/notifications', [\App\Http\Controllers\Api\Admin\NotificationController::class, 'fetch']);
    Route::post('/change_language', [\App\Http\Controllers\Api\Admin\AccountController::class, 'changeLanguage']);
    Route::post('/profile_submit', [\App\Http\Controllers\Api\Admin\AccountController::class, 'profileSubmit']);
});
