<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/home', [\App\Http\Controllers\Api\User\HomeController::class, 'index']);
Route::post('/settings', [\App\Http\Controllers\Api\User\SettingController::class, 'fetch']);
Route::post('/article/fetch', [\App\Http\Controllers\Api\User\ArticleController::class, 'fetch']);

Route::post('/reset_password', [\App\Http\Controllers\Api\User\AccountController::class, 'resetPassword']);
Route::post('/forget_password', [\App\Http\Controllers\Api\User\AccountController::class, 'forgetPassword']);
Route::post('/logout', [\App\Http\Controllers\Api\User\AccountController::class, 'logout']);
Route::post('/login', [\App\Http\Controllers\Api\User\AccountController::class, 'login']);
Route::post('/register/build_form', [\App\Http\Controllers\Api\User\AccountController::class, 'registerBuildForm']);
Route::post('/register', [\App\Http\Controllers\Api\User\AccountController::class, 'register']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('/profile_submit', [\App\Http\Controllers\Api\User\AccountController::class, 'profileSubmit']);
    Route::post('/security_submit', [\App\Http\Controllers\Api\User\AccountController::class, 'securitySubmit']);

    Route::post('/team/fetch_data', [\App\Http\Controllers\Api\User\TeamController::class, 'fetchData']);
    Route::post('/team/hierarchy', [\App\Http\Controllers\Api\User\TeamController::class, 'hierarchy']);

    Route::post('/plan_invest/build_form', [\App\Http\Controllers\Api\User\PlanInvestController::class, 'buildForm']);
    Route::post('/plan_invest/submit_form', [\App\Http\Controllers\Api\User\PlanInvestController::class, 'submitForm'])->block();

    Route::post('/portfolio/fetch', [\App\Http\Controllers\Api\User\PortfolioController::class, 'fetch']);
    Route::post('/portfolio/fund/{code}', [\App\Http\Controllers\Api\User\PortfolioController::class, 'fund']);
    Route::post('/portfolio/contract/{contract_id}', [\App\Http\Controllers\Api\User\PortfolioController::class, 'contract']);
    Route::post('/portfolio/withdraw_roi/{contract_id}', [\App\Http\Controllers\Api\User\PortfolioController::class, 'withdrawRoi'])->block();
    Route::post('/portfolio/withdraw_capital/{contract_id}', [\App\Http\Controllers\Api\User\PortfolioController::class, 'withdrawCapital'])->block();
    Route::post('/portfolio/opt_out/{contract_id}', [\App\Http\Controllers\Api\User\PortfolioController::class, 'optOut'])->block();
    Route::post('/portfolio/opt_in/{contract_id}', [\App\Http\Controllers\Api\User\PortfolioController::class, 'optIn'])->block();
    Route::post('/portfolio/fetch/existing', [\App\Http\Controllers\Api\User\PortfolioController::class, 'fetchExisting'])->block();

    Route::post('/credit/c1/transfer', [\App\Http\Controllers\Api\User\CreditController::class, 'transferC1'])->block();
    Route::post('/credit/c1/topup/load_history', [\App\Http\Controllers\Api\User\CreditController::class, 'topupC1LoadHistory'])->block();
    Route::post('/credit/c1/topup/submit_form', [\App\Http\Controllers\Api\User\CreditController::class, 'topupC1SubmitForm'])->block();
    Route::post('/credit/c2/transfer', [\App\Http\Controllers\Api\User\CreditController::class, 'transferC2'])->block();
    Route::post('/credit/c2/withdrawal/build_form', [\App\Http\Controllers\Api\User\CreditController::class, 'withdrawalC2buildForm'])->block();
    Route::post('/credit/c2/withdrawal/load_history', [\App\Http\Controllers\Api\User\CreditController::class, 'withdrawalC2LoadHistory'])->block();
    Route::post('/credit/c2/withdrawal/submit_form', [\App\Http\Controllers\Api\User\CreditController::class, 'withdrawalC2SubmitForm'])->block();

    Route::post('/check_account_name', [\App\Http\Controllers\Api\User\UserController::class, 'checkAccountName'])->block();

    Route::post('/dt', [\App\Http\Controllers\Api\User\DataTableController::class, 'load']);

    Route::post('/me', [\App\Http\Controllers\Api\User\AccountController::class, 'fetch']);
    Route::post('/change_language', [\App\Http\Controllers\Api\User\AccountController::class, 'changeLanguage']);
});
