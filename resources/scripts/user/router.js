import { createWebHistory, createRouter } from "vue-router";
import Router from './components/blank-router.vue';
import GlobalLayout from './layouts/global-layout.vue';
import MainLayout from './layouts/main-layout.vue';
import {useSettingStore} from "~/stores/setting.js";
import {useUserStore} from "~/stores/user.js";
import EventBus from "!/event-bus.js";

import AuthRegister from './pages/auth/register-page.vue';
import AuthLogin from './pages/auth/login-page.vue';
import AuthForgetPassword from './pages/auth/forget-password-page.vue';
import AuthResetPassword from './pages/auth/reset-password-page.vue';
import Home from './pages/main/home/home-page.vue';
import ArticleDetails from './pages/main/home/article-details.vue';
import ExploreHome from './pages/main/explore/home-page.vue';
import ExplorePlanDetails from './pages/main/explore/plan-details-page.vue';
import ExplorePlanInvest from './pages/main/explore/plan-invest-page.vue';
import PortfolioHome from './pages/main/portfolio/home-page.vue';
import PortfolioFundHome from './pages/main/portfolio/fund/home-page.vue';
import PortfolioFundContract from './pages/main/portfolio/fund/contract-page.vue';
import PortfolioFundExistingContract from './pages/main/portfolio/fund/existing-contract-page.vue';
import TeamHome from './pages/main/team/home-page.vue';
import TeamCalculatorHome from './pages/main/team/calculator/home-page.vue';
import TeamHierarchyHome from './pages/main/team/hierarchy/home-page.vue';
import MeHome from './pages/main/me/home-page.vue';
import MeProfile from './pages/main/me/profile-page.vue';
import MeSecurity from './pages/main/me/security-page.vue';
import MeC1Statement from './pages/main/me/c1/statement-page.vue';
import MeC1Transfer from './pages/main/me/c1/transfer-page.vue';
import MeC2Statement from './pages/main/me/c2/statement-page.vue';
import MeC2Transfer from './pages/main/me/c2/transfer-page.vue';
import MeC1Topup from './pages/main/me/c1/topup-page.vue';
import MeC2Withdrawal from './pages/main/me/c2/withdrawal-page.vue';

const routes = [
    {
        path: '/',
        component: GlobalLayout,
        redirect: {name: 'user.home'},
        children: [
            {
                path: '',
                component: MainLayout,
                meta: { auth: true },
                children: [
                    {
                        path: '',
                        redirect: {name: 'user.home'},
                        component: Router,
                        children: [
                            {
                                path: 'home',
                                name: 'user.home',
                                component: Home,
                            },
                            {
                                path: 'article/:id',
                                name: 'user.home.article',
                                component: ArticleDetails
                            }
                        ]
                    },
                    {
                        path: 'explore',
                        redirect: {name: 'user.explore.home'},
                        component: Router,
                        children: [
                            {
                                path: 'home',
                                name: 'user.explore.home',
                                component: ExploreHome,
                            },
                            {
                                path: 'plan_details/:code',
                                name: 'user.explore.plan_details',
                                component: ExplorePlanDetails,
                                meta: {
                                    hideFooter: true,
                                }
                            },
                            {
                                path: 'plan_invest/:code',
                                name: 'user.explore.plan_invest',
                                component: ExplorePlanInvest,
                                meta: {
                                    dark: true,
                                    hideFooter: true,
                                }
                            },
                        ]
                    },
                    {
                        path: 'portfolio',
                        redirect: {name: 'user.portfolio.home'},
                        component: Router,
                        children: [
                            {
                                path: 'home',
                                name: 'user.portfolio.home',
                                component: PortfolioHome,
                            },
                            {
                                path: 'fund/home/:code',
                                name: 'user.portfolio.fund.home',
                                component: PortfolioFundHome,
                            },
                            {
                                path: 'fund/contract/:contract_id',
                                name: 'user.portfolio.fund.contract',
                                component: PortfolioFundContract,
                            },
                            {
                                path: 'fund/existing_contract/:code',
                                name: 'user.portfolio.fund.existing_contract',
                                component: PortfolioFundExistingContract,
                            },
                        ]
                    },
                    {
                        path: 'team',
                        redirect: {name: 'user.team.home'},
                        component: Router,
                        children: [
                            {
                                path: 'home',
                                name: 'user.team.home',
                                component: TeamHome,
                            },
                            {
                                path: 'calculator/home',
                                name: 'user.team.calculator.home',
                                component: TeamCalculatorHome,
                                meta: {
                                    hideFooter: true,
                                    back: true,
                                    title: 'marketing_calculator'
                                }
                            },
                            {
                                path: 'hierarchy/home',
                                name: 'user.team.hierarchy.home',
                                component: TeamHierarchyHome,
                                meta: {
                                    hideFooter: true,
                                    dark: true,
                                    back: true,
                                    title: 'hierarchy'
                                }
                            }
                        ]
                    },
                    {
                        path: 'me',
                        redirect: {name: 'user.me.home'},
                        component: Router,
                        children: [
                            {
                                path: 'home',
                                name: 'user.me.home',
                                component: MeHome,
                            },
                            {
                                path: 'profile',
                                name: 'user.me.profile',
                                component: MeProfile,
                                meta: {
                                    dark: true,
                                    hideFooter: true,
                                    back: true,
                                    title: 'my_profile'
                                }
                            },
                            {
                                path: 'security',
                                name: 'user.me.security',
                                component: MeSecurity,
                                meta: {
                                    dark: true,
                                    hideFooter: true,
                                    back: true,
                                    title: 'account_security'
                                }
                            },
                            {
                                path: 'c1',
                                component: Router,
                                redirect: {name: 'user.c1.statement'},
                                children: [
                                    {
                                        path: 'statement',
                                        name: 'user.c1.statement',
                                        component: MeC1Statement,
                                        meta: {
                                            dark: true,
                                            hideFooter: true,
                                            back: true,
                                            title: 'credit_1_short',
                                        }
                                    },
                                    {
                                        path: 'transfer',
                                        name: 'user.c1.transfer',
                                        component: MeC1Transfer,
                                        meta: {
                                            dark: true,
                                            hideFooter: true,
                                            back: true,
                                            title: 'credit_1_short',
                                        }
                                    },
                                    {
                                        path: 'topup',
                                        name: 'user.c1.topup',
                                        component: MeC1Topup,
                                        meta: {
                                            dark: true,
                                            hideFooter: true,
                                            back: true,
                                            title: 'credit_1_short',
                                        }
                                    },
                                ]
                            },
                            {
                                path: 'c2',
                                component: Router,
                                redirect: {name: 'user.c2.statement'},
                                children: [
                                    {
                                        path: 'statement',
                                        name: 'user.c2.statement',
                                        component: MeC2Statement,
                                        meta: {
                                            dark: true,
                                            hideFooter: true,
                                            back: true,
                                            title: 'credit_2',
                                        }
                                    },
                                    {
                                        path: 'transfer',
                                        name: 'user.c2.transfer',
                                        component: MeC2Transfer,
                                        meta: {
                                            dark: true,
                                            hideFooter: true,
                                            back: true,
                                            title: 'credit_2',
                                        }
                                    },
                                    {
                                        path: 'withdrawal',
                                        name: 'user.c2.withdrawal',
                                        component: MeC2Withdrawal,
                                        meta: {
                                            dark: true,
                                            hideFooter: true,
                                            back: true,
                                            title: 'credit_2',
                                        }
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                path: '',
                component: Router,
                meta: { auth: false },
                children: [
                    {
                        path: 'register',
                        name: 'user.auth.register',
                        component: AuthRegister,
                        meta: {
                            auth: false,
                        }
                    },
                    {
                        path: 'login',
                        name: 'user.auth.login',
                        component: AuthLogin,
                        meta: {
                            auth: false,
                        }
                    },
                    {
                        path: 'forget_password',
                        name: 'user.auth.forget_password',
                        component: AuthForgetPassword,
                        meta: {
                            auth: false,
                        }
                    },
                    {
                        path: 'reset_password/:token',
                        name: 'user.auth.reset_password',
                        component: AuthResetPassword,
                        meta: {
                            auth: false,
                        }
                    }
                ]
            },

        ]
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes: routes,
    async scrollBehavior (to, from, savedPosition) {
        if (to.hash) {
            return {
                selector: to.hash,
                behavior: 'smooth',
            };
        } else {
            if (savedPosition) {
                return savedPosition;
            } else {
                return new Promise((resolve) => {
                    window.scrollTo(0, 0);
                    resolve();
                });
            }
        }
    }
});

router.beforeEach(async (to, from, next) => {
    const $settingStore = useSettingStore();
    const $userStore = useUserStore();

    await $settingStore.fetchSettings();

    if ($userStore.token) {
        await $userStore.fetchAccount();
    }

    //NOT LOGGED IN WHILE THE ROUTE GROUP NEED AUTH
    if(!$userStore.account) {
        let needAuth = [...to.matched].reverse().find(r => r.meta.auth);
        if (needAuth) {
            return next({ name: 'user.auth.login' });
        }
    } else {
        let guestOnly = [...to.matched].reverse().find(r => r.meta.auth === false);
        if (guestOnly) {
            return next({ name: 'user.home' });
        }
    }

    if (to.meta.dark) {
        EventBus.emit('BACKGROUND_DARK');
    } else {
        EventBus.emit('BACKGROUND_LIGHT');
    }

    return next();
});
export default router;
