import { createI18n } from 'vue-i18n';
import { CONFIG } from './config.js';
import Cookies from 'js-cookie';

export function setupI18n(options) {
    const i18n = createI18n(options);
    options.locale = Cookies.get('locale') || CONFIG.DEFAULT_LOCALE;
    options.messages[options.locale] = loadLocaleMessages(i18n, options.locale);
    setI18nLanguage(i18n, options.locale);
    return i18n;
}

export function setI18nLanguage(i18n, locale) {
    if (i18n.mode === 'legacy') {
        i18n.global.locale = locale;
    } else {
        i18n.global.locale.value = locale;
    }

    /**
     * NOTE:
     * If you need to specify the language setting for headers, such as the `fetch` API, set it here.
     * The following is an example for axios.
     *
     * axios.defaults.headers.common['Accept-Language'] = locale
     */
    document.querySelector('html').setAttribute('lang', locale);
}

export default setupI18n({
    legacy: true,
    locale: CONFIG.DEFAULT_LOCALE,
    fallbackLocale: CONFIG.DEFAULT_LOCALE,
    messages: {}
});

export async function loadLocaleMessages(i18n, locale) {
    // load locale messages
    if (!i18n.global.availableLocales.includes(locale)) {
        const messages = await import(
            /* webpackChunkName: "locale-[request]" */ `./lang/${locale}/index.js`
        );
        i18n.global.setLocaleMessage(locale, messages.default);
    }
}
