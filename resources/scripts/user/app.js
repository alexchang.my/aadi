import axios from "axios";
import { createPinia } from 'pinia';
import { createApp } from "vue";
import router from './router.js';
import i18n from "./i18n.js";
import helper from "./helper.js";
import moment from "moment";
import $bus from '!/event-bus.js';
import '!/shared/css/shared.css';
import './css/app.css';
import 'sweetalert2/dist/sweetalert2.min.css'
import {CONFIG} from "~/config.js";
import Cookies from "js-cookie";
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import './font-awesome.js';

import App from './App.vue';
import {useUserStore} from "~/stores/user.js";
import {useLocaleStore} from "~/stores/locale.js";

const pinia = createPinia();
const app = createApp(App);

// Configure global helpers
const helpers = {
    install(app) {
        app.helper = helper;
        app.provide('$helper', helper);
        app.config.globalProperties.$helper = helper;
        // app.prototype.$helper = helper;
        app.moment = moment;
        app.provide('$moment', moment);
        app.config.globalProperties.$moment = moment;

        app.bus = $bus;
        app.provide('$bus', $bus);
        app.config.globalProperties.$bus = $bus;
    }
};

/**
 * SETUP AXIOS -- START
 */
axios.defaults.withCredentials = true;
axios.defaults.baseURL = CONFIG.API_URL;
axios.defaults.headers['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers['Cache-Control'] = 'no-cache';
axios.defaults.headers['Pragma'] = 'no-cache';
axios.defaults.headers['Expires'] = 0;
axios.defaults.headers['Accept'] = 'application/json';
axios.defaults.headers['Content-Type'] = 'application/json';
axios.defaults.headers['Accept-Language'] = Cookies.get('locale') || CONFIG.DEFAULT_LOCALE;
axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';

axios.interceptors.response.use(response => {
    return response.data;
}, async error => {
    if (typeof error.response !== 'undefined') {
        const { status, data } = error.response;
        if (status === 401) {
            try {
                const $userStore = useUserStore();
                await $userStore.logoutAccount();
            } catch (e) {
                console.log(e);
            } finally {
                window.location.href = CONFIG.APP_ROOT;
            }
        } else if (status === 403) {
            window.history.back();
        } else if (status === 422 && data.message) {
            await helper.alertError({message: data.message});
        } else if (status === 419) {
            await axios.get(`${window.location.origin}/sanctum/csrf-cookie`).then(async response => {});
        }
    } else {
        console.log(error);
    }
    return Promise.reject(error);
});
/**
 * SETUP AXIOS -- END
 */

app
    .use(pinia)
    .use(i18n)
    .use(helpers)
    .use(router)
    .component('font-awesome-icon', FontAwesomeIcon)
    .mixin({
        computed: {
            $appName() {
                return import.meta.env.VITE_APP_NAME;
            },
            $locale() {
                const $localeStore = useLocaleStore();
                return $localeStore.locale;
            },
            $locales() {
                const $localeStore = useLocaleStore();
                return $localeStore.locale;
            },
            $user() {
                const $userStore = useUserStore();
                return $userStore.account;
            },
        }
    })
    .mount('#app');
