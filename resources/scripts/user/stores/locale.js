import { defineStore } from 'pinia';
import {CONFIG} from "~/config.js";
import Cookies from "js-cookie";
import axios from "axios";
import {useStorage} from "@vueuse/core";
import {useStorageDefaultOptions} from "!/shared/stores/useStorage.js";

const namespace = 'user';

export const useLocaleStore = defineStore({
    id: 'locale',
    state: () => {
        return {
            locale: useStorage(`${namespace}_locale`, CONFIG.DEFAULT_LOCALE, localStorage, useStorageDefaultOptions),
            locales: window.config.locales,
        };
    },
    actions: {
        updateAxios() {
            axios.defaults.headers['Accept-Language'] = this.locale || CONFIG.DEFAULT_LOCALE;
        },
        async updateLocale(l) {
            this.locale = l;
            Cookies.set('locale', l, { expires: 365 });

            //WANT UPDATE SERVER? DO IT HERE
            await location.reload();
        }
    }
});
