import { defineStore } from 'pinia';
import axios from "axios";
import Cookies from "js-cookie";
import {useLocaleStore} from "~/stores/locale.js";
import {useStorage} from "@vueuse/core";
import {CONFIG} from "~/config.js";
import {useStorageDefaultOptions} from "!/shared/stores/useStorage.js";
import BigNumber from "bignumber.js";

const tokenKey = 'ut';

export const useUserStore = defineStore({
    id: 'user',
    state: () => {
        return {
            account: useStorage(`${tokenKey}_account`, null, localStorage, useStorageDefaultOptions),
            token: useStorage(`${tokenKey}_token`, null, localStorage, useStorageDefaultOptions),
            lastUpdate: useStorage(`${tokenKey}_last_update`, null, localStorage, useStorageDefaultOptions),
        };
    },
    getters: {
        creditTotalValue: (state) => {
            return new BigNumber(state.account.credit_1).plus(new BigNumber(state.account.credit_2));
        },
        permissions: (state) => {
            if (!state.account) return [];
            if (!state.account.group) return [];
            if (!state.account.group.permissions) return [];
            return state.account.group.permissions.map(r => {
                return r.permission_tag;
            });
        }
    },
    actions: {
        updateAxios() {
            axios.defaults.headers['Authorization'] = 'Bearer ' + this.token;
        },
        async fetchAccount() {
            const localeStore = useLocaleStore();
            await axios.post('/me').then(async (data) => {
                this.updateAccount(data);
                if (data && data.lang && data.lang !== localeStore.locale) {
                    await localeStore.updateLocale(data.lang);
                }
            })
        },
        updateAccount(acc) {
            this.account = acc;
            this.lastUpdate = Date.now();
        },
        updateToken(t) {
            this.token = t;
            this.updateAxios();
        },
        async loginAccount(t) {
            this.updateToken(t);
            await this.fetchAccount();
        },
        logoutAccount() {
            this.account = null;
            this.token = null;
            this.lastUpdate = null;

            Cookies.remove(tokenKey);
            localStorage.removeItem(`${tokenKey}_account`);
            localStorage.removeItem(`${tokenKey}_token`);
            localStorage.removeItem(`${tokenKey}_last_update`);
        },
        async checkIfShouldRefreshFromServer() {
            if (this.account) {
                if (!this.lastUpdate) {
                    await this.fetchAccount();
                } else {
                    let seconds = (this.lastUpdate - Date.now()) / 1000;
                    if (seconds >= CONFIG.IDENTITY_REFETCH_SECONDS || seconds <= -CONFIG.IDENTITY_REFETCH_SECONDS) {
                        await this.fetchAccount();
                    }
                }
            }
        },
    }
})
