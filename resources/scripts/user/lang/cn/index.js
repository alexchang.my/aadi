import SharedCommon from '!/shared/lang/cn/index.js';
import Common from './_common.json';

export default {
    ...SharedCommon,
    ...Common,
}
