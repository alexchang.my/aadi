import SharedCommon from '!/shared/lang/en/index.js';
import Common from './_common.json';

export default {
    ...SharedCommon,
    ...Common,
}
