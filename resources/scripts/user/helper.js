import {CONFIG} from './config.js';
import Swal from "sweetalert2";
import $i18n from "!/admin/i18n.js";
import moment from "moment/moment.js";

export default {
    ucfirst(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },
    async alertSuccess({message, callback = null, message_is_html = false}) {
        let obj = {
            icon: 'success'
        };
        if (message_is_html) {
            obj.html = message;
        } else {
            obj.text = message;
        }

        Swal.fire(obj).then((result) => {
            if (typeof callback === 'function') {
                callback(result);
            }
        });
    },
    async alertError({message, callback = null, message_is_html = false}) {
        let obj = {
            icon: 'error'
        };
        if (message_is_html) {
            obj.html = message;
        } else {
            obj.text = message;
        }

        Swal.fire(obj).then((result) => {
            if (typeof callback === 'function') {
                callback(result);
            }
        });
    },
    async alertWarning({message, callback = null, message_is_html = false}) {
        let obj = {
            icon: 'warning'
        };
        if (message_is_html) {
            obj.html = message;
        } else {
            obj.text = message;
        }

        Swal.fire(obj).then((result) => {
            if (typeof callback === 'function') {
                callback(result);
            }
        });
    },
    async alertConfirm({message = null, callback = null, message_is_html = false}) {
        let obj = {
            icon: 'warning'
        };

        if (message === null) {
            $i18n.global.t('this_action_cannot_undone');
        }
        if (message_is_html) {
            obj.html = message;
        } else {
            obj.text = message;
        }

        obj.title = $i18n.global.t('are_you_sure') + '?';
        obj.showCancelButton = true;
        obj.confirmButtonColor = '#3085d6';
        obj.cancelButtonColor = '#d33';
        obj.confirmButtonText = $i18n.global.t('yes');
        obj.cancelButtonText = $i18n.global.t('cancel');

        Swal.fire(obj).then((result) => {
            if (typeof callback === 'function') {
                callback(result);
            }
        });
    },
    buildSelectFromArray(arr) {
        const resp = [];
        arr.forEach((key, value) => {
            resp.push({
                'text': key,
                'value': value
            });
        });
        return resp;
    },
    buildSelectFromArrayOrObject(arr) {
        const resp = [];
        if (Array.isArray(arr)) {
            arr.forEach((key, value) => {
                resp.push({
                    'text': key,
                    'value': value
                });
            });
        } else if (arr instanceof Object) {
            Object.keys(arr).forEach(k => {
                resp.push({
                    'text': arr[k],
                    'value': parseInt(k)
                });
            });
        }

        return resp;
    },
    buildSelectFromObject(obj, textField, valueField) {
        const resp = [];
        Object.entries(obj).forEach(([key, value]) => {
            resp.push({
                'text': value[textField],
                'value': valueField ? value[valueField] : key
            });
        });
        return resp;
    },
    buildSelect(obj, properties) {
        const newObj = {...obj};
        properties.forEach(property => {
            newObj[property] = [];
            Object.entries(obj[property]).forEach(([key, value]) => {
                newObj[property].push({
                    'text': value,
                    'value': key,
                });
            });
        });
        return newObj;
    },
    buildMultilingualArray(field_name) {
        return Object.keys(window.config.locales).map(k => {
            return field_name + '_' + k;
        });
    },
    distanceFormat: function (distance) {
        distance = parseFloat(distance);
        if (distance <= 1) {
            return '<1 km';
        } else {
            return distance.toFixed(1) + ' km';
        }
    },
    dateFormat: function (dt) {
        return moment(dt).format('YYYY-MM-DD');
    },
    timeFormat: function (dt) {
        return moment(dt).format('hh:mm:ss A');
    },
    dateTimeFormat: function (dt) {
        return moment(dt).format('YYYY-MM-DD hh:mm:ss A');
    },
    fundFormat: function (amount, decimal, separator) {
        if (isNaN(amount)) amount = 0;
        amount = parseFloat(amount.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);

        if (typeof decimal === 'undefined') {
            decimal = CONFIG.DECIMAL_POINT;
        }

        if (typeof separator === 'undefined') {
            separator = true;
        }

        // amount = parseFloat(amount);
        // amount = amount < 0 ? Math.round(Math.abs(amount) * 100) * -1 / 100 : amount;
        amount = amount.toFixed(decimal);

        if (separator === true) {
            let parts = amount.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        } else {
            return amount;
        }
    },
    optionalDecimalFormat: function (amount, decimal) {
        if (typeof decimal === 'undefined') {
            decimal = CONFIG.DECIMAL_POINT;
        }

        return (amount * 1).toFixed(decimal).replace(/[.,]00$/, "");
    },
    isImage(str) {
        return !!str.match(/.(jpg|jpeg|png|gif|bmp)$/i);
    },
    deepCopy(obj) {
        if (obj === null || typeof obj !== 'object') {
            return obj;
        }

        const copy = Array.isArray(obj) ? [] : {};

        Object.keys(obj).forEach(key => {
            copy[key] = this.deepCopy(obj[key]);
        });

        return copy;
    },
    arrayWrap(value) {
        return Array.isArray(value) ? value : [value];
    },
    camelCase(str) {
        let arr = str.split(/[_-]/);
        let newStr = "";
        for (let i = 1; i < arr.length; i++) {
            newStr += arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
        }
        return arr[0] + newStr;
    },
    pascalCase(str) {
        let arr = this.camelCase(str);
        return arr[0].toUpperCase() + arr.slice(1);
    },
    randomInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    randomString(length = 10) {
        let result           = '';
        let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for ( let i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    },
    guidGenerator() {
        let S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    },
    pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    },
    getCurrentDomain() {
        return window.location.protocol + '//' + window.location.hostname;
    },
    safeImageUrl(url) {
        let entities = [
            ['%3F', '?'],
            ['%3D', '='],
        ];
        for (let i = 0, max = entities.length; i < max; ++i)
            url = url.replace(new RegExp(entities[i][0], 'ig'), entities[i][1]);

        return url;
    },
    async adjustYoutubeIframe() {
        return new Promise(resolve => {
            if (document.getElementsByTagName('iframe').length) {
                for (let iframe of document.getElementsByTagName('iframe')) {
                    const regex = /^(https?:\/\/)?((www\.)?youtube\.com|youtu\.?be)\/.+$/gm;
                    if (iframe.src && regex.test(iframe.src)) {
                        let container = iframe.closest('p');
                        if (!container) {
                            container = iframe.closest('div');
                        }

                        if (container) {
                            if (iframe.clientWidth > container.clientWidth) {
                                let ratio = (container.clientWidth / iframe.clientWidth) * 100;
                                iframe.width = (iframe.clientWidth / 100) * ratio;
                                iframe.height = (iframe.clientHeight / 100) * ratio;
                            }
                        }
                    }
                }
            }

            resolve();
        });
    },
    editorJsParser(v) {
        if (!v) return null;
        if (v === 'null') return null;
        try {
            v = JSON.parse(v);
        } catch (e) {
            return v;
        }

        if (Array.isArray(v.blocks) && v.blocks.length) {
            let convertedHtml = '';
            v.blocks.forEach((block) => {
                switch (block.type) {
                    case "header":
                        convertedHtml += `<h${block.data.level}>${block.data.text}</h${block.data.level}>`;
                        break;
                    case "embed":
                        convertedHtml += `<div><iframe width="${block.data.width}" height="${block.data.height}" src="${block.data.embed}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen class="w-full aspect-video"></iframe>`;
                        if (block.data.caption) {
                            convertedHtml += `<br /><em>${block.data.caption}</em>`;
                        }
                        convertedHtml += '</div>';
                        break;
                    case "paragraph":
                        if (!block.data.text) {
                            convertedHtml += `<p>     </p>`;
                        } else {
                            convertedHtml += `<p>${block.data.text}</p>`;
                        }
                        break;
                    case "delimiter":
                        convertedHtml += "<hr />";
                        break;
                    case "image":
                        convertedHtml += `<div><img class="img-fluid" src="${block.data.file.url}" title="${block.data.caption}" /><br /><em>${block.data.caption}</em></div>`;
                        break;
                    case "list":
                        if (block.data.style === 'unordered') {
                            convertedHtml += '<ul class="pl-3" style="list-style: disc">';
                        } else {
                            convertedHtml += '<ul class="pl-3" style="list-style: decimal">';
                        }

                        block.data.items.forEach(function (li) {
                            convertedHtml += `<li>${li}</li>`;
                        });
                        convertedHtml += "</ul>";
                        break;
                    case "link":
                        convertedHtml += `<div><a class="text-primary" href="${block.data.link}">${block.data.link}</a></div>`;
                        break;
                    default:
                        console.log("Unknown block type", block.type);
                        break;
                }
            });

            return convertedHtml;
        }

        return null;
    },
    isValidContactNumber(contact_country_id, contact_number) {
        return parseInt(contact_country_id) > 0 && contact_number.length >= 8;
    },
    guessIsURL(str) {
        return str.startsWith('http://') || str.startsWith('https://') || str.startsWith('www.');
    },
    validURL(str) {
        let pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    },
    stripTags(str) {
        return str.replace(/(<([^>]+)>)/gi, "");
    },
    getLanguages() {
        return {
            en: {
                flag: '/img/flags/us.svg',
                text: 'English',
                locale: 'en',
            },
            cn: {
                flag: '/img/flags/cn.svg',
                text: '中文',
                locale: 'cn',
            }
        }
    },
    async copyText(text, callback) {
        // Create new element
        let el = document.createElement('textarea');
        // Set value (string to be copied)
        el.value = text;
        // Set non-editable to avoid focus and move outside of view
        el.setAttribute('readonly', '');
        el.style = {position: 'absolute', left: '-9999px'};
        document.body.appendChild(el);
        // Select text inside element
        el.select();
        // Copy text to clipboard
        document.execCommand('copy');
        // Remove temporary element
        document.body.removeChild(el);

        if (typeof callback === 'function') {
            callback();
        }
    }
};
