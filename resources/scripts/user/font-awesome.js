import { library } from '@fortawesome/fontawesome-svg-core';

import { faHome, faMagnifyingGlass, faBriefcase, faPeopleGroup, faUser, faCaretUp, faCaretDown, faShareNodes, faChevronRight, faClose, faKey, faWallet, faLock, faArrowLeft, faCopy, faUserPlus, faSquare, faCheckSquare } from "@fortawesome/free-solid-svg-icons";
import { faTrashAlt, faClock, faUser as faRegularUser, faQuestionCircle } from '@fortawesome/free-regular-svg-icons';

library.add(faHome, faMagnifyingGlass, faBriefcase, faPeopleGroup, faUser, faCaretUp, faCaretDown, faShareNodes, faChevronRight, faClose, faKey, faWallet, faLock, faArrowLeft, faCopy, faUserPlus, faSquare, faCheckSquare);
library.add(faTrashAlt, faClock, faRegularUser, faQuestionCircle);
