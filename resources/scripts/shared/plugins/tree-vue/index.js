import TreeRoot from './components/tree-root.vue';
import TreeBranch from './components/tree-branch.vue';
export default {
  install: (app) => {
    app.component('TreeRoot', TreeRoot);
    app.component('TreeBranch', TreeBranch);
  }
};
