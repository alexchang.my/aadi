import { createWebHistory, createRouter } from "vue-router";
import Router from './components/blank-router.vue';
import GlobalLayout from './layouts/global-layout.vue';
import MainLayout from './layouts/main-layout.vue';
import {useAdminStore} from "~/stores/admin.js";
import {useSettingStore} from "~/stores/setting.js";

const routes = [
    {
        path: '/admin',
        component: GlobalLayout,
        redirect: {name: 'admin.home'},
        children: [
            {
                path: '',
                component: MainLayout,
                meta: { auth: true },
                children: [
                    {
                        path: '',
                        name: 'admin.home',
                        component: () => import('./pages/main/home-page.vue'),
                    },
                    {
                        path: 'user',
                        component: Router,
                        redirect: 'admin.user.user.list',
                        children: [
                            {
                                path: 'user/list',
                                name: 'admin.user.user.list',
                                component: () => import('./pages/main/user/user/list-page.vue'),
                            },
                            {
                                path: 'topup/list',
                                name: 'admin.user.topup.list',
                                component: () => import('./pages/main/user/topup/list-page.vue'),
                            },
                            {
                                path: 'withdrawal/list',
                                name: 'admin.user.withdrawal.list',
                                component: () => import('./pages/main/user/withdrawal/list-page.vue'),
                            }
                        ]
                    },
                    {
                        path: 'article/list',
                        name: 'admin.article.list',
                        component: () => import('./pages/main/article/list-page.vue'),
                    },
                    {
                        path: 'investment_plan',
                        component: Router,
                        redirect: 'admin.investment_plan.investment_plan.list',
                        children: [
                            {
                                path: 'investment_plan/list',
                                name: 'admin.investment_plan.investment_plan.list',
                                component: () => import('./pages/main/investment-plan/investment-plan/list-page.vue'),
                            },
                            {
                                path: 'transaction/:code',
                                name: 'admin.investment_plan.transaction',
                                component: () => import('./pages/main/investment-plan/transaction/list-page.vue'),
                            },
                        ]
                    },
                    {
                        path: 'account',
                        component: Router,
                        redirect: 'admin.account.deposit',
                        children: [
                            {
                                path: 'deposit',
                                name: 'admin.account.deposit',
                                component: () => import('./pages/main/account/deposit-account-page.vue')
                            },
                            {
                                path: 'assured',
                                name: 'admin.account.assured',
                                component: () => import('./pages/main/account/assured-account-page.vue')
                            },
                            {
                                path: 'structured',
                                name: 'admin.account.structured',
                                component: () => import('./pages/main/account/structured-account-page.vue')
                            },
                            {
                                path: 'opex',
                                name: 'admin.account.opex',
                                component: () => import('./pages/main/account/opex-account-page.vue')
                            },
                            {
                                path: 'pl',
                                name: 'admin.account.pl',
                                component: () => import('./pages/main/account/pl-account-page.vue')
                            },
                            {
                                path: 'reserved',
                                name: 'admin.account.reserved',
                                component: () => import('./pages/main/account/reserved-account-page.vue')
                            },
                            {
                                path: 'fees_charges',
                                name: 'admin.account.fees_charges',
                                component: () => import('./pages/main/account/fees-charges-account-page.vue')
                            },
                        ]
                    },
                    {
                        path: 'report',
                        component: Router,
                        redirect: 'admin.report.daily_report',
                        children: [
                            {
                                path: 'assured_daily_report',
                                name: 'admin.report.assured_daily_report',
                                component: () => import('./pages/main/report/assured-daily-report-page.vue')
                            },
                            {
                                path: 'assured_trader_declare',
                                name: 'admin.report.assured_trader_declare',
                                component: () => import('./pages/main/report/assured-trader-declare-page.vue')
                            },
                            {
                                path: 'structured_daily_report',
                                name: 'admin.report.structured_daily_report',
                                component: () => import('./pages/main/report/structured-daily-report-page.vue')
                            },
                            {
                                path: 'structured_trader_declare',
                                name: 'admin.report.structured_trader_declare',
                                component: () => import('./pages/main/report/structured-trader-declare-page.vue')
                            },
                            {
                                path: 'user_investments',
                                name: 'admin.report.user_investments',
                                component: () => import('./pages/main/report/investment-report-page.vue')
                            },
                        ]
                    },
                    {
                        path: 'other',
                        component: Router,
                        redirect: 'admin.other.audit_trail.list',
                        children: [
                            {
                                path: 'country/list',
                                name: 'admin.other.country.list',
                                component: () => import('./pages/main/other/country/list-page.vue'),
                            },
                            {
                                path: 'setting/list',
                                name: 'admin.other.setting.list',
                                component: () => import('./pages/main/other/setting/list-page.vue'),
                            },
                            {
                                path: 'bank/list',
                                name: 'admin.other.bank.list',
                                component: () => import('./pages/main/other/bank/list-page.vue'),
                            },
                            {
                                path: 'company_bank/list',
                                name: 'admin.other.company_bank.list',
                                component: () => import('./pages/main/other/company-bank/list-page.vue'),
                            },
                            {
                                path: 'audit_trail/list',
                                name: 'admin.other.audit_trail.list',
                                component: () => import('./pages/main/other/audit-trail/list-page.vue'),
                            },
                        ]
                    },
                    {
                        path: 'management',
                        component: Router,
                        redirect: 'admin.management.admin.list',
                        children: [
                            {
                                path: 'admin_group/list',
                                name: 'admin.management.admin_group.list',
                                component: () => import('./pages/main/management/admin-group/list-page.vue'),
                            },
                            {
                                path: 'admin/list',
                                name: 'admin.management.admin.list',
                                component: () => import('./pages/main/management/admin/list-page.vue'),
                            }
                        ]
                    }

                ]
            },
            {
                path: '',
                component: Router,
                meta: { auth: false, sidebar: false },
                children: [
                    {
                        path: 'login',
                        name: 'admin.auth.login',
                        component: () => import('./pages/auth/login-page.vue'),
                    }
                ]
            },
            {
                path: "/:catchAll(.*)",
                name: "NotFound",
                redirect: {name: 'admin.home'}
            }
        ]
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes: routes,
    async scrollBehavior (to, from, savedPosition) {
        if (to.hash) {
            return {
                selector: to.hash,
                behavior: 'smooth',
            };
        } else {
            if (savedPosition) {
                return savedPosition;
            } else {
                return new Promise((resolve) => {
                    window.scrollTo(0, 0);
                    resolve();
                });
            }
        }
    }
});

router.beforeEach(async (to, from, next) => {
    const $adminStore = useAdminStore();
    const $settingStore = useSettingStore();

    await $settingStore.checkIfShouldRefreshFromServer();

    if ($adminStore.token) {
        await $adminStore.checkIfShouldRefreshFromServer();
    }

    //NOT LOGGED IN WHILE THE ROUTE GROUP NEED AUTH
    if(!$adminStore.account) {
        let needAuth = [...to.matched].reverse().find(r => r.meta.auth);
        if (needAuth) {
            return next({ name: 'admin.auth.login' });
        }
    } else {
        let guestOnly = [...to.matched].reverse().find(r => r.meta.auth === false);
        if (guestOnly) {
            return next({ name: 'admin.home' });
        }

        await $adminStore.fetchNotifications();
    }

    return next();
});
export default router;
