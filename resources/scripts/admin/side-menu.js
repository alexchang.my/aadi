export default [
    {name: 'dashboard', route: 'admin.home', icon: 'home'},
    {
        name: 'user', icon: 'menu', children: [
            {name: 'manage_user', route: 'admin.user.user.list', permissions: ['manage_user']},
            {name: 'manage_topup', route: 'admin.user.topup.list', permissions: ['manage_topup'], notification: 'pending_topup'},
            {name: 'manage_withdrawal', route: 'admin.user.withdrawal.list', permissions: ['manage_withdrawal'], notification: 'pending_withdrawal'},
        ]
    },
    {
        name: 'investment_plan', icon: 'menu', children: [
            {name: 'manage_investment_plan', icon: 'menu', route: 'admin.investment_plan.investment_plan.list', permissions: ['manage_investment_plan']},
        ]
    },
    {
        name: 'account', icon: 'menu', children: [
            {name: 'deposit_account', icon: 'menu', route: 'admin.account.deposit', permissions: ['deposit_account']},
            {name: 'assured_account', icon: 'menu', route: 'admin.account.assured', permissions: ['assured_account']},
            {name: 'structured_account', icon: 'menu', route: 'admin.account.structured', permissions: ['structured_account']},
            {name: 'opex_account', icon: 'menu', route: 'admin.account.opex', permissions: ['opex_account']},
            {name: 'pl_account', icon: 'menu', route: 'admin.account.pl', permissions: ['pl_account']},
            {name: 'reserved_account', icon: 'menu', route: 'admin.account.reserved', permissions: ['reserved_account']},
            {name: 'fees_charges_account', icon: 'menu', route: 'admin.account.fees_charges', permissions: ['fees_charges_account']},
        ]
    },
    {
        name: 'report', icon: 'menu', children: [
            {name: 'assured_daily_report', icon: 'menu', route: 'admin.report.assured_daily_report', permissions: ['assured_daily_report']},
            {name: 'assured_trader_declare', icon: 'menu', route: 'admin.report.assured_trader_declare', permissions: ['trader_declare', 'roi_adjustment', 'approve_roi_adjustment']},
            {name: 'structured_daily_report', icon: 'menu', route: 'admin.report.structured_daily_report', permissions: ['structured_daily_report']},
            {name: 'structured_trader_declare', icon: 'menu', route: 'admin.report.structured_trader_declare', permissions: ['trader_declare', 'roi_adjustment', 'approve_roi_adjustment']},
            {name: 'investment_report', icon: 'menu', route: 'admin.report.user_investments', permissions: ['investment_report']},
        ]
    },
    {
        name: 'manage_article', icon: 'menu', route: 'admin.article.list', permissions: ['manage_article']
    },
    {
        name: 'management', icon: 'menu', children: [
            {name: 'manage_admin', route: 'admin.management.admin.list', permissions: ['manage_admin']},
            {name: 'manage_admin_group', route: 'admin.management.admin_group.list', permissions: ['manage_admin_group']},
        ]
    },
    {
        name: 'other', icon: 'menu', children: [
            {name: 'manage_country', route: 'admin.other.country.list', permissions: ['manage_country']},
            {name: 'manage_setting', route: 'admin.other.setting.list', permissions: ['manage_setting']},
            {name: 'audit_trail', route: 'admin.other.audit_trail.list', permissions: ['audit_trail']},
        ]
    }
];
