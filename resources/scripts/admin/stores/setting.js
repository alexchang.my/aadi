import {useStorage} from "@vueuse/core";
import {defineStore} from "pinia";
import axios from "axios";
import {useStorageDefaultOptions} from "!/shared/stores/useStorage.js";

const namespace = 'admin';

export const useSettingStore = defineStore({
    id: 'setting',
    state: () => {
        return {
            settings: useStorage(`${namespace}_setting`, null, localStorage, useStorageDefaultOptions),
            lastUpdate: useStorage(`${namespace}_last_update`, null, localStorage, useStorageDefaultOptions)
        };
    },
    getters: {
        countryForOptions: (state) => {
            if (!state.settings || !state.settings.country) return [];
            return Object.keys(state.settings.country).map(key => {
                return {
                    value: String(state.settings.country[key].id),
                    text: String(state.settings.country[key].name)
                };
            }) || [];
        },
        extForOptions: (state) => {
            if (!state.settings || !state.settings.country) return [];
            return Object.keys(state.settings.country).map(key => {
                return {
                    value: String(state.settings.country[key].id),
                    text: String(state.settings.country[key].ext)
                };
            }) || [];
        },
        defaultCountry: (state) => {
            if (!state.settings.country) return null;
            if (!state.settings.default_country_code) return null;
            return state.settings.country.find(row => {
                return String(row.iso2).toUpperCase() === String(state.settings.default_country_code).toUpperCase();
            });
        }
    },
    actions: {
        async checkIfShouldRefreshFromServer() {
            if (!this.lastUpdate) {
                await this.fetchSettings();
            } else {
                let seconds = (this.lastUpdate - Date.now()) / 1000;
                if (seconds >= 600 || seconds <= -600) {
                    await this.fetchSettings();
                }
            }
        },
        setSettings({commit}, settings) {
            this.settings = settings;
        },
        async fetchSettings() {
            await axios.post('/settings').then(async (data) => {
                this.settings = data.settings;
                this.lastUpdate = Date.now();
            });
        }
    }
});
