export const CONFIG = {
    ROLE: 'admin',
    DEFAULT_LOCALE: 'en',
    DEFAULT_CURRENCY: 'RM',
    DECIMAL_POINT: 2,
    IDENTITY_REFETCH_SECONDS: 60,
    COOKIES_SAMESITE: 'lax',
    TIMEZONE: 'Asia/Kuala_Lumpur',
    API_URL: `${window.location.origin}/api/admin`,
    APP_ROOT: '/admin'
};
