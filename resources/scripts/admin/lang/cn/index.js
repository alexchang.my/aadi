import SharedCommon from '!/shared/lang/cn/index.js';
import Common from './_common.json';
import Permission from './_permission.json';

export default {
    ...SharedCommon,
    ...Common,
    ...Permission
}
