import { library } from '@fortawesome/fontawesome-svg-core';

import { faKey, faCopy, faMoneyBill, faCheckSquare, faSquareXmark, faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';

library.add(faKey, faCopy, faMoneyBill, faCheckSquare, faSquareXmark, faPaperPlane);
library.add(faTrashAlt);
