@php
    $config = [
        'appName' => config('app.name'),
        'fallback_locale' => config('app.fallback_locale'),
        'locale' => app()->getLocale(),
        'locales' => config('app.locales')
    ];
@endphp

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="default">

<head>
    {{--    <meta name="viewport" content="viewport-fit=cover, width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">--}}
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover, user-scalable=no, minimum-scale=1.0, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="{{ config('env.APP_NAME') }}">
    <meta name="theme-color" content="#191d24">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset='utf-8'>

    @if (request()->hasCookie('adminUserToken'))
        <meta name="admin_user_token" content="{{ request()->cookie('adminUserToken') }}">
        @php(\Cookie::queue(\Cookie::forget('adminUserToken')))
    @endif

    <link rel="apple-touch-icon" sizes="57x57" href="/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <title>{{ config('env.APP_NAME') }}</title>
    <meta property="og:url"                content="{{ request()->fullUrl() }}" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="{{ $meta['title'] }}" />
    <meta property="og:description"        content="{{ $meta['description'] }}" />
    <meta property="og:image"              content="{{ $meta['image'] }}" />
    @if ($meta['image_alt'])
        <meta property="og:image:alt"              content="{{ $meta['image_alt'] }}" />
    @endif

    @if ($role == 'user')
        <link href="//fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&family=Roboto&display=swap" rel="stylesheet">
        <link rel="manifest" href="/site.webmanifest" />
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">

        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <script>
            if (navigator.serviceWorker.controller) {
                console.log("Active service worker found");
            } else {
                if (typeof navigator.serviceWorker !== 'undefined') {
                    navigator.serviceWorker.register('service-worker.js')
                }
            }
        </script>
    @endif
</head>

<body class="{{ app()->getLocale() }} main">
<script>
    window.config = @json($config)
</script>
<noscript>
    You need to enable JavaScript to run this app.
</noscript>
<div id="app"></div>
{!! $assets !!}
</body>

</html>
